CREATE DEFINER=`root`@`localhost` PROCEDURE `MGNH050`(
	IN IN_GENKA_FLG CHAR(1),
	IN IN_ODR_KUBN CHAR(1),
	OUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMGNH050
	-- 機能		: 	納品基準  原価計算  店舗在庫マスタ（月次保管）更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-01
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _den_to_date DATE; 
	
	/* 業務コントロールＦ */
	DECLARE _p1_kony_egtn_code CHAR(7);
	DECLARE _p1_kony_barcd CHAR(19);
	DECLARE _p1_kyu CHAR(2);
	DECLARE _p1_syhn_code CHAR(9);
	DECLARE _p1_upd_flag INT;
	DECLARE _p2_genk_tank DOUBLE;
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	DECLARE IsNotFound INT;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	
	/*INIT SETUP*/
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/* プログラムID・端末IDの取得 */
	SET _pgmid = '\0';
	
	SELECT 
		DEN_TO_DATE
		INTO _den_to_date
	FROM MST_HIZS
	WHERE MST_HIZS.DM_KEY = _DM_KEY;
	-- 
	IF _den_to_date IS NULL THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	-- MAIN
	SEL_MAIN: BEGIN
		DECLARE CH050_01 CURSOR FOR
		SELECT KONY_EGTN_CODE, KONY_BARCD, KYU, SYHN_CODE
		, CASE WHEN ((PD_OUT_DATE IS NOT NULL AND PD_OUT_DATE > DATE(_den_to_date))
                             OR (PD_OUT_DATE IS NULL AND CNCL_KUBN = '0')) THEN 0 ELSE 1 END
		FROM MST_ZAI_SHOP_MONTS
		WHERE MST_ZAI_SHOP_MONTS.GENKA_FLG = IN_GENKA_FLG /*0*/
			AND MST_ZAI_SHOP_MONTS.ODR_KUBN = IN_ODR_KUBN /*2*/
			LIMIT 0,100
		;
		
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET IsNotFound=1;
		END;
		
		SET IsNotFound=0;
		
		OPEN CH050_01;
		dept_loop:WHILE(IsNotFound=0) DO

			SET _p1_kony_egtn_code = '\0';
			SET _p1_kony_barcd = '\0';
			SET _p1_kyu = '\0';
			SET _p1_syhn_code = '\0';
			SET _p1_upd_flag = 0;
			
			FETCH FROM CH050_01 
				INTO 
				_p1_kony_egtn_code,
				_p1_kony_barcd,
				_p1_kyu,
				_p1_syhn_code,
				_p1_upd_flag
			;
			
			IF IsNotFound = 1 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = '処理１ IsNotFound';
				LEAVE SEL_MAIN;
			END IF;
			
			/* 級商品別原価マスタ（レディ）読み込み */
			SET _p2_genk_tank = 0;
			
			SELECT GENK_TANK
				INTO _p2_genk_tank
			FROM MST_SYHN_GENK_RDYS
			WHERE MST_SYHN_GENK_RDYS.SYHN_CODE = _p1_syhn_code
				AND MST_SYHN_GENK_RDYS.KYU = _p1_kyu
			;

			IF _p2_genk_tank IS NULL THEN
				SET SQLCODE = 0;
			END IF;
			
			/* 存在した場合の処理 */
			SET @_p2_genk_tank = CAST(_p2_genk_tank AS DECIMAL(11,2));
			IF SQLCODE = 0 THEN
				IF _p1_upd_flag = 0 THEN
					UPDATE MST_ZAI_SHOP_MONTS SET
						GENK_TANK = @_p2_genk_tank
						, TMN_SEK_ZAIKIN 	= CAST(@_p2_genk_tank * TMN_SEK_ZAISU 	AS DECIMAL(11,0))
						, GEN_SEK_ZAIKIN 	= CAST(@_p2_genk_tank * GEN_SEK_ZAISU 	AS DECIMAL(11,0))
						, TMN_SHOP_ZAIKIN = CAST(@_p2_genk_tank * TMN_SHOP_ZAISU 	AS DECIMAL(11,0))
						, GEN_SHOP_ZAIKIN = CAST(@_p2_genk_tank * GEN_SHOP_ZAISU 	AS DECIMAL(11,0))
						, PGM_ID = _pgmid
						, USER_ID = NULL
						, MODIFIED = CURRENT_TIMESTAMP /*TS*/
					WHERE KONY_EGTN_CODE = _p1_kony_egtn_code
					AND KONY_BARCD = _p1_kony_barcd
					;
				ELSE
					UPDATE MST_ZAI_SHOP_MONTS SET
						GENK_TANK = @_p2_genk_tank
						, TMN_SEK_ZAIKIN 	= CAST(@_p2_genk_tank * TMN_SEK_ZAISU 	AS DECIMAL(11,0))
						, GEN_SEK_ZAIKIN 	= CAST(@_p2_genk_tank * GEN_SEK_ZAISU 	AS DECIMAL(11,0))
						, TMN_SHOP_ZAIKIN = CAST(@_p2_genk_tank * TMN_SHOP_ZAISU 	AS DECIMAL(11,0))
						, GEN_SHOP_ZAIKIN = CAST(@_p2_genk_tank * GEN_SHOP_ZAISU 	AS DECIMAL(11,0))
						, GENKA_FLG = '1'
						, GENKA_KESN_FLAG = '0'
						, GENKA_TS = CURRENT_TIMESTAMP
						, PGM_ID = _pgmid
						, USER_ID = NULL
						, MODIFIED = CURRENT_TIMESTAMP /*TS*/
					WHERE KONY_EGTN_CODE = _p1_kony_egtn_code
					AND KONY_BARCD = _p1_kony_barcd
					;
				END IF /*_p1_upd_flag = 0*/;
			ELSE 
			/* 存在しない場合の処理 */
				IF _p1_upd_flag <> 0 THEN
					UPDATE MST_ZAI_SHOP_MONTS SET
						GENKA_FLG = '1'
						, GENKA_KESN_FLAG 	= '1'
						, GENKA_TS 	= CURRENT_TIMESTAMP
						, PGM_ID = _pgmid
						, USER_ID = NULL
						, MODIFIED = CURRENT_TIMESTAMP /*TS*/
					WHERE KONY_EGTN_CODE = _p1_kony_egtn_code
					AND KONY_BARCD = _p1_kony_barcd
					;
				END IF;
			END IF;
		
		END WHILE dept_loop;
		CLOSE CH050_01;
		
	END SEL_MAIN;
	
END MAIN_PROC
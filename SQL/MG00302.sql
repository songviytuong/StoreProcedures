CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00302`(
	IN IN_ZAKR_KUBN CHAR(1),
	IN IN_ODRIGAI_KUBN CHAR(1),
	IN IN_KEJ_DATE_FROM DATE,
	IN IN_KEJ_DATE_TO  DATE,
	IN IN_GENKA_KUBN CHAR(1),
	OUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMG00302
	-- 機能		: 	在庫調整（Ｂ）の原価更新処理
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-01-30
	-- =============================================

	/* 在庫調整Ｂ */
	DECLARE _p0302_1_dnpynum INT;
	DECLARE _p0302_1_srnum DECIMAL(11, 2);
	DECLARE _p0302_1_gynum INT;
	DECLARE _p0302_1_syhn_code CHAR(9);
	DECLARE _p0302_1_kyu CHAR(2);
	DECLARE _p0302_1_sury INT;
	DECLARE _p0302_1_upd_sury DECIMAL(11,2);
	DECLARE _p0302_1_gm_gen_tank DECIMAL(11, 2);
	
	/* 級商品別原価マスタ（レディ） */
	DECLARE _p0302_2_genk_tank DECIMAL(11,2);
	
	/* 作業変数 */
	DECLARE _p0302_wk_genk_tank DECIMAL(11,2);
	DECLARE _p0302_wk_gm_gen_king DECIMAL(11, 2);
	DECLARE _p0302_wk_upd_king DECIMAL(11,2);
	DECLARE _p0302_wk_genka_kei_ym INT;
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	DECLARE _den_from_date DATE; 
	DECLARE _den_to_date DATE; 
	
	DECLARE IsNotFound INT;
	
	/* INIT */
	DECLARE _DM_KEY INT DEFAULT 0;
	
	/***************************************************************/
	/* 初期処理 */
	/***************************************************************/
	
  	/* 計上日を取得する */
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	
	/*INIT SETUP*/
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	SELECT 
			DEN_FROM_DATE, 
			DEN_TO_DATE INTO _den_from_date, _den_to_date
		FROM MST_HIZS
		WHERE MST_HIZS.DM_KEY = _DM_KEY;
	
	IF _den_from_date IS NULL THEN
			SET SQLCODE = 100;
	END IF;
	
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = '初期処理 IsNotFound';
		LEAVE MAIN_PROC;
	END IF;
	
	BEGIN
		DECLARE C0302_01 CURSOR FOR
		SELECT 	DNPYNUM
			, SRNUM
			, GYNUM
			, SYHN_CODE
			, KYU
			, SURY
			, UPD_SURY
			, GM_GEN_TANK
		FROM TRN_ZCYBS
		WHERE TRN_ZCYBS.ZAKR_KUBN		= IN_ZAKR_KUBN
			AND TRN_ZCYBS.ODRIGAI_KUBN 	= IN_ODRIGAI_KUBN
			AND TRN_ZCYBS.KEJ_DATE BETWEEN _den_from_date AND _den_to_date
			AND TRN_ZCYBS.GENKA_KUBN	= IN_GENKA_KUBN
		;
		
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET IsNotFound=1;
		END;
		
		SET IsNotFound=0;
		
		/* INIT */
		SET @DM_KEY = 0;
		SET @den_from_date = IN_KEJ_DATE_FROM;
		SET @den_to_date = IN_KEJ_DATE_TO;
		
		OPEN C0302_01;
		
		/***************************************************************/
		/* メイン処理 */
		/***************************************************************/
		dept_loop:WHILE(IsNotFound=0) DO
		
			SET _p0302_1_dnpynum = 0;
			SET _p0302_1_srnum = 0;
			SET _p0302_1_gynum = 0;
			SET _p0302_1_syhn_code = '\0';
			SET _p0302_1_kyu = '\0';
			SET _p0302_1_sury = 0;
			SET _p0302_1_upd_sury = 0;
			SET _p0302_1_gm_gen_tank = 0;
					
			FETCH FROM C0302_01
				INTO
				_p0302_1_dnpynum,
				_p0302_1_srnum,
				_p0302_1_gynum,
				_p0302_1_syhn_code,
				_p0302_1_kyu,
				_p0302_1_sury,
				_p0302_1_upd_sury,
				_p0302_1_gm_gen_tank
			;
			
			IF IsNotFound=1 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = '処理１ IsNotFound';
				LEAVE dept_loop;
			END IF;
			
			/* 級商品別原価マスタ（レディ）読込み */
			SET _p0302_wk_genk_tank = 0;
			SET _p0302_2_genk_tank = 0;
			SEL: BEGIN
				DECLARE EXIT HANDLER FOR NOT FOUND BEGIN END;
				SELECT GENK_TANK INTO _p0302_2_genk_tank
					FROM MST_SYHN_GENK_RDYS
					WHERE MST_SYHN_GENK_RDYS.SYHN_CODE = _p0302_1_syhn_code
					AND MST_SYHN_GENK_RDYS.KYU = _p0302_1_kyu
				;
				IF _p0302_2_genk_tank IS NULL THEN
					SET SQLCODE = 100;
				END IF;
				
				IF SQLCODE = 100 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = '級商品別原価マスタ（レディ）読込み IsNotFound';
					/* 原価単価取得 */
					SET _p0302_wk_genk_tank = CAST(_p0302_1_gm_gen_tank AS DECIMAL(11,2));
					LEAVE dept_loop;
				ELSE
					SET _p0302_wk_genk_tank = CAST(_p0302_2_genk_tank AS DECIMAL(11,2));
				END IF;
			END SEL;
			
			/* 月末原価金額取得 */
			SET _p0302_wk_gm_gen_king = 0;
			SET _p0302_wk_gm_gen_king = CAST(CAST(_p0302_1_sury AS DECIMAL(9,2)) * CAST(_p0302_wk_genk_tank AS DECIMAL(11,2)) AS DECIMAL(11,0));
		
			/* 更新用金額（原価金額）取得 */
			SET _p0302_wk_upd_king= 0;
			SET _p0302_wk_upd_king = CAST(CAST(_p0302_1_upd_sury AS DECIMAL(9,2)) * CAST(_p0302_wk_genk_tank AS DECIMAL(11,2)) AS DECIMAL(11,0));
			
			/* 原価計算会計月取得 */
			SET _p0302_wk_genka_kei_ym = 0;
			SET _p0302_wk_genka_kei_ym = YEAR(@den_from_date) * 100 + MONTH(@den_to_date);

			/* 在庫調整Ｂ更新 */
			UPT: BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 100;
				END;
				UPDATE TRN_ZCYBS
				SET GM_GEN_TANK 	= CAST(_p0302_wk_genk_tank AS DECIMAL(11,2)),
						GM_GEN_KING 	= CAST(_p0302_wk_gm_gen_king AS DECIMAL(11,0)),
						UPD_KING 			= CAST(_p0302_wk_upd_king AS DECIMAL(11,0)),
						GENKA_KEI_YM 	= _p0302_wk_genka_kei_ym,
						GENKA_TS 			= CURRENT_TIMESTAMP
				WHERE 
						TRN_ZCYBS.DNPYNUM	   		= _p0302_1_dnpynum
						AND TRN_ZCYBS.SRNUM        	= _p0302_1_srnum
						AND TRN_ZCYBS.GYNUM        	= _p0302_1_gynum
				;
				IF SQLCODE = 100 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = '在庫調整Ｂ更新 - UPDATE FAILED';
					LEAVE dept_loop;
				END IF;
			END UPT;
			
		END WHILE dept_loop;
		
		CLOSE C0302_01;
	END;
	
END MAIN_PROC
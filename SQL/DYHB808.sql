CREATE DEFINER=`root`@`localhost` PROCEDURE `DYHB808`(
	IN IN_CTL_KEY CHAR(6),
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYHB808
	-- 機能		: 	売上実績Ｆ 実績集計処理
	-- 著者		: 	TBinh-PrimeLabo
	-- パラメータ	:		(string) IN_CTL_KEY = 'CM-CTL'
	-- 作成日	: 	2019-02-18, 2019-02-19
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _kei_date CHAR(11);
	
	/* 業務コントロールＦ */
	DECLARE _cm_ctl CHAR(3);
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/* プログラムID・端末IDの取得 */
	SET _pgmid = '\0';
	
	/***************************************************************/
	/* 初期処理 */
	/***************************************************************/

	/* 計上日を取得する */
	SEL_MST_HIZS:BEGIN
	DECLARE EXIT HANDLER FOR NOT FOUND
	BEGIN
		SET SQLCODE = 100;
	END;
	SET _kei_date = '\0';
	SELECT KEI_DATE
			INTO _kei_date
			FROM MST_HIZS
		WHERE DM_KEY	= _DM_KEY
	;
	IF _kei_date = '\0' THEN
		SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN 
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("SEL_MST_HIZS IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	END SEL_MST_HIZS;
	
	
	/* シャルム・ココ年齢を取得 */
	SET _cm_ctl = '\0';
	SELECT substr(CTL_DATA, 1, 2)
	 INTO _cm_ctl
	 FROM MST_CTLS
	WHERE CTL_KEY = IN_CTL_KEY
	;
	IF _cm_ctl = '\0' THEN 
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("_cm_ctl IS NULL [`CTL_KEY`='ss']","ss",IN_CTL_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	
	/***************************************************************/
	/* メイン処理 */
	/***************************************************************/
	CUR:BEGIN
	/* 売上レポート実績：売上明細 */
	DECLARE _p1_mous_kubn CHAR(2)						; /* 購入申込区分 */
	DECLARE _p1_cncl_kubn CHAR(2)						; /* キャンセル区分 */
	DECLARE _p1_js_omit_flag CHAR(2)					; /* 売上レポート対象外フラグ */
	DECLARE _p1_keid_date CHAR(11)						; /* 計上日 */
	DECLARE _p1_keid_date_ym INT						; /* 計上日（年月） */
	DECLARE _p1_kkyk_code CHAR(13)						; /* 顧客CD */
	DECLARE _p1_tncho_code CHAR(7)						; /* 店長CD */
	DECLARE _p1_js_egtn_code CHAR(7)					; /* 営業店CD */
	DECLARE _p1_js_egtn_sep_code CHAR(2)				; /* 営業店分割CD */
	DECLARE _p1_js_sibu_code CHAR(7)					; /* 支部CD */
	DECLARE _p1_js_uri_king DOUBLE						; /* 売上金額 */
	DECLARE _p1_so_tant_yaku_code INT					; /* SOCIA役職ランク */
	DECLARE _p1_so_tant_yaku_lvl INT					; /* SOCIA役職レベル */
	DECLARE _p1_so_tant_jotai_code INT					; /* SOCIA社員状態コード */
	DECLARE _p1_so_tant_jiyu_code CHAR(3)				; /* SOCIA事由コード */
	DECLARE _p1_tant1_code CHAR(7)						; /* 担当者１CD */
	DECLARE _p1_tant2_code CHAR(7)						; /* 担当者２CD */
	DECLARE _p1_tant3_code CHAR(7)						; /* 担当者３CD */
	DECLARE _p1_tant4_code CHAR(7)						; /* 担当者４CD */
	DECLARE _p1_tant1_flag CHAR(2)						; /* 担当者１フラグ */
	DECLARE _p1_tant2_flag CHAR(2)						; /* 担当者２フラグ */
	DECLARE _p1_tant3_flag CHAR(2)						; /* 担当者３フラグ */
	DECLARE _p1_tant4_flag CHAR(2)						; /* 担当者４フラグ */
	DECLARE _p1_kony_dnpynum DOUBLE						; /* 購入申込№ */
	DECLARE _p1_kony_srnum DOUBLE						; /* 購入申込シリアル№ */
	/* 担当者マスタ */
	DECLARE _p2_so_tant_yaku_lvl INT					; /* SOCIA役職レベル */
	DECLARE _p2_so_tant_yaku_code INT					; /* SOCIA役職ランク */
	DECLARE _p2_so_tant_jotai_code INT					; /* SOCIA社員状態コード */
	DECLARE _p2_so_tant_jiyu_code CHAR(3)				; /* SOCIA事由コード */
	/* 購入申込ＨＨ */
	DECLARE _p3_charms_kubn CHAR(2)						; /* シャルム区分 */
	/* 作業変数 */
	DECLARE _wk_js_omit_flag CHAR(2)					; /* 売上レポート対象外フラグ */
	DECLARE _wk_new_uri_su INT							; /* 新規売上件数 */
	DECLARE _wk_add_uri_su INT							; /* 追加売上件数 */
	DECLARE _wk_new_can_su INT							; /* 新規キャンセル件数 */
	DECLARE _wk_add_can_su INT							; /* 追加キャンセル件数 */
	DECLARE _wk_new_uri_kin DOUBLE						; /* 新規売上金額 */
	DECLARE _wk_add_uri_kin DOUBLE						; /* 追加売上金額 */
	DECLARE _wk_new_can_kin DOUBLE						; /* 新規キャンセル金額 */
	DECLARE _wk_add_can_kin DOUBLE						; /* 追加キャンセル金額 */
	DECLARE _wk_etc_kin DOUBLE							; /* その他金額 */
	DECLARE _wk_new_uri_kin_etc DOUBLE					; /* 新規売上金額（その他含む） */
	DECLARE _wk_add_uri_kin_etc DOUBLE					; /* 追加売上金額（その他含む） */

	DECLARE _wk_count INT; /* 件数 */
	
	/***************************************************************/
	/* 売上レポート実績：売上明細 */
	/***************************************************************/
	
	/* 売上レポート実績：売上明細読込み */
	DECLARE DYHB808_01 CURSOR FOR 
	SELECT MOUS_KUBN
                   , CNCL_KUBN
                   , JS_OMIT_FLAG
                   , KEID_DATE
                   , YEAR(KEID_DATE) * 100 + MONTH(KEID_DATE)
                   , KKYK_CODE
                   , TNCHO_CODE
                   , JS_EGTN_CODE
                   , JS_EGTN_SEP_CODE
                   , JS_SIBU_CODE
                   , JS_URI_KING
                   , SO_TANT_YAKU_CODE
                   , SO_TANT_YAKU_LVL
                   , SO_TANT_JOTAI_CODE
                   , SO_TANT_JIYU_CODE
                   , TANT1_CODE
                   , TANT2_CODE
                   , TANT3_CODE
                   , TANT4_CODE
                   , CASE TANT1_CODE WHEN '' THEN '0' ELSE '1' END TANT1_FLAG
                   , CASE TANT2_CODE WHEN '' THEN '0' ELSE '1' END TANT2_FLAG
                   , CASE TANT3_CODE WHEN '' THEN '0' ELSE '1' END TANT3_FLAG
                   , CASE TANT4_CODE WHEN '' THEN '0' ELSE '1' END TANT4_FLAG
                   , KONY_DNPYNUM
                   , KONY_SRNUM
                FROM TRN_REPORT_URIS
               WHERE KESUR_FLAG = '2'
                 AND DAILY_FLG  = '0'
	;
	
	DECLARE EXIT HANDLER FOR NOT FOUND
	BEGIN
		SET SQLCODE = 100;
	END;
	
	OPEN DYHB808_01;
	dept_loop:WHILE(SQLCODE=0) DO
	
		SET _p1_mous_kubn 			= '\0';
		SET _p1_cncl_kubn 			= '\0';
		SET _p1_js_omit_flag 		= '\0';
		SET _p1_keid_date 			= '\0';
		SET _p1_keid_date_ym 		= 0;
		SET _p1_kkyk_code 			= '\0';
		SET _p1_tncho_code 			= '\0';
		SET _p1_js_egtn_code 		= '\0';
		SET _p1_js_egtn_sep_code 	= '\0';
		SET _p1_js_sibu_code 		= '\0';
		SET _p1_js_uri_king 		= 0;
		SET _p1_so_tant_yaku_code 	= 0;
		SET _p1_so_tant_yaku_lvl 	= 0;
		SET _p1_so_tant_jotai_code 	= 0;
		SET _p1_so_tant_jiyu_code 	= '\0';
		SET _p1_tant1_code 			= '\0';
		SET _p1_tant2_code 			= '\0';
		SET _p1_tant3_code 			= '\0';
		SET _p1_tant4_code 			= '\0';
		SET _p1_tant1_flag 			= '\0';
		SET _p1_tant2_flag 			= '\0';
		SET _p1_tant3_flag 			= '\0';
		SET _p1_tant4_flag 			= '\0';
		SET _p1_kony_dnpynum 		= 0;
    	SET _p1_kony_srnum 			= 0;

		FETCH FROM DYHB808_01
			INTO
				_p1_mous_kubn
				, _p1_cncl_kubn
				, _p1_js_omit_flag
				, _p1_keid_date
				, _p1_keid_date_ym
				, _p1_kkyk_code
				, _p1_tncho_code
				, _p1_js_egtn_code
				, _p1_js_egtn_sep_code
				, _p1_js_sibu_code
				, _p1_js_uri_king
				, _p1_so_tant_yaku_code
				, _p1_so_tant_yaku_lvl
				, _p1_so_tant_jotai_code
				, _p1_so_tant_jiyu_code
				, _p1_tant1_code
				, _p1_tant2_code
				, _p1_tant3_code
				, _p1_tant4_code
				, _p1_tant1_flag
				, _p1_tant2_flag
				, _p1_tant3_flag
				, _p1_tant4_flag
				, _p1_kony_dnpynum
				, _p1_kony_srnum
		;
		
		IF _p1_mous_kubn = '\0' THEN
			SET SQLCODE = 100;
		END IF;
		
		IF SQLCODE = 100 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'CURSOR IS EMPTY';
			LEAVE dept_loop;
		END IF;
		
		/* 売上レポート対象外フラグ */
		SET _wk_js_omit_flag = '\0';
		IF _p1_js_omit_flag = '0' THEN
			SET _wk_js_omit_flag = '0';
		ELSE
			SET _wk_js_omit_flag = '1';
		END IF;
		
		/* 作業用件数＆金額初期化 */
		SET _wk_new_uri_su 	= 0;
		SET _wk_add_uri_su 	= 0;
		SET _wk_new_can_su 	= 0;
		SET _wk_add_can_su 	= 0;
		SET _wk_new_uri_kin = 0.0;
		SET _wk_add_uri_kin = 0.0;
		SET _wk_new_can_kin = 0.0;
		SET _wk_add_can_kin = 0.0;
		SET _wk_etc_kin = 0.0;
		
		/* 新規売上件数＆金額 */
		IF _p1_mous_kubn = '1' AND _p1_cncl_kubn = '0' THEN
			IF _p1_js_uri_king >= 0 THEN
				SET _wk_new_uri_su = 1;
			ELSE
				SET _wk_new_uri_su = -1;
			END IF;
			SET _wk_new_uri_kin = _p1_js_uri_king;
		END IF;
		
		/* 追加売上件数＆金額 */
		IF _p1_mous_kubn = '2' AND _p1_cncl_kubn = '0' THEN
			IF _p1_js_uri_king >= 0 THEN
				SET _wk_add_uri_su = 1;
			ELSE
				SET _wk_add_uri_su = -1;
			END IF;
			SET _wk_add_uri_kin = _p1_js_uri_king;
		END IF;
		
		/* 新規キャンセル件数＆金額 */
		IF _p1_mous_kubn = '1' AND _p1_cncl_kubn = '1' THEN
			IF _p1_js_uri_king < 0 THEN
				SET _wk_new_can_su = 1;
			ELSE
				SET _wk_new_can_su = -1;
			END IF;
			SET _wk_new_can_kin = _p1_js_uri_king;
		END IF;
		
		/* 追加キャンセル件数＆金額 */
		IF _p1_mous_kubn = '2' AND _p1_cncl_kubn = '1' THEN
			IF _p1_js_uri_king < 0 THEN
				SET _wk_add_can_su = 1;
			ELSE
				SET _wk_add_can_su = -1;
			END IF;
			SET _wk_add_can_kin = _p1_js_uri_king;
		END IF;
		
		/* その他金額 */
		IF _p1_cncl_kubn = '2' THEN
			SET _wk_etc_kin = _p1_js_uri_king;
		END IF;
			
	/***************************************************************/
	/* 売上レポート実績：売上明細 */
	/***************************************************************/
	/* 売上実績Ｆ[店舗実績] */
	
	SET _wk_count = 0;
	SELECT count(*)
				 INTO _wk_count
			FROM JIS_URI_MISES
			WHERE JS_YYYYMM     = _p1_keid_date_ym
			AND JS_EGTN_CODE   	= _p1_js_egtn_code
			AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
	;
	
	/* 売上実績Ｆ[店舗実績]－追加処理 */
	IF _wk_count = 0 THEN
		-- INSERT
		INS_JIS_URI_MISES:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO JIS_URI_MISES (
                               JS_YYYYMM
                             , JS_EGTN_CODE
                             , JS_SYAHAN_KUBN
                             , JS_SIBU_CODE
                             , TNCHO_CODE
                             , ACT_SYAIN_SU
                             , SINKI_URI_CNT
                             , TUIKA_URI_CNT
                             , SINKI_CNCL_CNT
                             , TUIKA_CNCL_CNT
                             , SINKI_URI_KING
                             , TUIKA_URI_KING
                             , SINKI_CNCL_KING
                             , TUIKA_CNCL_KING
                             , ETC_KING
                             , YOBI01_FLAG
                             , YOBI02_FLAG
                             , YOBI03_FLAG
                             , YOBI01_KUBN
                             , YOBI02_KUBN
                             , YOBI03_KUBN
                             , YOBI01_CNT
                             , YOBI02_CNT
                             , YOBI03_CNT
                             , YOBI01_KING
                             , YOBI02_KING
                             , YOBI03_KING
                             , CRT_DATE
                             , CRT_PGM_ID
                             , CRT_USER_ID
                             , CREATED
                             , UPD_PGM_ID
                             , UPD_USER_ID
                             , MODIFIED)
                       VALUES (_p1_keid_date_ym,
                               _p1_js_egtn_code,
                               _wk_js_omit_flag,
                               _p1_js_sibu_code,
                               _p1_tncho_code,
                               0,
                               _wk_new_uri_su,
                               _wk_add_uri_su,
                               _wk_new_can_su,
                               _wk_add_can_su,
                               _wk_new_uri_kin,
                               _wk_add_uri_kin,
                               _wk_new_can_kin,
                               _wk_add_can_kin,
                               _wk_etc_kin,
                               '0',
                               '0',
                               '0',
                               '',
                               '',
                               '',
                               0,
                               0,
                               0,
                               CAST(0 AS DECIMAL(11, 0)),
                               CAST(0 AS DECIMAL(11, 0)),
                               CAST(0 AS DECIMAL(11, 0)),
                               _kei_date,
                               _pgmid,
                               NULL,
                               CURRENT_TIMESTAMP,
                               NULL,
                               NULL,
                               NULL);
		IF SQLCODE = 102 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'INS_JIS_URI_MISES FAILED';
			LEAVE dept_loop;
		END IF;
		END INS_JIS_URI_MISES;
		
	ELSE
		
		-- UPDATE
		UPT_JIS_URI_MISES:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_URI_MISES
			SET SINKI_URI_CNT   	= SINKI_URI_CNT   + _wk_new_uri_su,
					TUIKA_URI_CNT   = TUIKA_URI_CNT   + _wk_add_uri_su,
					SINKI_CNCL_CNT  = SINKI_CNCL_CNT  + _wk_new_can_su,
					TUIKA_CNCL_CNT  = TUIKA_CNCL_CNT  + _wk_add_can_su,
					SINKI_URI_KING  = SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
					TUIKA_URI_KING  = TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
					SINKI_CNCL_KING = SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
					TUIKA_CNCL_KING = TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
					ETC_KING        = ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
					UPD_PGM_ID      = _pgmid,
					UPD_USER_ID     = NULL,
					MODIFIED        = CURRENT_TIMESTAMP
			WHERE JS_YYYYMM      	= _p1_keid_date_ym
				AND JS_EGTN_CODE   	= _p1_js_egtn_code
				AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_URI_MISES FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_URI_MISES;
	END IF;

	/***************************************************************/
	/* 売上実績［店舗組織実績］ */
	/***************************************************************/
	/* 売上実績Ｆ[店舗組織実績] */
	SET _wk_count = 0;
	SELECT count(*)
			 INTO _wk_count
		FROM JIS_URI_MISE_SEPS
		WHERE JS_YYYYMM        	 = _p1_keid_date_ym
			AND JS_EGTN_CODE     = _p1_js_egtn_code
			AND JS_EGTN_SEP_CODE = _p1_js_egtn_sep_code
			AND JS_SYAHAN_KUBN   = _wk_js_omit_flag
	;
	IF _wk_count = 0 THEN
	-- INSERT
		INS_JIS_URI_MISE_SEPS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO JIS_URI_MISE_SEPS (
			   JS_YYYYMM
			 , JS_EGTN_CODE
			 , JS_EGTN_SEP_CODE
			 , JS_SYAHAN_KUBN
			 , JS_SIBU_CODE
			 , TNCHO_CODE
			 , SINKI_URI_CNT
			 , TUIKA_URI_CNT
			 , SINKI_CNCL_CNT
			 , TUIKA_CNCL_CNT
			 , SINKI_URI_KING
			 , TUIKA_URI_KING
			 , SINKI_CNCL_KING
			 , TUIKA_CNCL_KING
			 , ETC_KING
			 , CRT_DATE
			 , CRT_PGM_ID
			 , CRT_USER_ID
			 , CREATED
			 , UPD_PGM_ID
			 , UPD_USER_ID
			 , MODIFIED)
		VALUES (_p1_keid_date_ym,
				 _p1_js_egtn_code,
				 _p1_js_egtn_sep_code,
				 _wk_js_omit_flag,
				 _p1_js_sibu_code,
				 _p1_tncho_code,
				 _wk_new_uri_su,
				 _wk_add_uri_su,
				 _wk_new_can_su,
				 _wk_add_can_su,
				 _wk_new_uri_kin,
				 _wk_add_uri_kin,
				 _wk_new_can_kin,
				 _wk_add_can_kin,
				 _wk_etc_kin,
				 _kei_date,
				 _pgmid,
				 NULL,
				 CURRENT_TIMESTAMP,
				 NULL,
				 NULL,
				 NULL)
		;
		IF SQLCODE = 102 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'INS_JIS_URI_MISE_SEPS FAILED';
			LEAVE dept_loop;
		END IF;
		END INS_JIS_URI_MISE_SEPS;
		
	ELSE
	
	-- UPDATE
		UPT_JIS_URI_MISE_SEPS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_URI_MISE_SEPS
			SET SINKI_URI_CNT   	= SINKI_URI_CNT   + _wk_new_uri_su,
				TUIKA_URI_CNT   	= TUIKA_URI_CNT   + _wk_add_uri_su,
				SINKI_CNCL_CNT  	= SINKI_CNCL_CNT  + _wk_new_can_su,
				TUIKA_CNCL_CNT  	= TUIKA_CNCL_CNT  + _wk_add_can_su,
				SINKI_URI_KING  	= SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
				TUIKA_URI_KING  	= TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
				SINKI_CNCL_KING 	= SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
				TUIKA_CNCL_KING 	= TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
				ETC_KING        	= ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
				UPD_PGM_ID      	= _pgmid,
				UPD_USER_ID     	= NULL,
				MODIFIED        	= CURRENT_TIMESTAMP
			WHERE JS_YYYYMM        	= _p1_keid_date_ym
			AND JS_EGTN_CODE     	= _p1_js_egtn_code
			AND JS_EGTN_SEP_CODE 	= _p1_js_egtn_sep_code
			AND JS_SYAHAN_KUBN   	= _wk_js_omit_flag
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_URI_MISE_SEPS FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_URI_MISE_SEPS;
	END IF;
	
	/***************************************************************/
	/* 売上実績［個人実績］ */
	/***************************************************************/
	/* 売上実績Ｆ[個人実績] */
	SET _wk_count = 0;
	SELECT count(*)
				INTO _wk_count
				FROM JIS_URI_KOJNS
			WHERE JS_YYYYMM     = _p1_keid_date_ym
			AND JS_TANT_CODE   	= _p1_tant4_code
			AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
	;
	
	/* 売上実績Ｆ[個人実績]－追加処理 */
	IF _wk_count = 0 THEN
	-- INSERT
		INS_JIS_URI_KOJNS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO JIS_URI_KOJNS (
					  JS_YYYYMM
					, JS_TANT_CODE
					, JS_SYAHAN_KUBN
					, JS_SIBU_CODE
					, JS_EGTN_CODE
					, SO_TANT_YAKU_CODE
					, SO_TANT_YAKU_LVL
					, SO_TANT_JOTAI_CODE
					, SO_TANT_JIYU_CODE
					, SINKI_URI_CNT
					, TUIKA_URI_CNT
					, SINKI_CNCL_CNT
					, TUIKA_CNCL_CNT
					, SINKI_URI_KING
					, TUIKA_URI_KING
					, SINKI_CNCL_KING
					, TUIKA_CNCL_KING
					, ETC_KING
					, YOBI01_FLAG
					, YOBI02_FLAG
					, YOBI03_FLAG
					, YOBI01_KUBN
					, YOBI02_KUBN
					, YOBI03_KUBN
					, YOBI01_CNT
					, YOBI02_CNT
					, YOBI03_CNT
					, YOBI01_KING
					, YOBI02_KING
					, YOBI03_KING
					, CRT_DATE
					, CRT_PGM_ID
					, CRT_USER_ID
					, CREATED
					, UPD_PGM_ID
					, UPD_USER_ID
					, MODIFIED)
		 VALUES (
					_p1_keid_date_ym,
					_p1_tant4_code,
					_wk_js_omit_flag,
					_p1_js_sibu_code,
					_p1_js_egtn_code,
					_p1_so_tant_yaku_code,
					_p1_so_tant_yaku_lvl,
					_p1_so_tant_jotai_code,
					_p1_so_tant_jiyu_code,
					_wk_new_uri_su,
					_wk_add_uri_su,
					_wk_new_can_su,
					_wk_add_can_su,
					_wk_new_uri_kin,
					_wk_add_uri_kin,
					_wk_new_can_kin,
					_wk_add_can_kin,
					_wk_etc_kin,
					'0',
					'0',
					'0',
					'',
					'',
					'',
					0,
					0,
					0,
					CAST(0 AS DECIMAL(11, 0)),
					CAST(0 AS DECIMAL(11, 0)),
					CAST(0 AS DECIMAL(11, 0)),
					_kei_date,
					_pgmid,
					NULL,
					CURRENT_TIMESTAMP,
					NULL,
					NULL,
					NULL)
		;
		IF SQLCODE = 102 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'INS_JIS_URI_KOJNS FAILED';
			LEAVE dept_loop;
		END IF;
		END INS_JIS_URI_KOJNS;
		
	ELSE
	
	-- UPDATE
		UPT_JIS_URI_KOJNS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_URI_KOJNS
				SET SINKI_URI_CNT  		= SINKI_URI_CNT   + _wk_new_uri_su,
					TUIKA_URI_CNT   	= TUIKA_URI_CNT   + _wk_add_uri_su,
					SINKI_CNCL_CNT  	= SINKI_CNCL_CNT  + _wk_new_can_su,
					TUIKA_CNCL_CNT  	= TUIKA_CNCL_CNT  + _wk_add_can_su,
					SINKI_URI_KING  	= SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
					TUIKA_URI_KING  	= TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
					SINKI_CNCL_KING 	= SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
					TUIKA_CNCL_KING 	= TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
					ETC_KING        	= ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
					UPD_PGM_ID      	= _pgmid,
					UPD_USER_ID     	= NULL,
					MODIFIED        	= CURRENT_TIMESTAMP -- UPD_TS
				WHERE JS_YYYYMM      	= _p1_keid_date_ym
					AND JS_TANT_CODE   	= _p1_tant4_code
					AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_URI_KOJNS FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_URI_KOJNS;
	END IF;
		
	/***************************************************************/
	/* 売上実績Ｆ[顧客実績] */
	/***************************************************************/
	SET _wk_count = 0;
	SELECT count(*)
			INTO _wk_count
			FROM JIS_URI_KKYKS
		WHERE JS_YYYYMM     = _p1_keid_date_ym
		AND JS_SIBU_CODE   	= _p1_js_sibu_code
		AND JS_EGTN_CODE   	= _p1_js_egtn_code
		AND JS_TANT_CODE   	= _p1_tant4_code
		AND JS_KKYK_CODE   	= _p1_kkyk_code
		AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
	;
	/* 売上実績Ｆ[顧客実績]－追加処理 */
	IF _wk_count = 0 THEN
		-- INSERT
		INS_JIS_URI_KKYKS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO JIS_URI_KKYKS (
                               JS_YYYYMM
                             , JS_SIBU_CODE
                             , JS_EGTN_CODE
                             , JS_TANT_CODE
                             , JS_KKYK_CODE
                             , JS_SYAHAN_KUBN
                             , SINKI_URI_CNT
                             , TUIKA_URI_CNT
                             , SINKI_CNCL_CNT
                             , TUIKA_CNCL_CNT
                             , SINKI_URI_KING
                             , TUIKA_URI_KING
                             , SINKI_CNCL_KING
                             , TUIKA_CNCL_KING
                             , ETC_KING
                             , YOBI01_FLAG
                             , YOBI02_FLAG
                             , YOBI03_FLAG
                             , YOBI01_KUBN
                             , YOBI02_KUBN
                             , YOBI03_KUBN
                             , YOBI01_CNT
                             , YOBI02_CNT
                             , YOBI03_CNT
                             , YOBI01_KING
                             , YOBI02_KING
                             , YOBI03_KING
                             , CRT_DATE
                             , CRT_PGM_ID
                             , CRT_USER_ID
                             , CREATED
                             , UPD_PGM_ID
                             , UPD_USER_ID
                             , MODIFIED)
                       values (_p1_keid_date_ym
                             , _p1_js_sibu_code
                             , _p1_js_egtn_code
                             , _p1_tant4_code
                             , _p1_kkyk_code
                             , _wk_js_omit_flag
                             , _wk_new_uri_su
                             , _wk_add_uri_su
                             , _wk_new_can_su
                             , _wk_add_can_su
                             , _wk_new_uri_kin
                             , _wk_add_uri_kin
                             , _wk_new_can_kin
                             , _wk_add_can_kin
                             , _wk_etc_kin
                             , '0'
                             , '0'
                             , '0'
                             , ''
                             , ''
                             , ''
                             , 0
                             , 0
                             , 0
                             , CAST(0 AS DECIMAL(11,0))
                             , CAST(0 AS DECIMAL(11,0))
                             , CAST(0 AS DECIMAL(11,0))
                             , _kei_date
                             , _pgmid
                             , NULL
                             , CURRENT_TIMESTAMP
                             , NULL
                             , NULL
                             , NULL);
		IF SQLCODE = 102 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'INS_JIS_URI_KKYKS FAILED';
			LEAVE dept_loop;
		END IF;
		END INS_JIS_URI_KKYKS;
	ELSE
		-- UPDATE
		/* 売上実績Ｆ[顧客実績]－更新処理 */
		UPT_JIS_URI_KKYKS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_URI_KKYKS
			SET SINKI_URI_CNT   	= SINKI_URI_CNT   + _wk_new_uri_su,
				TUIKA_URI_CNT   	= TUIKA_URI_CNT   + _wk_add_uri_su,
				SINKI_CNCL_CNT  	= SINKI_CNCL_CNT  + _wk_new_can_su,
				TUIKA_CNCL_CNT  	= TUIKA_CNCL_CNT  + _wk_add_can_su,
				SINKI_URI_KING  	= SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
				TUIKA_URI_KING  	= TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
				SINKI_CNCL_KING 	= SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
				TUIKA_CNCL_KING 	= TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
				ETC_KING        	= ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
				UPD_PGM_ID      	= _pgmid,
				UPD_USER_ID     	= NULL,
				MODIFIED        	= CURRENT_TIMESTAMP
			WHERE JS_YYYYMM      	= _p1_keid_date_ym
				AND JS_SIBU_CODE   	= _p1_js_sibu_code
				AND JS_EGTN_CODE   	= _p1_js_egtn_code
				AND JS_TANT_CODE   	= _p1_tant4_code
				AND JS_KKYK_CODE   	= _p1_kkyk_code
				AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_URI_KKYKS FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_URI_KKYKS;
	END IF;
	
	/***************************************************************/
	/* 条件Ａ  売上実績［管理実績］－担当者１ＣＤ */
	/***************************************************************/
	IF _p1_tant1_flag = '1' THEN
		SET _wk_count = 0;
		SELECT count(*)
				INTO _wk_count
			FROM JIS_URI_KANRS
			WHERE JS_YYYYMM      	= _p1_keid_date_ym
				AND JS_TANT_CODE   	= _p1_tant1_code
				AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
		;
		IF _wk_count = 0 THEN
		
			SET _p2_so_tant_yaku_lvl 	= 0;
      		SET _p2_so_tant_yaku_code 	= 0;
      		SET _p2_so_tant_jotai_code	= 0;
			SET _p2_so_tant_jiyu_code 	= '\0';
			
			SELECT SO_TANT_YAKU_LVL
					, SO_TANT_YAKU_CODE
					, SO_TANT_JOTAI_CODE
					, SO_TANT_JIYU_CODE
				INTO _p2_so_tant_yaku_lvl
					, _p2_so_tant_yaku_code
					, _p2_so_tant_jotai_code
					, _p2_so_tant_jiyu_code
				FROM MST_TANTOUS
				WHERE TANT_CODE = _p1_tant1_code
			;
			
			-- INSERT
			INS_JIS_URI_KANRS_1:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 102;
			END;
			INSERT INTO JIS_URI_KANRS (
                                 JS_YYYYMM
                               , JS_TANT_CODE
                               , JS_SYAHAN_KUBN
                               , JS_SIBU_CODE
                               , JS_EGTN_CODE
                               , SO_TANT_YAKU_CODE
                               , SO_TANT_YAKU_LVL
                               , SO_TANT_JOTAI_CODE
                               , SO_TANT_JIYU_CODE
                               , SINKI_URI_CNT
                               , TUIKA_URI_CNT
                               , SINKI_CNCL_CNT
                               , TUIKA_CNCL_CNT
                               , SINKI_URI_KING
                               , TUIKA_URI_KING
                               , SINKI_CNCL_KING
                               , TUIKA_CNCL_KING
                               , ETC_KING
                               , YOBI01_FLAG
                               , YOBI02_FLAG
                               , YOBI03_FLAG
                               , YOBI01_KUBN
                               , YOBI02_KUBN
                               , YOBI03_KUBN
                               , YOBI01_CNT
                               , YOBI02_CNT
                               , YOBI03_CNT
                               , YOBI01_KING
                               , YOBI02_KING
                               , YOBI03_KING
                               , CRT_DATE
                               , CRT_PGM_ID
                               , CRT_USER_ID
                               , CREATED
                               , UPD_PGM_ID
                               , UPD_USER_ID
                               , MODIFIED)
                         values (_p1_keid_date_ym,
                                 _p1_tant1_code,
                                 _wk_js_omit_flag,
                                 _p1_js_sibu_code,
                                 _p1_js_egtn_code,
                                 _p2_so_tant_yaku_code,
                                 _p2_so_tant_yaku_lvl,
                                 _p2_so_tant_jotai_code,
                                 _p2_so_tant_jiyu_code,
                                 _wk_new_uri_su,
                                 _wk_add_uri_su,
                                 _wk_new_can_su,
                                 _wk_add_can_su,
                                 _wk_new_uri_kin,
                                 _wk_add_uri_kin,
                                 _wk_new_can_kin,
                                 _wk_add_can_kin,
                                 _wk_etc_kin,
                                 '0',
                                 '0',
                                 '0',
                                 '',
                                 '',
                                 '',
                                 0,
                                 0,
                                 0,
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 _kei_date,
                                 _pgmid,
                                 NULL,
                                 CURRENT_TIMESTAMP,
                                 NULL,
                                 NULL,
                                 NULL)
			;
			IF SQLCODE = 102 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'INS_JIS_URI_KANRS_1 FAILED';
				LEAVE dept_loop;
			END IF;
			END INS_JIS_URI_KANRS_1;
			
			ELSE
			-- UPDATE
			UPT_JIS_URI_KANRS_1:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 101;
			END;
			UPDATE JIS_URI_KANRS
                        SET SINKI_URI_CNT   = SINKI_URI_CNT   + _wk_new_uri_su,
                            TUIKA_URI_CNT   = TUIKA_URI_CNT   + _wk_add_uri_su,
                            SINKI_CNCL_CNT  = SINKI_CNCL_CNT  + _wk_new_can_su,
                            TUIKA_CNCL_CNT  = TUIKA_CNCL_CNT  + _wk_add_can_su,
                            SINKI_URI_KING  = SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
                            TUIKA_URI_KING  = TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
                            SINKI_CNCL_KING = SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
                            TUIKA_CNCL_KING = TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
                            ETC_KING        = ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
                            UPD_PGM_ID      = _pgmid,
                            UPD_USER_ID     = NULL,
                            MODIFIED        = CURRENT_TIMESTAMP
                      WHERE JS_YYYYMM      	= _p1_keid_date_ym
                        AND JS_TANT_CODE   	= _p1_tant1_code
                        AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
			;
			IF SQLCODE = 101 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'UPT_JIS_URI_KANRS_1 FAILED';
				LEAVE dept_loop;
			END IF;
			END UPT_JIS_URI_KANRS_1;
		END IF;
		
	END IF;
	
	/***************************************************************/
	/* 条件Ｂ  売上実績［管理実績］－担当者２ＣＤ */
	/***************************************************************/
	IF _p1_tant2_flag = '1' THEN
		SET _wk_count = 0;
		SELECT count(*)
				 INTO _wk_count
				 FROM JIS_URI_KANRS
			 WHERE JS_YYYYMM      	= _p1_keid_date_ym
				 AND JS_TANT_CODE   = _p1_tant2_code
				 AND JS_SYAHAN_KUBN = _wk_js_omit_flag
		;
		IF _wk_count = 0 THEN
		
			SET _p2_so_tant_yaku_lvl = 0;
      		SET _p2_so_tant_yaku_code = 0;
      		SET _p2_so_tant_jotai_code = 0;
			SET _p2_so_tant_jiyu_code = '\0';
			
			SELECT SO_TANT_YAKU_LVL
					, SO_TANT_YAKU_CODE
					, SO_TANT_JOTAI_CODE
					, SO_TANT_JIYU_CODE
				INTO _p2_so_tant_yaku_lvl
					, _p2_so_tant_yaku_code
					, _p2_so_tant_jotai_code
					, _p2_so_tant_jiyu_code
				FROM MST_TANTOUS
				WHERE TANT_CODE = _p1_tant2_code
			;
			
			-- INSERT
			INS_JIS_URI_KANRS_2:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 102;
			END;
			INSERT INTO JIS_URI_KANRS (
                                 JS_YYYYMM
                               , JS_TANT_CODE
                               , JS_SYAHAN_KUBN
                               , JS_SIBU_CODE
                               , JS_EGTN_CODE
                               , SO_TANT_YAKU_CODE
                               , SO_TANT_YAKU_LVL
                               , SO_TANT_JOTAI_CODE
                               , SO_TANT_JIYU_CODE
                               , SINKI_URI_CNT
                               , TUIKA_URI_CNT
                               , SINKI_CNCL_CNT
                               , TUIKA_CNCL_CNT
                               , SINKI_URI_KING
                               , TUIKA_URI_KING
                               , SINKI_CNCL_KING
                               , TUIKA_CNCL_KING
                               , ETC_KING
                               , YOBI01_FLAG
                               , YOBI02_FLAG
                               , YOBI03_FLAG
                               , YOBI01_KUBN
                               , YOBI02_KUBN
                               , YOBI03_KUBN
                               , YOBI01_CNT
                               , YOBI02_CNT
                               , YOBI03_CNT
                               , YOBI01_KING
                               , YOBI02_KING
                               , YOBI03_KING
                               , CRT_DATE
                               , CRT_PGM_ID
                               , CRT_USER_ID
                               , CREATED
                               , UPD_PGM_ID
                               , UPD_USER_ID
                               , MODIFIED)
                         values (_p1_keid_date_ym,
                                 _p1_tant2_code,
                                 _wk_js_omit_flag,
                                 _p1_js_sibu_code,
                                 _p1_js_egtn_code,
                                 _p2_so_tant_yaku_code,
                                 _p2_so_tant_yaku_lvl,
                                 _p2_so_tant_jotai_code,
                                 _p2_so_tant_jiyu_code,
                                 _wk_new_uri_su,
                                 _wk_add_uri_su,
                                 _wk_new_can_su,
                                 _wk_add_can_su,
                                 _wk_new_uri_kin,
                                 _wk_add_uri_kin,
                                 _wk_new_can_kin,
                                 _wk_add_can_kin,
                                 _wk_etc_kin,
                                 '0',
                                 '0',
                                 '0',
                                 '',
                                 '',
                                 '',
                                 0,
                                 0,
                                 0,
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 _kei_date,
                                 _pgmid,
                                 NULL,
                                 CURRENT_TIMESTAMP,
                                 NULL,
                                 NULL,
                                 NULL);
			IF SQLCODE = 102 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'INS_JIS_URI_KANRS_2 FAILED';
				LEAVE dept_loop;
			END IF;
			END INS_JIS_URI_KANRS_2;
		 
			ELSE

			-- UPDATE
			UPT_JIS_URI_KANRS_2:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 101;
			END;
			UPDATE JIS_URI_KANRS
                        SET SINKI_URI_CNT   = SINKI_URI_CNT   + _wk_new_uri_su,
                            TUIKA_URI_CNT   = TUIKA_URI_CNT   + _wk_add_uri_su,
                            SINKI_CNCL_CNT  = SINKI_CNCL_CNT  + _wk_new_can_su,
                            TUIKA_CNCL_CNT  = TUIKA_CNCL_CNT  + _wk_add_can_su,
                            SINKI_URI_KING  = SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
                            TUIKA_URI_KING  = TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
                            SINKI_CNCL_KING = SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
                            TUIKA_CNCL_KING = TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
                            ETC_KING        = ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
                            UPD_PGM_ID      = _pgmid,
                            UPD_USER_ID     = NULL,
                            UPD_TS          = CURRENT_TIMESTAMP
                      WHERE JS_YYYYMM      	= _p1_keid_date_ym
                        AND JS_TANT_CODE   	= _p1_tant2_code
                        AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
			;
			IF SQLCODE = 101 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'UPT_JIS_URI_KANRS_2 FAILED';
				LEAVE dept_loop;
			END IF;
			END UPT_JIS_URI_KANRS_2;
		END IF;
	END IF;
	
	/***************************************************************/
	/* 条件Ｃ  売上実績［管理実績］－担当者３ＣＤ */
	/***************************************************************/
	IF _p1_tant3_flag = '1' THEN
		SET _wk_count = 0;
		SELECT count(*)
                   INTO _wk_count
                   FROM JIS_URI_KANRS
				WHERE JS_YYYYMM     = _p1_keid_date_ym
				AND JS_TANT_CODE   	= _p1_tant3_code
				AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag;
		IF _wk_count = 0 THEN
		
			SET _p2_so_tant_yaku_lvl = 0;
      SET _p2_so_tant_yaku_code = 0;
      SET _p2_so_tant_jotai_code = 0;
      SET _p2_so_tant_jiyu_code = '\0';
			
			SELECT SO_TANT_YAKU_LVL
					, SO_TANT_YAKU_CODE
					, SO_TANT_JOTAI_CODE
					, SO_TANT_JIYU_CODE
				INTO _p2_so_tant_yaku_lvl
					, _p2_so_tant_yaku_code
					, _p2_so_tant_jotai_code
					, _p2_so_tant_jiyu_code
				FROM MST_TANTOUS
				WHERE TANT_CODE = _p1_tant3_code
			;
			
			-- INSERT
			INS_JIS_URI_KANRS_3:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 102;
			END;
			INSERT INTO JIS_URI_KANRS (
                                 JS_YYYYMM
                               , JS_TANT_CODE
                               , JS_SYAHAN_KUBN
                               , JS_SIBU_CODE
                               , JS_EGTN_CODE
                               , SO_TANT_YAKU_CODE
                               , SO_TANT_YAKU_LVL
                               , SO_TANT_JOTAI_CODE
                               , SO_TANT_JIYU_CODE
                               , SINKI_URI_CNT
                               , TUIKA_URI_CNT
                               , SINKI_CNCL_CNT
                               , TUIKA_CNCL_CNT
                               , SINKI_URI_KING
                               , TUIKA_URI_KING
                               , SINKI_CNCL_KING
                               , TUIKA_CNCL_KING
                               , ETC_KING
                               , YOBI01_FLAG
                               , YOBI02_FLAG
                               , YOBI03_FLAG
                               , YOBI01_KUBN
                               , YOBI02_KUBN
                               , YOBI03_KUBN
                               , YOBI01_CNT
                               , YOBI02_CNT
                               , YOBI03_CNT
                               , YOBI01_KING
                               , YOBI02_KING
                               , YOBI03_KING
                               , CRT_DATE
                               , CRT_PGM_ID
                               , CRT_USER_ID
                               , CREATED
                               , UPD_PGM_ID
                               , UPD_USER_ID
                               , MODIFIED)
                         values (_p1_keid_date_ym,
                                 _p1_tant3_code,
                                 _wk_js_omit_flag,
                                 _p1_js_sibu_code,
                                 _p1_js_egtn_code,
                                 _p2_so_tant_yaku_code,
                                 _p2_so_tant_yaku_lvl,
								 _p2_so_tant_jotai_code,
								 _p2_so_tant_jiyu_code,
                                 _wk_new_uri_su,
                                 _wk_add_uri_su,
                                 _wk_new_can_su,
                                 _wk_add_can_su,
                                 _wk_new_uri_kin,
                                 _wk_add_uri_kin,
                                 _wk_new_can_kin,
                                 _wk_add_can_kin,
                                 _wk_etc_kin,
                                 '0',
                                 '0',
                                 '0',
                                 '',
                                 '',
                                 '',
                                 0,
                                 0,
                                 0,
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 CAST(0 AS DECIMAL(11,0)),
                                 _kei_date,
                                 _pgmid,
                                 NULL,
                                 CURRENT_TIMESTAMP,
                                 NULL,
                                 NULL,
                                 NULL);
			IF SQLCODE = 102 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'INS_JIS_URI_KANRS_3 FAILED';
				LEAVE dept_loop;
			END IF;
			END INS_JIS_URI_KANRS_3;
			
		ELSE
		-- UPDATE
		UPT_JIS_URI_KANRS_3:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_URI_KANRS
                        SET SINKI_URI_CNT   = SINKI_URI_CNT   + _wk_new_uri_su,
                            TUIKA_URI_CNT   = TUIKA_URI_CNT   + _wk_add_uri_su,
                            SINKI_CNCL_CNT  = SINKI_CNCL_CNT  + _wk_new_can_su,
                            TUIKA_CNCL_CNT  = TUIKA_CNCL_CNT  + _wk_add_can_su,
                            SINKI_URI_KING  = SINKI_URI_KING  + CAST(_wk_new_uri_kin AS DECIMAL(11,0)),
                            TUIKA_URI_KING  = TUIKA_URI_KING  + CAST(_wk_add_uri_kin AS DECIMAL(11,0)),
                            SINKI_CNCL_KING = SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0)),
                            TUIKA_CNCL_KING = TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0)),
                            ETC_KING        = ETC_KING        + CAST(_wk_etc_kin AS DECIMAL(11,0)),
                            UPD_PGM_ID      = _pgmid,
                            UPD_USER_ID     = NULL,
                            MODIFIED        = CURRENT_TIMESTAMP
                      WHERE JS_YYYYMM      	= _p1_keid_date_ym
                        AND JS_TANT_CODE   	= _p1_tant3_code
                        AND JS_SYAHAN_KUBN 	= _wk_js_omit_flag
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_URI_KANRS_3 FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_URI_KANRS_3;		
		END IF;
	END IF;
	
	/***************************************************************/
	/* 条件Ｄ  売上実績［管理実績］－担当者４ＣＤ */
	/***************************************************************/
	IF	strcmp(_p1_tant4_code, _p1_tant1_code) <> 0 AND
			strcmp(_p1_tant4_code, _p1_tant2_code) <> 0 AND
			strcmp(_p1_tant4_code, _p1_tant3_code) <> 0 THEN
		
		SET _wk_count = 0;
		SELECT count(*)
                   INTO _wk_count
                   FROM JIS_TOTAL_KANRS
                  WHERE URI_KUBN           = '0'
                    AND KEID_DATE          = _p1_keid_date
                    AND KONY_EGTN_CODE     = _p1_js_egtn_code
                    AND KONY_EGTN_SEP_CODE = _p1_js_egtn_sep_code
                    AND TANT_CODE          = _p1_tant4_code
                    AND SYAHAN_KUBN        = _wk_js_omit_flag
                    AND CHARMS_KUBN        = _p3_charms_kubn;
		IF _wk_count = 0 THEN
		-- INSERT
		INS_JIS_TOTAL_KANRS_4:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO JIS_TOTAL_KANRS (
                               URI_KUBN
                             , KEID_DATE
                             , KONY_EGTN_CODE
                             , KONY_EGTN_SEP_CODE
                             , TANT_CODE
                             , SYAHAN_KUBN
                             , CHARMS_KUBN
                             , SINKI_URI_CNT
                             , TUIKA_URI_CNT
                             , SINKI_CNCL_CNT
                             , TUIKA_CNCL_CNT
                             , SINKI_URI_KING
                             , TUIKA_URI_KING
                             , SINKI_CNCL_KING
                             , TUIKA_CNCL_KING
                             , SYNC_FLAG
                             , CRT_PGM_ID
                             , CRT_USER_ID
                             , CREATED
                             , UPD_PGM_ID
                             , UPD_USER_ID
                             , MODIFIED)
                       VALUES ('0'
                             , _p1_keid_date
                             , _p1_js_egtn_code
                             , _p1_js_egtn_sep_code
                             , _p1_tant4_code
                             , _wk_js_omit_flag
                             , _p3_charms_kubn
                             , _wk_new_uri_su
                             , _wk_add_uri_su
                             , _wk_new_can_su
                             , _wk_add_can_su
                             , _wk_new_uri_kin_etc
                             , _wk_add_uri_kin_etc
                             , _wk_new_can_kin
                             , _wk_add_can_kin
                             , '0'
                             , _pgmid
                             , NULL
                             , CURRENT_TIMESTAMP
                             , NULL
                             , NULL
                             , NULL);
			IF SQLCODE = 102 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'INS_JIS_TOTAL_KANRS_4 FAILED';
				LEAVE dept_loop;
			END IF;
			END INS_JIS_TOTAL_KANRS_4;
		ELSE
		-- UPDATE
		UPT_JIS_TOTAL_KANRS_4:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE JIS_TOTAL_KANRS
				SET SINKI_URI_CNT   	= SINKI_URI_CNT   + _wk_new_uri_su
					, TUIKA_URI_CNT   	= TUIKA_URI_CNT   + _wk_add_uri_su
					, SINKI_CNCL_CNT  	= SINKI_CNCL_CNT  + _wk_new_can_su
					, TUIKA_CNCL_CNT  	= TUIKA_CNCL_CNT  + _wk_add_can_su
					, SINKI_URI_KING  	= SINKI_URI_KING  + CAST(_wk_new_uri_kin_etc AS DECIMAL(11,0))
					, TUIKA_URI_KING  	= TUIKA_URI_KING  + CAST(_wk_add_uri_kin_etc AS DECIMAL(11,0))
					, SINKI_CNCL_KING 	= SINKI_CNCL_KING + CAST(_wk_new_can_kin AS DECIMAL(11,0))
					, TUIKA_CNCL_KING 	= TUIKA_CNCL_KING + CAST(_wk_add_can_kin AS DECIMAL(11,0))
					, SYNC_FLAG       	= '0'
					, UPD_PGM_ID      	= _pgmid
					, UPD_USER_ID     	= NULL
					, MODIFIED        	= CURRENT_TIMESTAMP
				WHERE URI_KUBN         	= '0'
				AND KEID_DATE          	= _p1_keid_date
				AND KONY_EGTN_CODE     	= _p1_js_egtn_code
				AND KONY_EGTN_SEP_CODE 	= _p1_js_egtn_sep_code
				AND TANT_CODE          	= _p1_tant4_code
				AND SYAHAN_KUBN        	= _wk_js_omit_flag
				AND CHARMS_KUBN        	= _p3_charms_kubn
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_JIS_TOTAL_KANRS_4 FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_JIS_TOTAL_KANRS_4;
		END IF;
	END IF;
	
	
	
	/* 売上レポート実績：売上明細更新 */
	UPT_TRN_REPORT_URIS_END:BEGIN
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		 SET SQLCODE = 101;
	END;
	UPDATE TRN_REPORT_URIS
		SET DAILY_FLG    = '1',
			DAILY_KEI_YM = _p1_keid_date_ym,
			DAILY_TS     = CURRENT_TIMESTAMP
		WHERE KESUR_FLAG = '2'
		AND DAILY_FLG  = '0'
	;
	IF SQLCODE = 101 THEN 
		SET fnReturn = KFAIL;
		SET fnMsg = 'UPT_TRN_REPORT_URIS_END FAILED';
		LEAVE dept_loop;
	END IF;
	END UPT_TRN_REPORT_URIS_END;
		
	END WHILE dept_loop;
	CLOSE DYHB808_01;
	
	END CUR;
END MAIN_PROC
CREATE DEFINER=`root`@`localhost` PROCEDURE `DYHB230`(
	IN IN_CTL_KEY CHAR(8),
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYHB230
	-- 機能		: 	会員制度[ブリエ会員02]集計－申込期間内ＣＯ未満了
	-- 著者		: 	TBinh-PrimeLabo
	-- パラメータ	:		(string) IN_CTL_KEY = 'CAN-BBUP'
	-- 作成日	: 	2019-02-16
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _kei_date CHAR(11);

	/* 業務コントロール */
	DECLARE _can_bbup INT;

	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* 初期処理 */
	/***************************************************************/
	
	SET _kei_date = '\0';
	SELECT 
		KEI_DATE
		INTO _kei_date
	FROM MST_HIZS
	WHERE MST_HIZS.DM_KEY = _DM_KEY
	;
	-- 
	IF _kei_date = '\0' THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* 業務コントロール表参照 */
	SET _can_bbup = 0;
	SELECT 
		CTL_DATA
		INTO _can_bbup
	FROM MST_CTLS
	WHERE MST_CTLS.CTL_KEY = IN_CTL_KEY -- SAMPLE: CAN-BBUP
	;
	--
	IF _can_bbup = 0 THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2. IsNotFound [`CTL_KEY`='ss']","ss",IN_CTL_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/***************************************************************/
	/* 日別店舗別未納品金額削除 */
	/***************************************************************/
	DEL_TRN_MINO_MISE_DAILYS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 103;
		END;
		DELETE FROM TRN_MINO_MISE_DAILYS
		WHERE KEID_DATE = date(_kei_date)
		;
		IF SQLCODE = 103 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'DEL_TRN_MINO_MISE_DAILYS FAILED';
			LEAVE MAIN_PROC;
		END IF;
	END DEL_TRN_MINO_MISE_DAILYS;

	/***************************************************************/
	/* 購入申込ＢＢ */
	/***************************************************************/
	
	/* 購入申込ＢＢ読込み */
	CUR:BEGIN
		
	/* 購入申込ＢＢ */
	DECLARE _p1_kony_egtn_code CHAR(7)	; /* 購入営業店CD */
	DECLARE _p1_syahan_kubn CHAR(2)		; /* 社販区分（社員区分＝１は０ 以外は１） */
	DECLARE _p1_uri_king DOUBLE			; /* 値引後金額 */
	DECLARE _p1_uri_tax DOUBLE         	; /* 仮受消費税 */

	/* 作業項目 */
	DECLARE _wk_count INT				; /* 件数（存在チェック用） */
	
	DECLARE CHB230_01 CURSOR FOR
	SELECT KONY_EGTN_CODE
					, CASE WHEN SYIN_KUBN = '0' THEN '0' ELSE '1' END
					, sum(URI_KING)
					, sum(URI_TAX)
					FROM TRN_KONY_BBS
				 WHERE (KESUR_FLAG = '2' AND KKYK_HKWTS_FLAG = '0' AND DEL_FLAG <> 'D')
						OR (KESUR_FLAG = '2' AND KKYK_HKWTS_FLAG = '0' AND DEL_FLAG =  'D' AND CNCL_GYNUM = 0 AND TESEI_KUBN = '0' AND REODR_FLAG = '0' AND VERNUM < _can_bbup)
			GROUP BY KONY_EGTN_CODE, SYIN_KUBN
	;
	
	DECLARE EXIT HANDLER FOR NOT FOUND
	BEGIN
		SET SQLCODE = 100;
	END;
	
	OPEN CHB230_01;
	dept_loop:WHILE(SQLCODE=0) DO
	
		SET _p1_kony_egtn_code 	= '\0';
		SET _p1_syahan_kubn 	= '\0';
		SET _p1_uri_king 		= 0;
		SET _p1_uri_tax 		= 0;
		
		FETCH FROM CHB230_01
			INTO
				_p1_kony_egtn_code
				, _p1_syahan_kubn
				, _p1_uri_king
				, _p1_uri_tax
		;
		
		IF _p1_kony_egtn_code = '\0' THEN
			SET SQLCODE = 100;
		END IF;
		
		IF SQLCODE = 100 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'CURSOR IS EMPTY';
			LEAVE dept_loop;
		END IF;
		
	/***************************************************************/
	/* 購入申込ＢＢ */
	/***************************************************************/
	/* 日別店舗別未納品金額件数取得 */
	SET _wk_count = 0;
	
	SELECT count(*)
				 INTO _wk_count
				 FROM TRN_MINO_MISE_DAILYS
			WHERE KEID_DATE     = DATE(_kei_date)
			AND KONY_EGTN_CODE 	= _p1_kony_egtn_code
			AND SYAHAN_KUBN    	= _p1_syahan_kubn
	;
	
	/* 日別店舗別未納品金額の追加処理 */
	IF _wk_count = 0 THEN
		SET SQLCODE = 100;
	END IF;
	
	IF SQLCODE = 100 OR _wk_count = 0 THEN
		-- INSERT
		INS_TRN_MINO_MISE_DAILYS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 102;
		END;
		INSERT INTO TRN_MINO_MISE_DAILYS
						 ( KEID_DATE
						 , KONY_EGTN_CODE
						 , SYAHAN_KUBN
						 , URI_KING
						 , URI_TAX
						 , CRT_DATE
						 , CRT_PGM_ID
						 , CRT_USER_ID
						 , MODIFIED )
			VALUES (DATE(_kei_date)
						 , _p1_kony_egtn_code
						 , _p1_syahan_kubn
						 , CAST(_p1_uri_king AS DECIMAL(11,0))
						 , CAST(_p1_uri_tax AS DECIMAL(9,0))
						 , _kei_date
						 , _pgmid
						 , NULL
						 , CURRENT_TIMESTAMP)
		;
		IF SQLCODE = 102 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'INS_TRN_MINO_MISE_DAILYS FAILED';
			LEAVE dept_loop;
		END IF;
		END INS_TRN_MINO_MISE_DAILYS;
		
	ELSE
		-- UPDATE
		UPT_TRN_MINO_MISE_DAILYS:BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		UPDATE TRN_MINO_MISE_DAILYS
					SET   URI_KING  	= URI_KING + CAST(_p1_uri_king AS DECIMAL(11,0))
						, URI_TAX   	= URI_TAX  + CAST(_p1_uri_tax AS DECIMAL(9,0))
				WHERE KEID_DATE      	= DATE(_kei_date)
					AND KONY_EGTN_CODE 	= _p1_kony_egtn_code
					AND SYAHAN_KUBN    	= _p1_syahan_kubn
		;
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_TRN_MINO_MISE_DAILYS FAILED';
			LEAVE dept_loop;
		END IF;
		END UPT_TRN_MINO_MISE_DAILYS;
		
	END IF;
	END WHILE dept_loop;
	CLOSE CHB230_01;
		
	END CUR;

END MAIN_PROC
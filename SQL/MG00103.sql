CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00103`(
	IN IN_ZAKR_KUBN CHAR(1),
	IN IN_SKMB_KUBN CHAR(2),
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMG00103
	-- 機能		: 	製品以外の原価計算処理１
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-13
	-- =============================================
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 在庫Ｍ（レディ） */
	DECLARE _p0103_1_syhn_code CHAR(9);  /* 商品CD */
	DECLARE _p0103_1_kyu CHAR(2);        /* 級 */
	DECLARE _p0103_1_pmn_zaisury DOUBLE;   /* 前月末在庫数量 */
	DECLARE _p0103_1_pmn_zaiking DOUBLE;   /* 前月末在庫金額 */
	DECLARE _p0103_1_tmn_sirinsu DOUBLE;   /* 当月受入数量（仕入） */
	DECLARE _p0103_1_tmn_sirinkg DOUBLE;   /* 当月受入金額（仕入） */
	DECLARE _p0103_1_tmn_kksinsu DOUBLE;   /* 当月格下入庫数量 */
	DECLARE _p0103_1_tmn_kksinkg DOUBLE;   /* 当月格下入庫金額 */

	/* 級商品別原価マスタ（レディ） */
	DECLARE _p0103_9_syhn_code CHAR(9);  /* 商品コード */
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* 処理１ */
	/***************************************************************/
	
	/* 在庫Ｍ（レディ）読込み */
	CUR: BEGIN
		DECLARE C0103_01 CURSOR FOR
		SELECT SYHN_CODE, KYU,
						 sum(PMN_ZAISURY), sum(PMN_ZAIKING),
						 sum(TMN_SIRINSU), sum(TMN_SIRINKG),
						 sum(TMN_KKSINSU), sum(TMN_KKSINKG) 
				FROM MST_ZAI_RDYS
			 WHERE ZAKR_KUBN =  IN_ZAKR_KUBN -- '1'
				 AND SKMB_KUBN <> IN_SKMB_KUBN -- '01'
		GROUP BY SYHN_CODE, KYU
		ORDER BY SYHN_CODE, KYU;
		
		DECLARE CONTINUE HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE = 100;
		END;
		
		OPEN C0103_01;
		
		dept_loop:WHILE(SQLCODE = 0) DO
		
			SET _p0103_1_syhn_code = '\0';
			SET _p0103_1_kyu = '\0';

			SET _p0103_1_pmn_zaisury = 0;
			SET _p0103_1_pmn_zaiking = 0;
			SET _p0103_1_tmn_sirinsu = 0;
			SET _p0103_1_tmn_sirinkg = 0;
			SET _p0103_1_tmn_kksinsu = 0;
			SET _p0103_1_tmn_kksinkg = 0;
			
			FETCH FROM C0103_01
				INTO 	_p0103_1_syhn_code
						, _p0103_1_kyu
						, _p0103_1_pmn_zaisury
						, _p0103_1_pmn_zaiking
						, _p0103_1_tmn_sirinsu
						, _p0103_1_tmn_sirinkg
						, _p0103_1_tmn_kksinsu
						, _p0103_1_tmn_kksinkg
			;			
			
			IF SQLCODE = 100 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = REPLACE(REPLACE("処理１ IsNotFound [`ZAKR_KUBN`='ss1', SKMB_KUBN='ss2']","ss1",IN_ZAKR_KUBN),"ss2",IN_SKMB_KUBN);
				LEAVE dept_loop;
			END IF;
			
			--
			
			SET _p0103_9_syhn_code = '\0';
			SELECT SYHN_CODE
					 INTO _p0103_9_syhn_code
			FROM MST_SYHN_GENK_RDYS
			WHERE SYHN_CODE = _p0103_1_syhn_code
				AND KYU       = _p0103_1_kyu
			;
			
			--
			
			IF _p0103_9_syhn_code = '\0' THEN 
				SET SQLCODE = 100; 
			END IF;
			
			IF SQLCODE = 100 THEN
				UPT_MST_SYHN_GENK_RDYS:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 101;
				END;
				UPDATE MST_SYHN_GENK_RDYS
						SET PMN_ZAISURY    = CAST((CAST(PMN_ZAISURY AS DECIMAL(11,2)) + CAST(_p0103_1_pmn_zaisury AS DECIMAL(11,2))) AS DECIMAL(11,2)),
								PMN_ZAIKING    = CAST((CAST(PMN_ZAIKING AS DECIMAL(11,0)) + CAST(_p0103_1_pmn_zaiking AS DECIMAL(11,0))) AS DECIMAL(11,0)),
								TMN_SIRINSU    = CAST((CAST(TMN_SIRINSU AS DECIMAL(11,2)) + CAST(_p0103_1_tmn_sirinsu AS DECIMAL(11,2))) AS DECIMAL(11,2)),
								TMN_SIRINKG    = CAST((CAST(TMN_SIRINKG AS DECIMAL(11,0)) + CAST(_p0103_1_tmn_sirinkg AS DECIMAL(11,0))) AS DECIMAL(11,0)),
								TMN_KKSINSU    = CAST((CAST(TMN_KKSINSU AS DECIMAL(11,2)) + CAST(_p0103_1_tmn_kksinsu AS DECIMAL(11,2))) AS DECIMAL(11,2)),
								TMN_KKSINKG    = CAST((CAST(TMN_KKSINKG AS DECIMAL(11,0)) + CAST(_p0103_1_tmn_kksinkg AS DECIMAL(11,0))) AS DECIMAL(11,0)),
								GENK_SYUK_SURY = CAST((CAST(GENK_SYUK_SURY AS DECIMAL(11,2)) + CAST(_p0103_1_pmn_zaisury AS DECIMAL(11,2))
																																				+ CAST(_p0103_1_tmn_sirinsu AS DECIMAL(11,2))
																																				+ CAST(_p0103_1_tmn_kksinsu AS DECIMAL(11,2))) AS DECIMAL(11,2)),
								GENK_SYUK_KING = CAST((CAST(GENK_SYUK_KING AS DECIMAL(11,0)) + CAST(_p0103_1_pmn_zaiking AS DECIMAL(11,0))
																																				+ CAST(_p0103_1_tmn_sirinkg AS DECIMAL(11,0))
																																				+ CAST(_p0103_1_tmn_kksinkg AS DECIMAL(11,0))) AS DECIMAL(11,0)) 
					WHERE SYHN_CODE = _p0103_1_syhn_code
						AND KYU       = _p0103_1_kyu
				;
				
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'UPT_MST_SYHN_GENK_RDYS FAILED';
					LEAVE dept_loop;
				END IF;
				
				END UPT_MST_SYHN_GENK_RDYS;
			END IF;
		
		END WHILE dept_loop;
		
		CLOSE C0103_01;
	END CUR;
	
END MAIN_PROC
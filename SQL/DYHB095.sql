CREATE DEFINER=`root`@`localhost` PROCEDURE `DYHB095`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYHB095
	-- 機能		: 	売上実績用組織Ｍ予約更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-12
	-- =============================================
	
	DECLARE _DM_KEY INT DEFAULT 0;
	
	/* (1) 月別営業店別予算＆人員Ｆ（予約）*/
	DECLARE _p1_dnpynum DECIMAL;
	DECLARE _p1_js_yyyymm INT;
	DECLARE _p1_egtn_code CHAR(7);
	DECLARE _p1_egtn_sep_code CHAR(2);
	
	/* (2) 月別営業店別予算＆人員Ｆ        */
	DECLARE _p2_count INT;
	DECLARE _p4_r_gynum INT;
	
	/* 日付コントールＦより計上日と計上日の翌日を取得 */
	DECLARE _keid_date CHAR(11); /* 計上日       */
	DECLARE _next_date CHAR(11); /* 計上日の翌日 */
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/* プログラムID・端末IDの取得 */
	SET _pgmid = '\0';
	
	/***************************************************************/
	/* 更新処理 */
	/***************************************************************/
	
	SET _keid_date = '\0';
	SET _next_date = '\0';
	--
	SELECT KEI_DATE
				, KEI_DATE + 1 days
		INTO _keid_date
				, _next_date
	FROM MST_HIZS
	WHERE MST_HIZS.DM_KEY = _DM_KEY
	;
	
	--
	IF _keid_date IS NULL AND _next_date IS NULL THEN
		SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* (1) 月別営業店別予算＆人員Ｆ（予約） */
	-- MAIN
	SEL_MAIN: BEGIN
		DECLARE CB095_01 CURSOR FOR
		SELECT DNPYNUM
				, JS_YYYYMM
				, EGTN_CODE
				, EGTN_SEP_CODE
		FROM TRN_REPORT_YY1S
		WHERE DEL_FLAG = ''
		AND CHG_SYR_FLG = '0'
		AND CHG_DATE <= _next_date
		ORDER BY DNPYNUM
	;
	
	DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE=100;
		END;
		
		OPEN CB095_01;
		dept_loop:WHILE(SQLCODE=0) DO
			
			SET _p1_dnpynum = 0;
			SET _p1_js_yyyymm = 0;
			SET _p1_egtn_code = '\0';
			SET _p1_egtn_sep_code = '\0';
			
			FETCH FROM CB095_01
				INTO
						_p1_dnpynum
						, _p1_js_yyyymm
						, _p1_egtn_code
						, _p1_egtn_sep_code
			;
		
			IF IsNotFound = 1 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = '処理１ IsNotFound';
				LEAVE SEL_MAIN;
			END IF;
			
			/* (2) 月別営業店別予算＆人員Ｆ */
			SET _p2_count = 0;
			--
			SELECT COUNT(*)
				INTO _p2_count
			FROM TRN_REPORT_YSNS
			WHERE JS_YYYYMM = _p1_js_yyyymm
			AND 	EGTN_CODE = _p1_egtn_code
			AND 	EGTN_SEP_CODE = _p1_egtn_sep_code
			;
			
			IF _p2_count > 0 THEN
				-- UPDATE
				UPT_TRN_REPORT_YSNS:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 101;
				END;
				UPDATE TRN_REPORT_YSNS As A, TRN_REPORT_YY1S as B
					SET 
					A.EGTN_NAME 				= B.EGTN_NAME
					,	A.SIBU_CODE 			= B.SIBU_CODE
					, A.TNCHO_CODE 			= B.TNCHO_CODE
					, A.SYZ_SYAIN_SU 		= B.SYZ_SYAIN_SU
					, A.ACT_SYAIN_SU 		= B.ACT_SYAIN_SU
					, A.YOBI01_KUBN 		= B.YOBI01_KUBN
					, A.YOBI01_NUM 			= B.YOBI01_NUM
					, A.UPD_PGM_ID 			= _pgmid
					, A.UPD_USER_ID 		= ''
					, A.MODIFIED 				= CURRENT_TIMESTAMP
				WHERE A.JS_YYYYMM     = _p1_js_yyyymm     
				AND		A.EGTN_CODE     = _p1_egtn_code     
				AND 	A.EGTN_SEP_CODE = _p1_egtn_sep_code
				AND B.DNPYNUM = _p1_dnpynum
				;
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'UPT_TRN_REPORT_YSNS FAILED';
					LEAVE dept_loop;
				END IF;
				END UPT_TRN_REPORT_YSNS;
				
			ELSE
				-- INSERT
				INS_TRN_REPORT_YSNS:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 102;
				END;
				INSERT INTO TRN_REPORT_YSNS (JS_YYYYMM, EGTN_CODE, EGTN_SEP_CODE
                        , EGTN_NAME, SIBU_CODE, TNCHO_CODE, SYZ_SYAIN_SU, ACT_SYAIN_SU, YOBI01_KUBN, YOBI01_NUM
                        , CRT_PGM_ID, CRT_USER_ID, CRT_TS, SYNC_FLAG)
					SELECT 	JS_YYYYMM, EGTN_CODE, EGTN_SEP_CODE
                        , EGTN_NAME, SIBU_CODE, TNCHO_CODE, SYZ_SYAIN_SU, ACT_SYAIN_SU, YOBI01_KUBN, YOBI01_NUM
                        , _pgmid, '', CURRENT_TIMESTAMP, '0'
					FROM TRN_REPORT_YY1S
                    WHERE DNPYNUM = _p1_dnpynum
				;
				IF SQLCODE = 102 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'INS_TRN_REPORT_YSNS FAILED';
					LEAVE dept_loop;
				END IF;
				END INS_TRN_REPORT_YSNS;
				
			END IF;
			
			/* (3) 月別営業店別社員Ｆ - DELETE */
			DEL_TRN_REPORT_SYAINS:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 103;
			END;
			DELETE FROM TRN_REPORT_SYAINS
			WHERE JS_YYYYMM = _p1_js_yyyymm
				AND EGTN_CODE     = _p1_egtn_code     
				AND EGTN_SEP_CODE = _p1_egtn_sep_code
			;
			IF SQLCODE = 103 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'DEL_TRN_REPORT_SYAINS FAILED';
				LEAVE dept_loop;
			END IF;
			END DEL_TRN_REPORT_SYAINS;
			
			/* (4) 月別営業店別社員Ｆ(予約) */
			SET _p4_r_gynum = 0;
			SEL_4: BEGIN
				DECLARE CB095_04 CURSOR FOR
				SELECT R_GYNUM
				FROM TRN_REPORT_YY2S
				WHERE DNPYNUM = _p1_dnpynum
					ORDER BY R_GYNUM
				;
				
				DECLARE CONTINUE HANDLER FOR NOT FOUND
				BEGIN
					SET SQLCODE = 100;
				END;
				
				OPEN CB095_04;
				CB095_04Loop:WHILE(SQLCODE=0) DO
				
					SET _p4_r_gynum = 0;
					
					FETCH FROM CB095_04
					INTO
						_p4_r_gynum
					;
					
					IF _p4_r_gynum = 0 THEN
						SET SQLCODE = 100;
					END IF;
					IF SQLCODE=100 THEN
						SET fnReturn = KFAIL;
						SET fnMsg = '月別営業店別社員Ｆ(予約) IsNotFound';
						LEAVE CB095_04Loop;
					END IF;
					
					/* (5) 月別営業店別社員Ｆ - INSERT */
					INS_TRN_REPORT_SYAINS:BEGIN
					DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
					BEGIN
						 SET SQLCODE = 102;
					END;
					INSERT 
							INTO TRN_REPORT_SYAINS
							(
							JS_YYYYMM, EGTN_CODE, EGTN_SEP_CODE, GYNUM
							, TANT_CODE, KAISO_KUBN, SO_TANT_YAKU_CODE, SO_TANT_YAKU_LVL, SO_TANT_JOTAI_CODE, SO_TANT_JIYU_CODE, JOUI_TANT_CODE, SO_YASUMI_KUBN
							, CRT_PGM_ID, CRT_USER_ID, CRT_TS
							)
							SELECT 
							JS_YYYYMM, EGTN_CODE, EGTN_SEP_CODE, GYNUM
							, TANT_CODE, KAISO_KUBN, SO_TANT_YAKU_CODE, SO_TANT_YAKU_LVL, SO_TANT_JOTAI_CODE, SO_TANT_JIYU_CODE, JOUI_TANT_CODE, SO_YASUMI_KUBN
							, _pgmid, '', CURRENT_TIMESTAMP
					FROM TRN_REPORT_YY2S
					WHERE DNPYNUM = _p1_dnpynum
						AND R_GYNUM = _p4_r_gynum
					;
					IF SQLCODE=102 THEN
						SET fnReturn = KFAIL;
						SET fnMsg = 'INS_TRN_REPORT_SYAINS FAILED';
						LEAVE CB095_04Loop;
					END IF;
					END INS_TRN_REPORT_SYAINS;
					
					/* (4-UPDATE) 月別営業店別社員Ｆ(予約) */
					UPT_TRN_REPORT_YY2S:BEGIN
					DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
					BEGIN
						 SET SQLCODE = 101;
					END;
					UPDATE TRN_REPORT_YY2S
						SET CHG_SYR_FLG  = '1'
							, CHG_SYR_DATE = _keid_date
							, CHG_SYR_TS   = CURRENT_TIMESTAMP
							, UPD_PGM_ID   = _pgmid
							, UPD_USER_ID  = ''
							, MODIFIED     = CURRENT_TIMESTAMP -- UPD_TS
					WHERE DNPYNUM = _p1_dnpynum
						AND R_GYNUM = _p4_r_gynum
					;
					IF SQLCODE=101 THEN
						SET fnReturn = KFAIL;
						SET fnMsg = 'UPT_TRN_REPORT_YY2S FAILED';
						LEAVE CB095_04Loop;
					END IF;
					END UPT_TRN_REPORT_YY2S;
					
				END WHILE CB095_04Loop;
				CLOSE CB095_04;
			END SEL_4;
			
			/* (1-UPDATE) 月別営業店別予算＆人員Ｆ（予約） */
			UPT_TRN_REPORT_YY1S:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 101;
			END;
			UPDATE TRN_REPORT_YY1S
				SET CHG_SYR_FLG  = '1'
					, CHG_SYR_DATE = _keid_date
					, CHG_SYR_TS   = CURRENT_TIMESTAMP
					, UPD_PGM_ID   = _pgmid
					, UPD_USER_ID  = ''
					, MODIFIED     = CURRENT_TIMESTAMP -- UPD_TS
			WHERE DNPYNUM  = _p1_dnpynum
			;
			IF SQLCODE=101 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = 'UPT_TRN_REPORT_YY1S FAILED';
				LEAVE dept_loop;
			END IF;
			END UPT_TRN_REPORT_YY1S;
			
		END WHILE dept_loop;
		CLOSE CB095_01;
		
	END SEL_MAIN;
	
END MAIN_PROC
CREATE DEFINER=`root`@`localhost` PROCEDURE `DYSB020`(
	IN IN_CTL_KEY CHAR(8),
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYSB020
	-- 機能		: 	会員制度[ブリエ会員02]集計－申込期間内ＣＯ未満了
	-- 著者		: 	TBinh-PrimeLabo
	-- パラメータ	:		(string) IN_CTL_KEY = 'KAI-BKNY'
	-- 作成日	: 	2019-02-15
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _keijyo_date CHAR(11);

	/* 業務コントロールＦ */
	DECLARE _kai_bkny CHAR(7);
	
	/* ＶＩＰ会員・ブリエ会員用実績テーブル */
	DECLARE _p1_intro_kkyk_code CHAR(13);
	DECLARE _p1_count DOUBLE;
	DECLARE _wk_count INT;
		
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* 初期処理 */
	/***************************************************************/
	
	/* 計上日取得 */
	SET _keijyo_date = '\0';
	SELECT 
		KEI_DATE
		INTO _keijyo_date
	FROM MST_HIZS
	WHERE MST_HIZS.DM_KEY = _DM_KEY
	;
	-- 
	IF _keijyo_date IS NULL THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* ブリエ会員：集計対象となる購入金額を取得 */
	SET _kai_bkny = '\0';
	SELECT 
		substr(CTL_DATA, 1, 6)
		INTO _kai_bkny
	FROM MST_CTLS
	WHERE MST_CTLS.CTL_KEY = IN_CTL_KEY -- SAMPLE: KAI-BKNY
	;
	--
	IF _kai_bkny = '\0' THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2. IsNotFound [`CTL_KEY`='ss']","ss",IN_CTL_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/***************************************************************/
	/* 処理開始 */
	/***************************************************************/
	VIP:BEGIN
	DECLARE DYSB020_01 CURSOR FOR 
    SELECT T00.INTRO_KKYK_CODE
                   , count(*)
                FROM TRN_KAIN_JISS T00
           LEFT JOIN MST_KOKYAKUS  T10 ON T10.KKYK_CODE = T00.INTRO_KKYK_CODE
               WHERE T00.MOUS_KUBN      = '1'
                 AND T00.CLNG_OFF_FLAG  = '0'
                 AND T00.KONY_MOUS_DATE BETWEEN T10.BRI_SSTR_DATE AND T10.BRI_SEND_DATE
                 AND T00.BRI_CALC_FLAG  = '0'
                 AND T00.DEL_FLAG       = ' '
                 AND ((T00.KONY_MOUS_DATE >= '2015-01-01' AND T00.KONY_KING >= _kai_bkny)
                  OR  (T00.KONY_MOUS_DATE <= '2014-12-31'))

                 AND T00.INTRO_SYIN_KUBN     = '0'
                 AND T00.INTRO_KAIN_KUBN     NOT IN ('6', '7', '9', 'A', 'B')
                 AND T00.INTRO_GKSE_KUBN     = '0'
                 AND T00.INTRO_NENREI        >= 20

                 AND T10.SYIN_KUBN     = '0'
                 AND T10.KAIN_KUBN     NOT IN ('6', '7', '9', 'A', 'B')
                 AND T10.GKSE_KUBN     = '0'
                 AND CASE WHEN T10.BIRTH IS NULL THEN 0
                                                 ELSE YEAR(_keijyo_date) - YEAR(T10.BIRTH)
                                                    + CASE WHEN MONTH(T10.BIRTH) * 100 + DAY(T10.BIRTH) <= MONTH(_keijyo_date) * 100 + DAY(_keijyo_date) THEN 0
                                                           WHEN MONTH(T10.BIRTH) * 100 + DAY(T10.BIRTH) >  MONTH(_keijyo_date) * 100 + DAY(_keijyo_date) THEN -1
                                                       END
                      END              >= 20
                 AND T10.BRI_SSTR_DATE IS NOT NULL
                 AND T10.BRI_SEND_DATE IS NOT NULL
            GROUP BY T00.INTRO_KKYK_CODE
		;
	
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE = 100;
		END;
		
		OPEN DYSB020_01;
		dept_loop:WHILE(SQLCODE=0) DO
			
			SET _p1_intro_kkyk_code = '\0';
			SET _p1_count = 0;
			
			FETCH FROM DYSB020_01
				INTO
				_p1_intro_kkyk_code,
				_p1_count
			;
			
			IF _p1_intro_kkyk_code = '\0' OR _p1_count = 0 THEN
				SET SQLCODE = 100;
			END IF;
			IF SQLCODE = 100 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'CURSOR IS EMPTY';
				LEAVE dept_loop;
			END IF;
			
			/***************************************************************/
			/* ＶＩＰ会員・ブリエ会員用集計テーブルの更新 */
			/***************************************************************/
			
			SET _wk_count = 0;
			SELECT count(*)
				 into _wk_count
			FROM TRN_KAIN_CALCS
			WHERE KKYK_CODE = _p1_intro_kkyk_code; 
			
			IF _wk_count = 0 THEN
			
				/* ＶＩＰ会員・ブリエ会員用集計テーブルの追加処理 */
				INS_TRN_KAIN_CALCS:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 102;
				END;
				INSERT INTO TRN_KAIN_CALCS
					 ( KKYK_CODE
					 , VIP_CLNG_OFF_KING
					 , VIP_YUYO_CLNG_MAE_KING
					 , VIP_YUYO_CLNG_ATO_KING
					 , BRI_CLNG_OFF_KING
					 , BRI_YUYO_CLNG_MAE_KING
					 , BRI_YUYO_CLNG_ATO_KING
					 , DEL_FLAG
					 , CRT_PGM_ID
					 , CRT_USER_ID
					 , CREATED -- CRT_TS
					 , UPD_PGM_ID
					 , UPD_USER_ID
					 , MODIFIED -- UPD_TS 
					 )
			VALUES ( _p1_intro_kkyk_code
					 , 0
					 , 0
					 , 0
					 , 0
					 , _p1_count
					 , 0
					 , ' '
					 , _pgmid
					 , NULL
					 , CURRENT_TIMESTAMP
					 , _pgmid
					 , NULL
					 , CURRENT_TIMESTAMP
					 )
				;
				IF SQLCODE = 102 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'INS_TRN_KAIN_CALCS FAILED';
					LEAVE dept_loop;
				END IF;
				END INS_TRN_KAIN_CALCS;
				
			ELSE
			
				/* ＶＩＰ会員・ブリエ会員用集計テーブルの更新処理 */
				UPT_TRN_KAIN_CALCS:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 101;
				END;
				UPDATE TRN_KAIN_CALCS
						SET BRI_YUYO_CLNG_MAE_KING	= _p1_count
							, UPD_PGM_ID							= _pgmid
							, UPD_USER_ID							= NULL
							, MODIFIED								= CURRENT_TIMESTAMP -- UPD_TS
					WHERE KKYK_CODE								= _p1_intro_kkyk_code
				;
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'UPT_TRN_KAIN_CALCS FAILED';
					LEAVE dept_loop;
				END IF;
				END UPT_TRN_KAIN_CALCS;
				
			END IF;
			
		END WHILE dept_loop;
		CLOSE DYSB020_01;
		
	END VIP;

END MAIN_PROC
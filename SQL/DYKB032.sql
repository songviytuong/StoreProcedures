CREATE DEFINER=`root`@`localhost` PROCEDURE `DYKB032`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYKB032
	-- 機能		: 	商品仕入先別仕入単価Ｍ 予約仕入単価更新処理
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-11
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _tekiyo_date DATE; 
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	SET _pgmid = '\0';
	SET _tekiyo_date = '\0';
	
	SELECT KEI_DATE + 1 days
		INTO _tekiyo_date
	FROM MST_HIZS
	WHERE DM_KEY = _DM_KEY
	;
	
	IF _tekiyo_date = '\0' THEN 
		SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/***************************************************************/
	/* 処理１ */
	/***************************************************************/
	UPT_MST_SYHN_TANKS: BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			 SET SQLCODE = 101;
		END;
		--
		UPDATE MST_SYHN_TANKS
			SET 
				NOW_SIR_TANK 		= YOYK_SIR_TANK
				, YOYK_TEKI_DATE 	= NULL
				, YOYK_SIR_TANK 	= CAST(0 AS DECIMAL(11,2))
				, PGM_ID			= _pgmid
				, MODIFIED			= CURRENT_TIMESTAMP
		WHERE
			YOYK_TEKI_DATE <= _tekiyo_date
		;
		--
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_MST_SYHN_TANKS FAILED';
			LEAVE MAIN_PROC;
		END IF;
	END UPT_MST_SYHN_TANKS;

END MAIN_PROC
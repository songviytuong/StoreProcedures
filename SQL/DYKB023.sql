CREATE DEFINER=`root`@`localhost` PROCEDURE `DYKB023`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYKB023
	-- 機能		: 	レディメイド発注データ 発注完了→削除更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-17
	-- =============================================
		
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* 処理１ */
	/***************************************************************/

	/* 発注Ｂ更新処理 */
	UPT_1:BEGIN
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		 SET SQLCODE = 101;
	END;
	UPDATE TRN_HATBS
			SET DEL_FLAG		= 'D'
		WHERE HACCY_KNRY_KUBN	= '1'
			AND DEL_FLAG        = ' '
	;
	IF SQLCODE = 101 THEN 
		SET fnReturn = KFAIL;
		SET fnMsg = '発注Ｂ更新処理 UPT_1 FAILED';
		LEAVE MAIN_PROC;
	END IF;
	END UPT_1;
	
	/***************************************************************/
	/* 処理２ */
	/***************************************************************/
	CUR:BEGIN
	/* 発注Ｈ */
	DECLARE _p1_srnum DOUBLE		; /* シリアル№ */
	DECLARE _p1_dpynum INT			; /* 発注№（入荷№） */

	/* 作業用件数 */
	DECLARE _wk_count INT			; /* 発注Ｂの対象件数 */
	
	DECLARE DYKB023_01 CURSOR FOR 
	SELECT SRNUM, DNPYNUM
			FROM TRN_HATHS
			WHERE DEL_FLAG = ' '
			ORDER BY DNPYNUM, SRNUM
	;
	
	DECLARE EXIT HANDLER FOR NOT FOUND
	BEGIN
		SET SQLCODE = 100;
	END;
	
	OPEN DYKB023_01;
	dept_loop:WHILE(SQLCODE=0) DO
		
		SET _p1_srnum = 0;
    SET _p1_dpynum = 0;
		
		FETCH FROM DYKB023_01
			INTO
				  _p1_srnum
				, _p1_dpynum
		;
		
		IF _p1_srnum = 0 OR _p1_dpynum = 0 THEN
			SET SQLCODE = 100;
		END IF;
		
		IF SQLCODE = 100 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'CURSOR IS EMPTY';
			LEAVE dept_loop;
		END IF;
		
		/* 発注Ｂの件数取得 */
		SET _wk_count = 0;
		SELECT count(*)
			 INTO _wk_count
			 FROM TRN_HATBS
			WHERE DNPYNUM = _p1_dpynum
				AND SRNUM   = _p1_srnum
				AND DEL_FLAG = ' '
		;
		
		IF _wk_count = 0 THEN
			UPT_TRN_HATHS:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 101;
			END;
			UPDATE 	TRN_HATHS
				SET DEL_FLAG 	= 'D'
				WHERE DNPYNUM  	= _p1_dpynum
				AND SRNUM    	= _p1_srnum
			;
			IF SQLCODE = 101 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'UPT_TRN_HATHS FAILED';
				LEAVE dept_loop;
			END IF;
			END UPT_TRN_HATHS;
		END IF;
		
	END WHILE dept_loop;
	CLOSE DYKB023_01;
	
	END CUR;

END MAIN_PROC
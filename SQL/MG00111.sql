DROP PROCEDURE IF EXISTS `MG00111`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00111`(
)
proc_label:BEGIN
	
    DECLARE DATA_NOT_FOUND INT DEFAULT FALSE;
    DECLARE KFAIL INT DEFAULT 0;
    DECLARE KSUCCEED INT DEFAULT 1;
    DECLARE FNRETURN INT DEFAULT 1;
	DECLARE _p0111_1_SYHN_CODE CHAR(9) DEFAULT '0';
	DECLARE _p0111_1_KYU CHAR(2) DEFAULT '0';
    DECLARE _p0111_1_PMN_ZAISURY DECIMAL(11,2) DEFAULT 0;
    DECLARE _p0111_1_PMN_ZAIKING DECIMAL(11,2) DEFAULT 0;
    DECLARE _p0111_1_TMN_KKSINSU DECIMAL(11,2) DEFAULT 0;
    DECLARE _p0111_1_TMN_KKSINKG DECIMAL(11,2) DEFAULT 0;
    
    /* 級商品別原価マスタ（レディ）存在チェック */
	DECLARE C1 CURSOR FOR
		SELECT SYHN_CODE, KYU,
                     SUM(PMN_ZAISURY) AS PMN_ZAISURY, 
                     SUM(PMN_ZAIKING) AS PMN_ZAIKING,
                     SUM(TMN_KKSINSU) AS TMN_KKSINSU, 
                     SUM(TMN_KKSINKG) AS TMN_KKSINKG 
			FROM MST_ZAI_RDYS
			WHERE ZAKR_KUBN    = '1'
                 AND SKMB_KUBN = '01'
                 AND KYU       <> 'A'
                 AND KYU       <> 'S'
            GROUP BY SYHN_CODE, KYU;
            
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SELECT KFAIL AS fnReturn;
	END;
    
    DECLARE CONTINUE HANDLER FOR SQLWARNING
    BEGIN
    END;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND
    BEGIN
		SET DATA_NOT_FOUND = TRUE;
    END;
    
    OPEN C1;
    
    SELECT_MST_ZAI_RDY: WHILE 1 DO
		FETCH C1 INTO _p0111_1_SYHN_CODE, _p0111_1_KYU, _p0111_1_PMN_ZAISURY, _p0111_1_PMN_ZAIKING, _p0111_1_TMN_KKSINSU, _p0111_1_TMN_KKSINKG;
        
        IF DATA_NOT_FOUND THEN
			LEAVE SELECT_MST_ZAI_RDY;
        END IF;
		
		IF (SELECT syhn_code FROM MST_SYHN_GENK_RDYS WHERE SYHN_CODE = _p0111_1_SYHN_CODE AND KYU = _p0111_1_KYU) IS NULL THEN
			ITERATE  SELECT_MST_ZAI_RDY;
		END IF;
		
		/* 級商品別原価マスタ（レディ）更新 */
		UPDATE MST_SYHN_GENK_RDYS
		SET PMN_ZAISURY    = CAST((CAST(PMN_ZAISURY AS DECIMAL(11,2)) + CAST(_p0111_1_PMN_ZAISURY AS DECIMAL(11,2))) AS DECIMAL(11,2)),
			PMN_ZAIKING    = CAST((CAST(PMN_ZAIKING AS DECIMAL(11,0)) + CAST(_p0111_1_PMN_ZAIKING AS DECIMAL(11,0))) AS DECIMAL(11,0)),
			TMN_KKSINSU    = CAST((CAST(TMN_KKSINSU AS DECIMAL(11,0)) + CAST(_p0111_1_TMN_KKSINSU AS DECIMAL(11,0))) AS DECIMAL(11,0)),
			TMN_KKSINKG    = CAST((CAST(TMN_KKSINKG AS DECIMAL(11,0)) + CAST(_p0111_1_TMN_KKSINKG AS DECIMAL(11,0))) AS DECIMAL(11,0)),
			GENK_SYUK_SURY = CAST((CAST(GENK_SYUK_SURY AS DECIMAL(11,2))  + CAST(_p0111_1_PMN_ZAISURY AS DECIMAL(11,2)) + CAST(_p0111_1_TMN_KKSINSU AS DECIMAL(11,2))) AS DECIMAL(11,0)),
			GENK_SYUK_KING = CAST((CAST(GENK_SYUK_KING AS DECIMAL(11,2))  + CAST(_p0111_1_PMN_ZAIKING AS DECIMAL(11,2)) + CAST(_p0111_1_TMN_KKSINKG AS DECIMAL(11,2))) AS DECIMAL(11,0))
		WHERE 
			SYHN_CODE 	   = _p0111_1_SYHN_CODE
			AND KYU        = _p0111_1_KYU;
				  
	END WHILE SELECT_MST_ZAI_RDY;
	
	CLOSE C1;
    
    SELECT FNRETURN AS fnReturn;
			
END
;;
delimiter ;
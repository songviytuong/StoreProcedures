CREATE DEFINER=`root`@`localhost` PROCEDURE `MM00032`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMM00032
	-- 機能		: 	信販カード会社／店別債権残高Ｆの月次更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-12
	-- =============================================
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	/*INIT SETUP*/
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	
	/* 店舗在庫マスタの月次更新 */
	UPT: BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			SET SQLCODE = 100;
		END;
		UPDATE TRN_MISE_URZNS
		SET	   PMN_GE_ZAN_KING     = TMN_GE_ZAN_KING
			 , TMN_GE_URI_KING     = NMN_GE_URI_KING
			 , TMN_GE_NYU_KING     = NMN_GE_NYU_KING
			 , TMN_GE_ZAN_KING     = NOW_GE_ZAN_KING
			 , PMN_M_ZAN_CRJT_KING = TMN_M_ZAN_CRJT_KING
			 , PMN_M_ZAN_GIFT_KING = TMN_M_ZAN_GIFT_KING
			 , TMN_M_URI_CRJT_KING = NMN_M_URI_CRJT_KING
			 , TMN_M_URI_GIFT_KING = NMN_M_URI_GIFT_KING
			 , TMN_M_ZAN_CRJT_KING = NOW_M_ZAN_CRJT_KING
			 , TMN_M_ZAN_GIFT_KING = NOW_M_ZAN_GIFT_KING
			 , PMM_ZAN_CRJT_KING   = TMM_ZAN_CRJT_KING
			 , PMN_ZAN_GIFT_KING   = TMN_ZAN_GIFT_KING
			 , TMN_URI_CRJT_KING   = NMN_URI_CRJT_KING
			 , TMN_URI_GIFT_KING   = NMN_URI_GIFT_KING
			 , TMN_NYK_CRJT_KING   = NMN_NYK_CRJT_KING
			 , TMN_NYK_GIFT_KING   = NMN_NYK_GIFT_KING
			 , TMM_ZAN_CRJT_KING   = NOW_ZAN_CRJT_KING
			 , TMN_ZAN_GIFT_KING   = NOW_ZAN_GIFT_KING
			 , NMN_GE_URI_KING     = 0
			 , NMN_GE_NYU_KING     = 0
			 , NMN_M_URI_CRJT_KING = 0
			 , NMN_M_URI_GIFT_KING = 0
			 , NMN_URI_CRJT_KING   = 0
			 , NMN_URI_GIFT_KING   = 0
			 , NMN_NYK_CRJT_KING   = 0
			 , NMN_NYK_GIFT_KING   = 0
			 , UPD_PGM_ID          = _pgmid
			 , UPD_USER_ID         = NULL
			 , MODIFIED            = CURRENT_TIMESTAMP -- UPD_TS
		;
		IF SQLCODE = 100 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = '信販カード会社／店別債権残高Ｆの月次更新 - UPDATE FAILED';
			LEAVE MAIN_PROC;
		END IF;
	END UPT;

END MAIN_PROC
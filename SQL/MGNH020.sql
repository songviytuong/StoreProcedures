CREATE DEFINER=`root`@`localhost` PROCEDURE `MGNH020`(
	IN IN_ODR_KUBN INT,
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMGNH020
	-- 機能		: 	納品基準  原価計算  納品基準売上ＢＢ更新
	-- 著者		: 	TBinh-PrimeLabo
	-- パラメータ	:		(int) IN_ODR_KUBN = 2
	-- 作成日	: 	2019-02-14
	-- =============================================
	
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _den_from_date CHAR(11);  /* 伝票入力期間開始 */
	DECLARE _den_to_date CHAR(11);  /* 伝票入力期間終了 */
	
	/* 納品基準売上ＢＢ */
	DECLARE _p1_srnum DOUBLE			; /* シリアル№ */
	DECLARE _p1_dnpynum INT				; /* 伝票№ */
	DECLARE _p1_gynum INT					; /* 行№ */
	DECLARE _p1_hojo_gynum INT		; /* 補助行№ */
	DECLARE _p1_kyu CHAR(2)				; /* 級 */
	DECLARE _p1_syhn_code CHAR(9)	; /* 商品CD */

	/* 級商品別原価マスタ（レディ） */
	DECLARE _p2_genk_tank DOUBLE	; /* 原価単価 */
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/* 計上日取得 */
	SET _den_from_date = '\0';
	SET _den_to_date = '\0';
	SELECT 	DEN_FROM_DATE
				, DEN_TO_DATE
		INTO _den_from_date, _den_to_date
	FROM MST_HIZS
	WHERE DM_KEY = _DM_KEY;
	
	IF _den_from_date = '\0' OR _den_to_date = '\0' THEN
		SET SQLCODE = 100;
	END IF;
	
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("計上日取得 IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;

	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	
	/* 納品基準売上ＢＢ読み込み */
	CUR: BEGIN
		DECLARE CNH020_01 CURSOR FOR
		SELECT SRNUM
				 , DNPYNUM
				 , GYNUM
				 , HOJO_GYNUM
				 , KYU
				 , SYHN_CODE
			FROM TRN_URIBB_NHNS
		 WHERE KEID_DATE BETWEEN _den_from_date AND _den_to_date
			 AND ODR_KUBN  = IN_ODR_KUBN -- '2'
		;
		
		DECLARE CONTINUE HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE = 100;
		END;
		
		OPEN CNH020_01;
		
		dept_loop:WHILE(SQLCODE = 0) DO
		
			SET _p1_srnum = 0;
			SET _p1_dnpynum = 0;
			SET _p1_gynum = 0;
			SET _p1_hojo_gynum = 0;
			
			SET _p1_kyu = '\0';
			SET _p1_syhn_code = '\0';

			FETCH FROM CNH020_01
				INTO 	
						  _p1_srnum
						, _p1_dnpynum
						, _p1_gynum
						, _p1_hojo_gynum
						, _p1_kyu
						, _p1_syhn_code
				;
			
			IF SQLCODE = 100 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = REPLACE("納品基準売上ＢＢ読み込み IsNotFound [`ODR_KUBN`='ss']","ss",IN_ODR_KUBN);
				LEAVE dept_loop;
			END IF;
			
			/* 級商品別原価マスタ（レディ）読み込み */
			SET _p2_genk_tank = 0;
			SELECT GENK_TANK
					 INTO _p2_genk_tank
			FROM MST_SYHN_GENK_RDYS
			WHERE SYHN_CODE = _p1_syhn_code
				AND KYU       = _p1_kyu
			;
			--
			IF _p2_genk_tank = 0 THEN 
				SET SQLCODE = 100; 
			END IF;
			
			/* 存在した場合の処理 */
			IF SQLCODE = 100 THEN
				
				UPT_TRN_URIBB_NHNS_1:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 101;
				END;
				UPDATE TRN_URIBB_NHNS
					SET GM_GEN_TANK  	= CAST(_p2_genk_tank AS DECIMAL(11,2))
						, UPD_GEN_KING 	= CAST((CAST(_p2_genk_tank AS DECIMAL(11,2)) * CAST(UPD_SURY AS DECIMAL(11,2))) AS DECIMAL(11,0))
						, GENKA_FLG    	= '1'
						, GENKA_TS     	= CURRENT_TIMESTAMP
						, UPD_PGM_ID   	= _pgmid
						, UPD_USER_ID  	= NULL
						, MODIFIED      = CURRENT_TIMESTAMP
				WHERE SRNUM      		= CAST(_p1_srnum AS DECIMAL(14,0))
					AND DNPYNUM    		= _p1_dnpynum
					AND GYNUM      		= _p1_gynum
					AND HOJO_GYNUM 		= _p1_hojo_gynum
				;
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'UPT_TRN_URIBB_NHNS_1 FAILED';
					LEAVE dept_loop;
				END IF;
				END UPT_TRN_URIBB_NHNS_1;
			
			ELSE
			
				UPT_TRN_URIBB_NHNS_2:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 101;
				END;
				UPDATE TRN_URIBB_NHNS
					SET GENKA_FLG    	= '0'
						, GENKA_TS     	= CURRENT_TIMESTAMP
						, UPD_PGM_ID   	= _pgmid
						, UPD_USER_ID  	= NULL
						, MODIFIED     	= CURRENT_TIMESTAMP
				WHERE SRNUM					= CAST(_p1_srnum AS DECIMAL(14,0))
					AND DNPYNUM    		= _p1_dnpynum
					AND GYNUM      		= _p1_gynum
					AND HOJO_GYNUM 		= _p1_hojo_gynum
				;
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'UPT_TRN_URIBB_NHNS_2 FAILED';
					LEAVE dept_loop;
				END IF;
				END UPT_TRN_URIBB_NHNS_2;
			
			END IF;
		
		END WHILE dept_loop;
		
		CLOSE CNH020_01;
	END CUR;
	
END MAIN_PROC
CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00201`(
	IN IN_ZAKR_KUBN CHAR(1),
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMG00201
	-- 機能		: 	初期処理１
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-13
	-- =============================================
	
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _keijyo_date CHAR(11);
	
	/* 在庫Ｍ（レディ） */
	DECLARE _p0201_1_bumn_code CHAR(7);        /* 部門コード */
	DECLARE _p0201_1_syhn_code CHAR(9);        /* 商品コード */
	DECLARE _p0201_1_kyu CHAR(2);              /* 級 */
	DECLARE _p0201_1_genk_tank DOUBLE;           /* 原価単価 */

	/* 級商品別原価マスタ（レディ） */
	DECLARE _p0201_2_genk_tank DOUBLE;           /* 原価単価 */

	/* 作業用 */
	DECLARE _p0201_wk_genk_tank DOUBLE;          /* ＷＫ－原価単価 */

	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* 初期処理 */
	/***************************************************************/
	
	/* 計上日を取得し＋１日する */
	SET _keijyo_date = '\0';
	SELECT KEI_DATE
		INTO _keijyo_date
	FROM MST_HIZS
	WHERE DM_KEY = _DM_KEY;
	
	IF _keijyo_date = '\0' THEN
		SET SQLCODE = 100;
	END IF;
	
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("計上日を取得し＋１日する IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;

	/***************************************************************/
	/* メイン処理 */
	/***************************************************************/
	
	CUR: BEGIN
		DECLARE C0201_01 CURSOR FOR
		SELECT BUMN_CODE, SYHN_CODE,
			KYU, GENK_TANK
		FROM MST_ZAI_RDYS
		WHERE ZAKR_KUBN = IN_ZAKR_KUBN -- 1
		ORDER BY BUMN_CODE, SYHN_CODE, KYU
		;
		
		DECLARE CONTINUE HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE = 100;
		END;
		
		OPEN C0201_01;
		
		dept_loop:WHILE(SQLCODE = 0) DO
		
			SET _p0201_1_bumn_code = '\0';
			SET _p0201_1_syhn_code = '\0';
			SET _p0201_1_kyu = '\0';

			SET _p0201_1_genk_tank = 0;

			FETCH FROM C0201_01
				INTO 	
						_p0201_1_bumn_code
					, _p0201_1_syhn_code
					, _p0201_1_kyu
					, _p0201_1_genk_tank
			;			
			
			IF SQLCODE = 100 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = REPLACE("処理１ IsNotFound [`ZAKR_KUBN`='ss']","ss",IN_ZAKR_KUBN);
				LEAVE dept_loop;
			END IF;
			
			/* 級商品別原価マスタ（レディ）原価単価取得 */
			SET _p0201_wk_genk_tank = 0;
			SET _p0201_2_genk_tank = 0;
			SELECT GENK_TANK
					 INTO _p0201_2_genk_tank
			FROM MST_SYHN_GENK_RDYS
			WHERE SYHN_CODE = _p0201_1_syhn_code
				AND KYU       = _p0201_1_kyu
			;
			
			--
			
			IF _p0201_2_genk_tank = 0 THEN 
				SET SQLCODE = 100; 
			END IF;
			
			IF SQLCODE = 100 THEN
				SET _p0201_wk_genk_tank = CAST(_p0201_1_genk_tank AS DECIMAL(11,2));
			ELSE
				SET _p0201_wk_genk_tank = CAST(_p0201_2_genk_tank AS DECIMAL(11,2));
			END IF;
			
			/* 在庫Ｍ（レディ）更新 */
			UPT_MST_ZAI_RDYS:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 101;
			END;
			
			UPDATE MST_ZAI_RDYS
			SET TMN_IDOINKG   = 0
					, TMN_URIOTKG   = 0
					, TMN_IDOOTKG   = 0
					, TMN_CYOOTKG   = 0
					, TMN_KKSOTKG   = 0
					, TMN_ZAIKING   = 0
					, GEN_ZAIKING   = 0
					, PMN_GENK_TANK = CAST(_p0201_1_genk_tank AS DECIMAL(11,2))
					, GENKA_DATE    = _keijyo_date
					, GENKA_TIME    = CURRENT_TIME
					, PGM_ID        = _pgmid
					, USER_ID       = _pgmid
					, MODIFIED      = CURRENT_TIMESTAMP -- TS
					, UPD_DATE      = _keijyo_date
					, GENK_TANK     = CAST(_p0201_wk_genk_tank AS DECIMAL(11,2))
			WHERE BUMN_CODE = _p0201_1_bumn_code
				AND SYHN_CODE = _p0201_1_syhn_code
				AND KYU       = _p0201_1_kyu
			;
				
			IF SQLCODE = 101 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = 'UPT_MST_ZAI_RDYS FAILED';
				LEAVE dept_loop;
			END IF;
				
			END UPT_MST_ZAI_RDYS;
		
		END WHILE dept_loop;
		
		CLOSE C0201_01;
	END CUR;
	
END MAIN_PROC
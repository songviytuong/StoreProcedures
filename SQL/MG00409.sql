CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00409`(
	IN IN_CTL_KEY CHAR(100),
	IN IN_RIYU_SYBT_CODE INT,
	IN IN_ZAKR_KUBN CHAR(1),
	OUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMG00409
	-- 機能		: 	在庫Ｍ端数金額調整処理
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-01-30, 2019-01-31
	-- =============================================
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _keijyo_date DATE; 
	DECLARE _monthly_to_date DATE; 
	
	/* 業務コントロールＦ */
	DECLARE _zcy_riyu_code_gen INT;
	DECLARE _zcy_kamk_code_gen INT;
	DECLARE _zcy_zgen_kubn_gen CHAR(2);
	DECLARE _zcy_riyu_code_zou INT;
	DECLARE _zcy_kamk_code_zou INT;
	DECLARE _zcy_zgen_kubn_zou CHAR(2);
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	--
	/* シリアル＆伝票№採番用 */
	DECLARE _s1_DPN_KEY CHAR(4) DEFAULT "500";
	DECLARE _s1_dps_num INT;
	DECLARE _s1_dpn_num INT;
	DECLARE _s1_err_code INT;

	/* 在庫マスタ（レディ） */		
	DECLARE _p1_bumn_code CHAR(7)					 ; /* 部門CD */
	DECLARE _p1_syhn_code CHAR(9)					 ; /* 商品CD */
	DECLARE _p1_kyu CHAR(2)								 ; /* 級 */
	DECLARE _p1_tmn_zaiking DECIMAL(11,2)	 ; /* 当月末帳簿在庫金額 */

	/* 商品マスタ */
	DECLARE _p2_syhn_name CHAR(41)			   ; /* 商品名：正式 */
	DECLARE _p2_syhn_tani CHAR(5)          ; /* 単位 */
	DECLARE _p2_syhng_code INT						 ; /* 商品群CD */
	DECLARE _p2_colr_code INT							 ; /* カラーCD */
	DECLARE _p2_size_code INT							 ; /* サイズCD */
	DECLARE _p2_skmb_kubn CHAR(3)          ; /* 商品科目分類区分 */
	DECLARE _p2_odr_kubn CHAR(2)           ; /* オーダー商品区分 */
	DECLARE _p2_zakr_kubn CHAR(2)	         ; /* 在庫管理区分 */

	/* カラー マスタ */
	DECLARE _p3_colr_name CHAR(21)         ; /* カラー名 */

	/* サイズマスタ */
	DECLARE _p4_size_name CHAR(21)         ; /* サイズ名 */

	/* 作業項目 */
	DECLARE _ctl_gyo_count INT             ; /* コントロールブレイク用 行№ */
	DECLARE _ctl_bumn_code CHAR(7)    		 ; /* コントロールブレイク用 部門コード */
	--
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	DECLARE IsNotFound INT;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
	
	/*INIT SETUP*/
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
  /* 初期処理 */
  /***************************************************************/
  /* １．当月会計期間（終了）を取得する */
	SELECT 
		KEI_DATE, MONTHLY_TO_DATE
		INTO _keijyo_date, _monthly_to_date
	FROM MST_HIZS
	WHERE MST_HIZS.DM_KEY = _DM_KEY;
	-- 
-- 	select _keijyo_date;
	IF _keijyo_date IS NULL THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("1. IsNotFound [`DM_KEY`= n]","n",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* ２．    業務コントロールより */
  /* ２－１．微調整在庫調整理由（在庫金額減［帳簿在庫金額＞０］のとき） */
	SET _zcy_riyu_code_gen = 0;
	SELECT 
		CTL_DATA 
		INTO _zcy_riyu_code_gen 
	FROM MST_CTLS 
	WHERE MST_CTLS.CTL_KEY = IN_CTL_KEY; /*GEN-ZCY1*/
	-- 
	IF _zcy_riyu_code_gen = 0 THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2-1. IsNotFound [`CTL_KEY`='s']","s",IN_CTL_KEY);
		LEAVE MAIN_PROC;
	END IF;

	SET _zcy_kamk_code_gen = 0;
	SET _zcy_zgen_kubn_gen = '\0';
	SELECT 
		KMK_CODE, ZAIK_ZOGN_KUBN
		INTO _zcy_kamk_code_gen, _zcy_zgen_kubn_gen
	FROM MST_KAMRIYUS
	WHERE MST_KAMRIYUS.RIYU_SYBT_CODE = IN_RIYU_SYBT_CODE /*2*/
		AND MST_KAMRIYUS.RIYU_CODE = _zcy_riyu_code_gen;
	-- 
	IF _zcy_kamk_code_gen = 0 THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2-1. IsNotFound [`RIYU_SYBT_CODE`= n]","n",IN_RIYU_SYBT_CODE);
		LEAVE MAIN_PROC;
	END IF;
	
	/* ２－２．微調整在庫調整理由（在庫金額増［帳簿在庫金額＜０］のとき） */
	SET _zcy_riyu_code_zou = 0;
	SELECT 
		CTL_DATA 
		INTO _zcy_riyu_code_zou 
	FROM MST_CTLS 
	WHERE MST_CTLS.CTL_KEY = IN_CTL_KEY; /*GEN-ZCY2*/
	-- 
	IF _zcy_riyu_code_zou = 0 THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2-2. IsNotFound [`CTL_KEY`='ss']","ss",IN_CTL_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	SET _zcy_kamk_code_zou = 0;
	SET _zcy_zgen_kubn_zou = '\0';
	SELECT 
		KMK_CODE, ZAIK_ZOGN_KUBN
		INTO _zcy_kamk_code_zou, _zcy_zgen_kubn_zou
	FROM MST_KAMRIYUS
	WHERE MST_KAMRIYUS.RIYU_SYBT_CODE = IN_RIYU_SYBT_CODE /*2*/
		AND MST_KAMRIYUS.RIYU_CODE = _zcy_riyu_code_zou;
	-- 
	
	IF _zcy_kamk_code_zou = 0 THEN
			SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("2-2. IsNotFound [`RIYU_SYBT_CODE`='ss']","ss",IN_RIYU_SYBT_CODE);
		LEAVE MAIN_PROC;
	END IF;
	
	-- MAIN
	SEL_MAIN: BEGIN
		DECLARE C0409_01 CURSOR FOR
		SELECT BUMN_CODE, SYHN_CODE, KYU, TMN_ZAIKING
		FROM MST_ZAI_RDYS
		WHERE MST_ZAI_RDYS.ZAKR_KUBN = IN_ZAKR_KUBN /*1*/
			AND MST_ZAI_RDYS.TMN_ZAISURY = 0
			AND MST_ZAI_RDYS.TMN_ZAIKING > 0
			ORDER BY BUMN_CODE, SYHN_CODE, KYU
		;
		
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET IsNotFound=1;
		END;
		
		SET IsNotFound=0;
		
		/* INIT */
		SET _ctl_gyo_count = 1000;
		SET _ctl_bumn_code = '\0';

		OPEN C0409_01;
		
		/***************************************************************/
		/* メイン処理 */
		/***************************************************************/
		dept_loop:WHILE(IsNotFound=0) DO
	
			SET _p1_bumn_code = '\0';
			SET _p1_syhn_code = '\0';
			SET _p1_kyu = '\0';
			
			SET _p1_tmn_zaiking = 0;
					
			FETCH FROM C0409_01
				INTO
				_p1_bumn_code,
				_p1_syhn_code,
				_p1_kyu,
				_p1_tmn_zaiking
			;
			IF IsNotFound=1 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = '処理１ IsNotFound';
				LEAVE SEL_MAIN;
			END IF;
			
			SET _ctl_gyo_count = _ctl_gyo_count + 1;
			/* コントロールブレイクの判断 */
      IF _ctl_gyo_count > 999 OR strcmp(_ctl_bumn_code, _p1_bumn_code) != 0 THEN
				/* コントロールブレイクキー初期化 */
				SET _ctl_gyo_count = 1;
				SET _ctl_bumn_code = '\0';
				
				/* シリアル№採番 */
        SET _s1_dps_num = 0;
        SET _s1_err_code = 0;
				
				call ANUMDPS(_s1_dps_num,_s1_err_code);
				
				IF _s1_err_code <> 0 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = "シリアル№採番 [`_s1_err_code`!= 0]";
					LEAVE dept_loop;
				END IF;
				
				/* 伝票№採番 */
				SET _s1_dpn_num = 0;
				SET _s1_err_code = 0;
				
				call ANUMDPN1(_s1_DPN_KEY,_s1_dpn_num,_s1_err_code);
				
				IF _s1_err_code <> 0 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = "伝票№採番 [`_s1_err_code`!= 0]";
					LEAVE dept_loop;
				END IF;
				
				/* 在庫調整Ｈ更新 */
				UPT: BEGIN
					DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
					BEGIN
						 SET SQLCODE = 100;
					END;
					SET @_s1_dps_num = CAST(_s1_dps_num AS DECIMAL(14,0));
					
					INSERT INTO TRN_ZCYHS (
										 SRNUM, GYM_CODE, DNPYNUM
										 , TESEI_KUBN, ODRIGAI_KUBN, KEJ_DATE
										 , MON_KUBN, TANT_CODE, ZAIK_BUMN_CODE
										 , KEIH_BUMN_CODE, RIYU_CODE, KMK_CODE
										 , ZAIK_ZOGN_KUBN, PRT_KUBN, YOBI01_NUM
										 , CRT_DATE
										 , CRT_PGM_ID, CRT_USER_ID, CREATED /*CRT_TS*/
										 , UPD_PGM_ID, UPD_USER_ID, MODIFIED /*UPD_TS*/)
									VALUES (
											@_s1_dps_num,'Z100',_s1_dpn_num,
											'0', '1', _monthly_to_date,
											'1', '000172', _p1_bumn_code,
											'000141', _zcy_riyu_code_gen, _zcy_kamk_code_gen,
											_zcy_zgen_kubn_gen, '1', 1,
											_keijyo_date,
											_pgmid, Null, CURRENT_TIMESTAMP,
											Null, Null, Null);
					IF SQLCODE = 100 THEN 
						SET fnReturn = KFAIL;
						SET fnMsg = '在庫調整Ｈ更新 - INSERT FAILED';
						LEAVE dept_loop;
					END IF;
				END UPT;
			END IF;
			
			/* 商品マスタ */
			SET _p2_syhn_name = '\0';
			SET _p2_syhn_tani = '\0';
			SET _p2_syhng_code = 0;
      SET _p2_colr_code = 0;
      SET _p2_size_code = 0;
			SET _p2_skmb_kubn = '\0';
			SET _p2_odr_kubn = '\0';
			SET _p2_zakr_kubn = '\0';
			
			SELECT 
						SYHN_NAME
					, SYHN_TANI
					, SYHNG_CODE
					, COLR_CODE
					, SIZE_CODE
					, SKMB_KUBN
					, ODR_KUBN
					, ZAKR_KUBN
					INTO _p2_syhn_name
					, _p2_syhn_tani
					, _p2_syhng_code
					, _p2_colr_code
					, _p2_size_code
					, _p2_skmb_kubn
					, _p2_odr_kubn
					, _p2_zakr_kubn
			FROM MST_SYOHINS
			WHERE MST_SYOHINS.SYHN_CODE = _p1_syhn_code
			;
			
			/* カラーマスタ */
			SET _p3_colr_name = '\0';
			SELECT COLR_NAME
					INTO _p3_colr_name
			FROM MST_COLORS
			WHERE MST_COLORS.COLR_CODE = _p2_colr_code;
			
			/* サイズマスタ */
			SET _p4_size_name = '\0';
			SELECT SIZE_NAME
					INTO _p4_size_name
			FROM MST_SIZES
			WHERE MST_SIZES.SIZE_CODE = _p2_size_code;
			
			/* 在庫調整Ｂ更新 */
			SET @_s1_dps_num = CAST(_s1_dps_num AS DECIMAL(14,0));
			SET @_p1_tmn_zaiking = CAST(_p1_tmn_zaiking AS DECIMAL(11,0));
			INSERT TRN_ZCYBS (
						 SRNUM, GYNUM, GYM_CODE
						 , DNPYNUM, TESEI_KUBN, ODRIGAI_KUBN
						 , KEJ_DATE, MON_KUBN, TANT_CODE
						 , ZAIK_BUMN_CODE, KEIH_BUMN_CODE, RIYU_CODE
						 , KMK_CODE, ZAIK_ZOGN_KUBN, SYHN_CODE
						 , KYU, SYHN_NAME, COLR_NAME
						 , SIZE_NAME, SURY, TANI
						 , GT_GEN_TANK, GT_GEN_KING, GM_GEN_TANK
						 , GM_GEN_KING, UPD_SURY, UPD_KING
						 , SKMB_KUBN, ODR_KUBN, ZAKR_KUBN
						 , SYHNG_CODE, YOBI01_NUM
						 , GENKA_KUBN, CRT_DATE, CRT_PGM_ID
						 , CRT_USER_ID, CREATED, UPD_PGM_ID
						 , UPD_USER_ID, MODIFIED, SIHA_YOTE_DATE
						 , SIHA_SIME_DATE, SIHA_KEI_DATE, SIHA_ZUMI_TS)
						 VALUES (
						 @_s1_dps_num, _ctl_gyo_count, 'Z100',
						 _s1_dpn_num, '0', '1',
						 _monthly_to_date, '1', '000172',
						 _p1_bumn_code, '000141', _zcy_riyu_code_zou,
						 _zcy_kamk_code_zou, _zcy_zgen_kubn_zou, _p1_syhn_code,
						 _p1_kyu, _p2_syhn_name, _p3_colr_name,
						 _p4_size_name, 0.00, _p2_syhn_tani,
						 0.00, @_p1_tmn_zaiking, 0.00,
						 @_p1_tmn_zaiking, 0.00, @_p1_tmn_zaiking,
						 _p2_skmb_kubn, _p2_odr_kubn, _p2_zakr_kubn,
						 _p2_syhng_code, 1,
						 '0', _keijyo_date, _pgmid,
						 Null, CURRENT_TIMESTAMP, Null,
						 Null, Null, Null,
						 Null, Null, Null);			
		END WHILE dept_loop;
		CLOSE C0409_01;
		
		/***************************************************************/
    /* ２回目－処理 */
    /***************************************************************/

    /* コントロールブレイクキー初期化 */
		SEL_SECOND: BEGIN
		DECLARE C0409_02 CURSOR FOR
		SELECT BUMN_CODE, SYHN_CODE, KYU, TMN_ZAIKING
		FROM MST_ZAI_RDYS
		WHERE MST_ZAI_RDYS.ZAKR_KUBN = IN_ZAKR_KUBN /*1*/
			AND MST_ZAI_RDYS.TMN_ZAISURY = 0
			AND MST_ZAI_RDYS.TMN_ZAIKING < 0
			ORDER BY BUMN_CODE, SYHN_CODE, KYU
		;
		
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET IsNotFound=1;
		END;
		
		SET IsNotFound=0;
		
		/* INIT */
		SET _ctl_gyo_count = 1000;
		SET _ctl_bumn_code = '\0';
		
		OPEN C0409_02;
		dept_loop:WHILE(IsNotFound=0) DO
	
			SET _p1_bumn_code = '\0';
			SET _p1_syhn_code = '\0';
			SET _p1_kyu = '\0';
			
			SET _p1_tmn_zaiking = 0;
					
			FETCH FROM C0409_01
				INTO
				_p1_bumn_code,
				_p1_syhn_code,
				_p1_kyu,
				_p1_tmn_zaiking
			;
			IF IsNotFound=1 THEN
				SET fnReturn = KFAIL;
				SET fnMsg = '処理2 IsNotFound';
				LEAVE SEL_MAIN;
			END IF;
			/* 行番号加算 */
			SET _ctl_gyo_count = _ctl_gyo_count + 1;
			/* コントロールブレイクの判断 */
      IF _ctl_gyo_count > 999 OR strcmp(_ctl_bumn_code, _p1_bumn_code) <> 0 THEN
				/* コントロールブレイクキー初期化 */
				SET _ctl_gyo_count = 1;
				SET _ctl_bumn_code = '\0';
				
				/* シリアル№採番 */
        SET _s1_dps_num = 0;
        SET _s1_err_code = 0;
				
				call ANUMDPS(_s1_dps_num,_s1_err_code);
				
				IF _s1_err_code <> 0 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = "シリアル№採番 [`_s1_err_code`!= 0]";
					LEAVE dept_loop;
				END IF;
				
				/* 伝票№採番 */
				SET _s1_dpn_num = 0;
				SET _s1_err_code = 0;
				
				call ANUMDPN1(_s1_DPN_KEY,_s1_dpn_num,_s1_err_code);

				IF _s1_err_code <> 0 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = "伝票№採番 [`_s1_err_code`!= 0]";
					LEAVE dept_loop;
				END IF;
				
				/* 在庫調整Ｈ更新 */
				UPT: BEGIN
					DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
					BEGIN
						 SET SQLCODE = 100;
					END;
					SET @_s1_dps_num = CAST(_s1_dps_num AS DECIMAL(14,0));
					
					INSERT INTO TRN_ZCYHS (
										 SRNUM, GYM_CODE, DNPYNUM
										 , TESEI_KUBN, ODRIGAI_KUBN, KEJ_DATE
										 , MON_KUBN, TANT_CODE, ZAIK_BUMN_CODE
										 , KEIH_BUMN_CODE, RIYU_CODE, KMK_CODE
										 , ZAIK_ZOGN_KUBN, PRT_KUBN, YOBI01_NUM
										 , CRT_DATE
										 , CRT_PGM_ID, CRT_USER_ID, CREATED /*CRT_TS*/
										 , UPD_PGM_ID, UPD_USER_ID, MODIFIED /*UPD_TS*/)
									VALUES (
											@_s1_dps_num,'Z100',_s1_dpn_num,
											'0', '1', _monthly_to_date,
											'1', '000172', _p1_bumn_code,
											'000141', _zcy_riyu_code_gen, _zcy_kamk_code_gen,
											_zcy_zgen_kubn_gen, '1', 1,
											_keijyo_date,
											_pgmid, Null, CURRENT_TIMESTAMP,
											Null, Null, Null);
					IF SQLCODE = 100 THEN 
						SET fnReturn = KFAIL;
						SET fnMsg = '在庫調整Ｈ更新 - INSERT FAILED';
						LEAVE dept_loop;
					END IF;
				END UPT;
			END IF;
			
			/* 商品マスタ */
			SET _p2_syhn_name = '\0';
			SET _p2_syhn_tani = '\0';
			SET _p2_syhng_code = 0;
      SET _p2_colr_code = 0;
      SET _p2_size_code = 0;
			SET _p2_skmb_kubn = '\0';
			SET _p2_odr_kubn = '\0';
			SET _p2_zakr_kubn = '\0';
			
			SELECT 
						SYHN_NAME
					, SYHN_TANI
					, SYHNG_CODE
					, COLR_CODE
					, SIZE_CODE
					, SKMB_KUBN
					, ODR_KUBN
					, ZAKR_KUBN
					INTO _p2_syhn_name
					, _p2_syhn_tani
					, _p2_syhng_code
					, _p2_colr_code
					, _p2_size_code
					, _p2_skmb_kubn
					, _p2_odr_kubn
					, _p2_zakr_kubn
			FROM MST_SYOHINS
			WHERE MST_SYOHINS.SYHN_CODE = _p1_syhn_code
			;
			
			/* カラーマスタ */
			SET _p3_colr_name = '\0';
			SELECT COLR_NAME
					INTO _p3_colr_name
			FROM MST_COLORS
			WHERE MST_COLORS.COLR_CODE = _p2_colr_code;
			
			/* サイズマスタ */
			SET _p4_size_name = '\0';
			SELECT SIZE_NAME
					INTO _p4_size_name
			FROM MST_SIZES
			WHERE MST_SIZES.SIZE_CODE = _p2_size_code;
			
			/* 在庫調整Ｂ更新 */
			SET @_s1_dps_num = CAST(_s1_dps_num AS DECIMAL(14,0));
			SET @_p1_tmn_zaiking = CAST(_p1_tmn_zaiking * -1 AS DECIMAL(11,0));
			SET @_p1_tmn_zaiking2 = CAST(_p1_tmn_zaiking AS DECIMAL(11,0));
			INSERT TRN_ZCYBS (
						 SRNUM, GYNUM, GYM_CODE
						 , DNPYNUM, TESEI_KUBN, ODRIGAI_KUBN
						 , KEJ_DATE, MON_KUBN, TANT_CODE
						 , ZAIK_BUMN_CODE, KEIH_BUMN_CODE, RIYU_CODE
						 , KMK_CODE, ZAIK_ZOGN_KUBN, SYHN_CODE
						 , KYU, SYHN_NAME, COLR_NAME
						 , SIZE_NAME, SURY, TANI
						 , GT_GEN_TANK, GT_GEN_KING, GM_GEN_TANK
						 , GM_GEN_KING, UPD_SURY, UPD_KING
						 , SKMB_KUBN, ODR_KUBN, ZAKR_KUBN
						 , SYHNG_CODE, YOBI01_NUM
						 , GENKA_KUBN, CRT_DATE, CRT_PGM_ID
						 , CRT_USER_ID, CREATED, UPD_PGM_ID
						 , UPD_USER_ID, MODIFIED, SIHA_YOTE_DATE
						 , SIHA_SIME_DATE, SIHA_KEI_DATE, SIHA_ZUMI_TS)
						 VALUES (
						 @_s1_dps_num, _ctl_gyo_count, 'Z100',
						 _s1_dpn_num, '0', '1',
						 _monthly_to_date, '1', '000172',
						 _p1_bumn_code, '000141', _zcy_riyu_code_zou,
						 _zcy_kamk_code_zou, _zcy_zgen_kubn_zou, _p1_syhn_code,
						 _p1_kyu, _p2_syhn_name, _p3_colr_name,
						 _p4_size_name, 0.00, _p2_syhn_tani,
						 0.00, @_p1_tmn_zaiking, 0.00,
						 @_p1_tmn_zaiking, 0.00, @_p1_tmn_zaiking2,
						 _p2_skmb_kubn, _p2_odr_kubn, _p2_zakr_kubn,
						 _p2_syhng_code, 1,
						 '0', _keijyo_date, _pgmid,
						 Null, CURRENT_TIMESTAMP, Null,
						 Null, Null, Null,
						 Null, Null, Null);			
			
		END WHILE dept_loop;
		CLOSE C0409_02;
		END SEL_SECOND;
	END SEL_MAIN;
END MAIN_PROC
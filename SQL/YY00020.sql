CREATE DEFINER=`root`@`localhost` PROCEDURE `YY00020`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bYY00020
	-- 機能		: 	信販・カードマスタの年次更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-22
	-- =============================================
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);

	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	
	SET _pgmid = '\0';
	
	UPT_MST_SHINPANS: BEGIN
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		 SET SQLCODE = 101;
	END;
	UPDATE MST_SHINPANS
		SET 	PYN_M_ZAN_CRJT_SURY = PMN_M_ZAN_CRJT_SURY
				, PYN_M_ZAN_GIFT_SURY = PMN_M_ZAN_GIFT_SURY
				, PYN_M_URZN          = PMN_M_URZN
				, RUI_M_URI_CRJT_SURY = TMN_M_URI_CRJT_SURY
				, RUI_M_URI_GIFT_SURY = TMN_M_URI_GIFT_SURY
				, RUI_M_URI_CRJT_KING = TMN_M_URI_CRJT_KING
				, RUI_M_URI_GIFT_KING = TMN_M_URI_GIFT_KING
				, PYN_ZAN_CRJT_SURY   = PMN_ZAN_CRJT_SURY
				, PYN_ZAN_GIFT_SURY   = PMN_ZAN_GIFT_SURY
				, PYN_URZN            = PMN_URZN
				, RUI_KSKM_CRJT_SURY  = TMN_KSKM_CRJT_SURY
				, RUI_KSKM_GIFT_SURY  = TMN_KSKM_GIFT_SURY
				, RUI_URI_CRJT_SURY   = TMN_URI_CRJT_SURY
				, RUI_URI_GIFT_SURY   = TMN_URI_GIFT_SURY
				, RUI_KSKM_CRJT_KING  = TMN_KSKM_CRJT_KING
				, RUI_KSKM_GIFT_KING  = TMN_KSKM_GIFT_KING
				, RUI_URI_CRJT_KING   = TMN_URI_CRJT_KING
				, RUI_URI_GIFT_KING   = TMN_URI_GIFT_KING
				, RUI_TRKM_CRJT_SURY  = TMN_TRKM_CRJT_SURY
				, RUI_TRKM_GIFT_SURY  = TMN_TRKM_GIFT_SURY
				, RUI_TRKM_CRJT_KING  = TMN_TRKM_CRJT_KING
				, RUI_TRKM_GIFT_KING  = TMN_TRKM_GIFT_KING
				, RUI_NYK_KING        = TMN_NYK_KING
				, RUI_NYK_TESUR       = TMN_NYK_TESUR
				, RUI_NYK_FURI_TESUR  = TMN_NYK_FURI_TESUR
				, RUI_NYK_SKIP_TESUR  = TMN_NYK_SKIP_TESUR
				, RUI_NYK_KRFT_TESUR  = TMN_NYK_KRFT_TESUR
				, RUI_NYK_TOTAL       = TMN_NYK_TOTAL
				, RUI_NYK_CDRY_TESUR  = TMN_NYK_CDRY_TESUR
				, RUI_NYK_UKTR_TESUR  = TMN_NYK_UKTR_TESUR
				, PGM_ID            	= _pgmid
				, USER_ID           	= NULL
				, MODIFIED           	= CURRENT_TIMESTAMP -- TS
		;
		
		IF SQLCODE = 101 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = 'UPT_MST_SHINPANS FAILED';
			LEAVE MAIN_PROC;
		END IF;
		END UPT_MST_SHINPANS;
	
END MAIN_PROC
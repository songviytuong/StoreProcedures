CREATE DEFINER=`root`@`localhost` PROCEDURE `DYZK050`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYZK050
	-- 機能		: 	仕入Ｂ支払区分更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-14
	-- =============================================
	
	DECLARE _s1_sqlcode INT;
	DECLARE _s1_sqlmsg CHAR(129);

	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';

	SET _s1_sqlcode = 0;
	SET _s1_sqlmsg = '\0';

	call UPSIH01(_s1_sqlcode,_s1_sqlmsg);
	
END MAIN_PROC
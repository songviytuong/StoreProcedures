CREATE DEFINER=`root`@`localhost` PROCEDURE `DYCR020`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bDYCR020
	-- 機能		: 	クオーク 立替金データ累積処理
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-20, 2019-02-21
	-- =============================================
	
	/* クレジット自動入金ＣＴＬＦ（クオーク） */
	DECLARE _p1_dm_key INT;
	DECLARE _p1_db_inpt_flag CHAR(2);
	DECLARE _p1_db_inpt_date CHAR(11);
	DECLARE _p1_db_inpt_cnt INT;
	DECLARE _p1_db_head_flag CHAR(2);
	DECLARE _p1_db_head_itak_cd CHAR(6);
	DECLARE _p1_db_head_crt_date CHAR(7);
	DECLARE _p1_db_head_sime_date CHAR(7);
	DECLARE _p1_db_head_siha_date CHAR(7);
	DECLARE _p1_db_head_from_date CHAR(7);
	DECLARE _p1_db_head_to_date CHAR(7);
	DECLARE _p1_db_head_crt_date_2 CHAR(11);
	DECLARE _p1_db_head_sime_date_2 CHAR(11);
	DECLARE _p1_db_head_siha_date_2 CHAR(11);
	DECLARE _p1_db_head_from_date_2 CHAR(11);
	DECLARE _p1_db_head_to_date_2 CHAR(11);
	DECLARE _p1_db_head_itak_name CHAR(21);
	DECLARE _p1_db_head_sinp_cd CHAR(7);
	DECLARE _p1_db_head_sinp_name CHAR(21);
	DECLARE _p1_db_tral_flag CHAR(2);
	DECLARE _p1_db_tralitak_cd CHAR(6);
	DECLARE _p1_db_traluri_cnt INT;
	DECLARE _p1_db_tral_uri_king DOUBLE;
	DECLARE _p1_db_tral_koj_cnt INT;
	DECLARE _p1_db_tral_koj_king DOUBLE;
	DECLARE _p1_db_tral_sas_king DOUBLE;
	DECLARE _p1_db_end_flag CHAR(2);
	DECLARE _p1_db_end_itak_cd CHAR(6);
	DECLARE _p1_data_crt_flag CHAR(2);
	DECLARE _p1_data_crt_date CHAR(11);
	DECLARE _p1_data_crt_cnt INT;
	DECLARE _p1_data_crt_uri_cnt INT;
	DECLARE _p1_data_crt_uri1_king DOUBLE;
	DECLARE _p1_data_crt_uri2_king DOUBLE;
	DECLARE _p1_data_crt_koj_cnt INT;
	DECLARE _p1_data_crt_koj1_king DOUBLE;
	DECLARE _p1_data_crt_koj2_king DOUBLE;
	DECLARE _p1_data_crt_etc_cnt INT;
	DECLARE _p1_data_crt_etc1_king DOUBLE;
	DECLARE _p1_data_crt_etc2_king DOUBLE;
	DECLARE _p1_data_crt_err_flag CHAR(2);
	DECLARE _p1_data_crt_err_num INT;
	DECLARE _p1_auto_hiki_flag CHAR(2);
	DECLARE _p1_auto_hiki_date CHAR(11);
	
	DECLARE _p1_auto_hiki_cnt INT;
	DECLARE _p1_auto_hiki_uri_cnt INT;
	DECLARE _p1_auto_hiki_uri1_king DOUBLE;
	DECLARE _p1_auto_hiki_uri2_king DOUBLE;
	DECLARE _p1_auto_hiki_koj_cnt INT;
	DECLARE _p1_auto_hiki_koj1_king DOUBLE;
	DECLARE _p1_auto_hiki_koj2_king DOUBLE;
	DECLARE _p1_auto_jgai_cnt INT;
	DECLARE _p1_auto_jgai_uri_cnt INT;
	DECLARE _p1_auto_jgai_uri1_king DOUBLE;
	DECLARE _p1_auto_jgai_uri2_king DOUBLE;
	DECLARE _p1_auto_jgai_koj_cnt INT;
	DECLARE _p1_auto_jgai_koj1_king DOUBLE;
	DECLARE _p1_auto_jgai_koj2_king DOUBLE;
	DECLARE _p1_auto_kskm_yoyk_flag CHAR(2);
	DECLARE _p1_auto_kskm_yoyk_date CHAR(11);
	DECLARE _p1_auto_kskm_flag CHAR(2);
	DECLARE _p1_auto_kskm_date CHAR(11);
	DECLARE _p1_auto_kskm_cnt INT;
	DECLARE _p1_auto_kskm_uri_cnt INT;
	DECLARE _p1_auto_kskm_uri1_king DOUBLE;
	DECLARE _p1_auto_kskm_uri2_king DOUBLE;
	DECLARE _p1_auto_kskm_koj_cnt INT;
	DECLARE _p1_auto_kskm_koj1_king DOUBLE;
	DECLARE _p1_auto_kskm_koj2_king DOUBLE;
	DECLARE _p1_data_ruisk_flag CHAR(2);
	DECLARE _p1_data_ruisk_date CHAR(11);
	DECLARE _p1_yobi01_flag CHAR(2);
	DECLARE _p1_yobi02_flag CHAR(2);
	DECLARE _p1_yobi03_flag CHAR(2);
	DECLARE _p1_yobi01_kubn CHAR(2);
	DECLARE _p1_yobi02_kubn CHAR(3);
	DECLARE _p1_yobi03_kubn CHAR(7);
	DECLARE _p1_yobi01_king DOUBLE;
	DECLARE _p1_yobi02_king DOUBLE;
	DECLARE _p1_yobi03_king DOUBLE;
	DECLARE _p1_del_flag CHAR(2);
	DECLARE _p1_crt_pgm_id CHAR(65);
	DECLARE _p1_crt_user_id CHAR(65);
	DECLARE _p1_crt_ts CHAR(19);
	DECLARE _p1_upd_pgm_id CHAR(65);
	DECLARE _p1_upd_user_id CHAR(65);
	DECLARE _p1_upd_ts CHAR(19);
	DECLARE _p1_quoq_nyk_date CHAR(11);
	DECLARE _in_p1_01 SMALLINT;
	DECLARE _in_p1_02 SMALLINT;
	DECLARE _in_p1_03 SMALLINT;
	DECLARE _in_p1_04 SMALLINT;
	DECLARE _in_p1_05 SMALLINT;
	DECLARE _in_p1_06 SMALLINT;
	DECLARE _in_p1_07 SMALLINT;
	DECLARE _in_p1_08 SMALLINT;
	DECLARE _in_p1_09 SMALLINT;
	DECLARE _in_p1_10 SMALLINT;
	DECLARE _in_p1_11 SMALLINT;
	DECLARE _in_p1_12 SMALLINT;
	DECLARE _in_p1_13 SMALLINT;
	
	/* 立替金ＴＲ：明細データ（クオーク） */
	DECLARE _p2_dnpynum DOUBLE;
	DECLARE _p2_gynum INT;
	DECLARE _p2_data_kubn CHAR(2);
	DECLARE _p2_data_sybt CHAR(3);
	DECLARE _p2_kame_num_1 CHAR(7);
	DECLARE _p2_kame_num_2 CHAR(4);
	DECLARE _p2_kame_num_3 CHAR(4);
	DECLARE _p2_mous_num CHAR(14);
	DECLARE _p2_mous_name_kana CHAR(21);
	DECLARE _p2_mous_uke_date CHAR(7);
	DECLARE _p2_trkm_date CHAR(7);
	DECLARE _p2_syoy_king CHAR(10);
	DECLARE _p2_shiha_kais CHAR(4);
	DECLARE _p2_syo_siha_date CHAR(7);
	DECLARE _p2_koky_tesur_king CHAR(8);
	DECLARE _p2_kame_tesur_king CHAR(8);
	DECLARE _p2_skip_tesur_king CHAR(8);
	DECLARE _p2_yobi_01 CHAR(8);
	DECLARE _p2_yobi_02 CHAR(8);
	DECLARE _p2_sasi_siha_king CHAR(10);
	DECLARE _p2_tel CHAR(13);
	DECLARE _p2_dnp_num CHAR(11);
	DECLARE _p2_dair_ten_code CHAR(21);
	DECLARE _p2_kame_tant_code CHAR(11);
	DECLARE _p2_kame_seri_num CHAR(17);
	DECLARE _p2_tesur_kame2_king CHAR(8);
	DECLARE _p2_tesur_siha2_king CHAR(8);
	DECLARE _p2_trkm_hiyo_king CHAR(8);
	DECLARE _p2_card_num CHAR(17);
	DECLARE _p2_yobi_03 CHAR(12);
	DECLARE _p2_tate_sime_date CHAR(7);
	DECLARE _p2_tate_siha_date CHAR(7);
	DECLARE _p2_cnv_flag CHAR(2);
	DECLARE _p2_cnv_date CHAR(11);
	DECLARE _p2_cnv_ts CHAR(19);
	DECLARE _p2_cnv_err01_flag CHAR(2);
	DECLARE _p2_cnv_err02_flag CHAR(2);
	DECLARE _p2_cnv_err03_flag CHAR(2);
	DECLARE _p2_cnv_err04_flag CHAR(2);
	DECLARE _p2_cnv_err05_flag CHAR(2);
	DECLARE _p2_cnv_err06_flag CHAR(2);
	DECLARE _p2_cnv_err07_flag CHAR(2);
	DECLARE _p2_cnv_err08_flag CHAR(2);
	DECLARE _p2_cnv_err09_flag CHAR(2);
	DECLARE _p2_cnv_err10_flag CHAR(2);
	DECLARE _p2_crjt_kame_num CHAR(21);
	DECLARE _p2_chk_sinp_code INT;
	DECLARE _p2_chk_siha_houh_kubn CHAR(2);
	DECLARE _p2_chk_sinp_egtn_code CHAR(7);
	DECLARE _p2_chk_synin_num CHAR(17);
	DECLARE _p2_chk_name_kana CHAR(23);
	DECLARE _p2_chk_syoy_king DOUBLE;
	DECLARE _p2_chk_shiha_kais DOUBLE;
	DECLARE _p2_chk_koky_tesur_king DOUBLE;
	DECLARE _p2_chk_kame_tesur_king DOUBLE;
	DECLARE _p2_chk_skip_tesur_king DOUBLE;
	DECLARE _p2_chk_sasi_siha_king DOUBLE;
	DECLARE _p2_chk_tesur_kame2_king DOUBLE;
	DECLARE _p2_chk_tesur_siha2_king DOUBLE;
	DECLARE _p2_chk_trkm_hiyo_king DOUBLE;
	DECLARE _p2_chk_yobi01_king DOUBLE;
	DECLARE _p2_chk_yobi02_king DOUBLE;
	DECLARE _p2_chk_yobi03_king DOUBLE;
	DECLARE _p2_match_err_flag CHAR(2);
	DECLARE _p2_match_date CHAR(11);
	DECLARE _p2_match_ts CHAR(19);
	DECLARE _p2_match_err01_flag CHAR(2);
	DECLARE _p2_match_err02_flag CHAR(2);
	DECLARE _p2_match_err03_flag CHAR(2);
	DECLARE _p2_match_err04_flag CHAR(2);
	DECLARE _p2_match_err05_flag CHAR(2);
	DECLARE _p2_match_err06_flag CHAR(2);
	DECLARE _p2_match_err07_flag CHAR(2);
	DECLARE _p2_match_err08_flag CHAR(2);
	DECLARE _p2_match_err09_flag CHAR(2);
	DECLARE _p2_match_err10_flag CHAR(2);
	DECLARE _p2_hiki_flag CHAR(2);
	DECLARE _p2_kony_dnpynum DOUBLE;
	DECLARE _p2_kony_srnum DOUBLE;
	DECLARE _p2_rirk_num INT;
	DECLARE _p2_head_vernum INT;
	DECLARE _p2_lognum INT;
	DECLARE _p2_kkyk_code CHAR(13);
	DECLARE _p2_kkyk_kana CHAR(23);
	DECLARE _p2_kkyk_kana_myoji CHAR(12);
	DECLARE _p2_kkyk_kana_namae CHAR(12);
	DECLARE _p2_kskm_flag CHAR(2);
	DECLARE _p2_kskm_date CHAR(11);
	DECLARE _p2_kskm_ts CHAR(19);
	DECLARE _p2_crny_dnpynum INT;
	DECLARE _p2_crny_srnum DOUBLE;
	DECLARE _p2_crny_gynum INT;
	DECLARE _p2_yobi01_flag CHAR(2);
	DECLARE _p2_yobi02_flag CHAR(2);
	DECLARE _p2_yobi03_flag CHAR(2);
	DECLARE _p2_yobi01_kubn CHAR(2);
	DECLARE _p2_yobi02_kubn CHAR(3);
	DECLARE _p2_yobi03_kubn CHAR(7);
	DECLARE _p2_yobi01_king DOUBLE;
	DECLARE _p2_yobi02_king DOUBLE;
	DECLARE _p2_yobi03_king DOUBLE;
	DECLARE _p2_del_flag CHAR(2);
	DECLARE _p2_crt_date CHAR(11);
	DECLARE _p2_crt_pgm_id CHAR(65);
	DECLARE _p2_crt_user_id CHAR(65);
	DECLARE _p2_crt_ts CHAR(19);
	DECLARE _p2_upd_pgm_id CHAR(65);
	DECLARE _p2_upd_user_id CHAR(65);
	DECLARE _p2_upd_ts CHAR(19);
	DECLARE _in_p2_01 SMALLINT;
	DECLARE _in_p2_02 SMALLINT;
	DECLARE _in_p2_03 SMALLINT;
	DECLARE _in_p2_04 SMALLINT;
	DECLARE _in_p2_05 SMALLINT;
	DECLARE _in_p2_06 SMALLINT;
	DECLARE _in_p2_07 SMALLINT;
	DECLARE _in_p2_08 SMALLINT;
	DECLARE _in_p2_09 SMALLINT;
	DECLARE _in_p2_10 SMALLINT;
	DECLARE _in_p2_11 SMALLINT;
	DECLARE _in_p2_12 SMALLINT;
	
	-- CUSTOM: DECLARE VARIABLES
	DECLARE _p1_db_inpt_date_01 CHAR(11);
	DECLARE _p1_data_crt_date_02 CHAR(11);
	DECLARE _p1_auto_hiki_date_03 CHAR(11);
	DECLARE _p1_auto_kskm_yoyk_date_04 CHAR(11);
	DECLARE _p1_auto_kskm_date_05 CHAR(11);
	DECLARE _p1_data_ruisk_date_06 CHAR(11);
	DECLARE _p1_crt_pgm_id_07 CHAR(65);
	DECLARE _p1_crt_user_id_08 CHAR(65);
	DECLARE _p1_crt_ts_09 CHAR(19);
	DECLARE _p1_upd_pgm_id_10 CHAR(65);
	DECLARE _p1_upd_user_id_11 CHAR(65);
	DECLARE _p1_upd_ts_12 CHAR(19);
	DECLARE _p1_quoq_nyk_date_13 CHAR(11); 
	
	DECLARE _p2_cnv_ts_01 CHAR(19);
	DECLARE _p2_match_date_02 CHAR(11);
	DECLARE _p2_match_ts_03 CHAR(19);
	DECLARE _p2_kskm_date_04 CHAR(11);
	DECLARE _p2_kskm_ts_05 CHAR(19);
	DECLARE _p2_crt_date_06 CHAR(11);
	DECLARE _p2_crt_pgm_id_07 CHAR(65);
	DECLARE _p2_crt_user_id_08 CHAR(65);
	DECLARE _p2_crt_ts_09 CHAR(19);
	DECLARE _p2_upd_pgm_id_10 CHAR(65);
	DECLARE _p2_upd_user_id_11 CHAR(65);
	DECLARE _p2_upd_ts_12 CHAR(19);
	-- END CUSTOM: DECLARE VARIABLES
	
	DECLARE _wk_hojo_num INT;
	DECLARE _in_wk1_01 SMALLINT;
	
	/* 日付コントロールＦ */
	DECLARE _DM_KEY INT DEFAULT 0;
	DECLARE _kei_date DATE; 
	
	DECLARE _auto_kskm_flag CHAR(2);
	DECLARE _data_ruisk_flag CHAR(2);
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	/* 初期パラメータ */
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	/* 初期設定 */
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	SET _pgmid = '\0';
	SET _kei_date = '\0';
	
	/* 計上日を取得する */
	SELECT KEI_DATE
		INTO _kei_date
	FROM MST_HIZS
	WHERE DM_KEY = _DM_KEY
	;
	
	IF _kei_date = '\0' THEN 
		SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("計上日を取得する IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* クレジット自動入金ＣＴＬＦ（クオーク）のフラグチェック */
	SET _auto_kskm_flag = '\0';
	SET _data_ruisk_flag = '\0';
	
	SELECT AUTO_KSKM_FLAG, DATA_RUISK_FLAG
		INTO _auto_kskm_flag, _data_ruisk_flag
	FROM MST_TATE_QUOQ_CTLS
	WHERE DM_KEY = _DM_KEY;
	
	IF (_auto_kskm_flag IS NULL OR _data_ruisk_flag IS NULL OR _auto_kskm_flag <> '1' OR _data_ruisk_flag <> '0') THEN 
		SET SQLCODE = 100;
	END IF;
	IF SQLCODE = 100 THEN
		SET fnReturn = KFAIL;
		SET fnMsg = REPLACE("クレジット自動入金ＣＴＬＦ（クオーク）のフラグチェック IsNotFound [`DM_KEY`='ss']","ss",_DM_KEY);
		LEAVE MAIN_PROC;
	END IF;
	
	/* クレジット自動入金ＣＴＬＦ（クオーク）のフラグ更新 */
	UPT_MST_TATE_QUOQ_CTLS:BEGIN
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		 SET SQLCODE = 101;
	END;
	UPDATE MST_TATE_QUOQ_CTLS
				SET DATA_RUISK_FLAG  = '1'
			, DATA_RUISK_DATE = _kei_date
	WHERE DM_KEY = _DM_KEY
	;
	IF SQLCODE = 101 THEN 
		SET fnReturn = KFAIL;
		SET fnMsg = 'UPT_MST_TATE_QUOQ_CTLS FAILED';
		LEAVE MAIN_PROC;
	END IF;
	END UPT_MST_TATE_QUOQ_CTLS;
	
	/***************************************************************/
	/* 初期処理                                                    */
	/***************************************************************/
	/* クレジット自動入金ＣＴＬＦ（クオーク）の補助№を取得 */
	SET _wk_hojo_num = 0;
	SELECT max(HOJO_NUM)
		 INTO _wk_hojo_num, _in_wk1_01
	FROM TRN_TATE_RUI_QUOQ_CTLS
	WHERE TATE_CRT_DATE = _kei_date
	;
	
	IF _in_wk1_01 < 0 THEN
		SET _wk_hojo_num = 0;
	END IF;
	
	SEL_MAIN: BEGIN
	-- 01
		DECLARE CR020_01 CURSOR FOR
		SELECT DM_KEY
			 , DB_INPT_FLAG
			 , DB_INPT_DATE
			 , DB_INPT_CNT
			 , DB_HEAD_FLAG
			 , DB_HEAD_ITAK_CD
			 , DB_HEAD_CRT_DATE
			 , DB_HEAD_SIME_DATE
			 , DB_HEAD_SIHA_DATE
			 , DB_HEAD_FROM_DATE
			 , DB_HEAD_TO_DATE
			 , DB_HEAD_CRT_DATE_2
			 , DB_HEAD_SIME_DATE_2
			 , DB_HEAD_SIHA_DATE_2
			 , DB_HEAD_FROM_DATE_2
			 , DB_HEAD_TO_DATE_2
			 , DB_HEAD_ITAK_NAME
			 , DB_HEAD_SINP_CD
			 , DB_HEAD_SINP_NAME
			 , DB_TRAL_FLAG
			 , DB_TRALITAK_CD
			 , DB_TRALURI_CNT
			 , DB_TRAL_URI_KING
			 , DB_TRAL_KOJ_CNT
			 , DB_TRAL_KOJ_KING
			 , DB_TRAL_SAS_KING
			 , DB_END_FLAG
			 , DB_END_ITAK_CD
			 , DATA_CRT_FLAG
			 , DATA_CRT_DATE
			 , DATA_CRT_CNT
			 , DATA_CRT_URI_CNT
			 , DATA_CRT_URI1_KING
			 , DATA_CRT_URI2_KING
			 , DATA_CRT_KOJ_CNT
			 , DATA_CRT_KOJ1_KING
			 , DATA_CRT_KOJ2_KING
			 , DATA_CRT_ETC_CNT
			 , DATA_CRT_ETC1_KING
			 , DATA_CRT_ETC2_KING
			 , DATA_CRT_ERR_FLAG
			 , DATA_CRT_ERR_NUM
			 , AUTO_HIKI_FLAG
			 , AUTO_HIKI_DATE
			 , AUTO_HIKI_CNT
			 , AUTO_HIKI_URI_CNT
			 , AUTO_HIKI_URI1_KING
			 , AUTO_HIKI_URI2_KING
			 , AUTO_HIKI_KOJ_CNT
			 , AUTO_HIKI_KOJ1_KING
			 , AUTO_HIKI_KOJ2_KING
			 , AUTO_JGAI_CNT
			 , AUTO_JGAI_URI_CNT
			 , AUTO_JGAI_URI1_KING
			 , AUTO_JGAI_URI2_KING
			 , AUTO_JGAI_KOJ_CNT
			 , AUTO_JGAI_KOJ1_KING
			 , AUTO_JGAI_KOJ2_KING
			 , AUTO_KSKM_YOYK_FLAG
			 , AUTO_KSKM_YOYK_DATE
			 , AUTO_KSKM_FLAG
			 , AUTO_KSKM_DATE
			 , AUTO_KSKM_CNT
			 , AUTO_KSKM_URI_CNT
			 , AUTO_KSKM_URI1_KING
			 , AUTO_KSKM_URI2_KING
			 , AUTO_KSKM_KOJ_CNT
			 , AUTO_KSKM_KOJ1_KING
			 , AUTO_KSKM_KOJ2_KING
			 , DATA_RUISK_FLAG
			 , DATA_RUISK_DATE
			 , YOBI01_FLAG
			 , YOBI02_FLAG
			 , YOBI03_FLAG
			 , YOBI01_KUBN
			 , YOBI02_KUBN
			 , YOBI03_KUBN
			 , YOBI01_KING
			 , YOBI02_KING
			 , YOBI03_KING
			 , DEL_FLAG
			 , CRT_PGM_ID
			 , CRT_USER_ID
			 , CRT_TS
			 , UPD_PGM_ID
			 , UPD_USER_ID
			 , UPD_TS
			 , QUOQ_NYK_DATE
		FROM MST_TATE_QUOQ_CTLS
	 WHERE DM_KEY = _DM_KEY;
	 
	 DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE=100;
		END;
	
		OPEN CR020_01;
		dept_loop:WHILE(SQLCODE=0) DO
			SET _p1_dm_key = 0;

			SET _p1_db_inpt_flag = '\0';
			SET _p1_db_inpt_date = '\0';

			SET _p1_db_inpt_cnt = 0;
			SET _p1_db_head_flag = '\0';
			SET _p1_db_head_itak_cd = '\0';
			SET _p1_db_head_crt_date = '\0';
			SET _p1_db_head_sime_date = '\0';
			SET _p1_db_head_siha_date = '\0';
			SET _p1_db_head_from_date = '\0';
			SET _p1_db_head_to_date = '\0';
			SET _p1_db_head_crt_date_2 = '\0';
			SET _p1_db_head_sime_date_2 = '\0';
			SET _p1_db_head_siha_date_2 = '\0';
			SET _p1_db_head_from_date_2 = '\0';
			SET _p1_db_head_to_date_2 = '\0';
			SET _p1_db_head_itak_name = '\0';
			SET _p1_db_head_sinp_cd = '\0';
			SET _p1_db_head_sinp_name = '\0';
			SET _p1_db_tral_flag = '\0';
			SET _p1_db_tralitak_cd = '\0';
			SET _p1_db_traluri_cnt = 0;
			SET _p1_db_tral_uri_king = 0;
			SET _p1_db_tral_koj_cnt = 0;
			SET _p1_db_tral_koj_king = 0;
			SET _p1_db_tral_sas_king = 0;
			SET _p1_db_end_flag = '\0';
			SET _p1_db_end_itak_cd = '\0';
			SET _p1_data_crt_flag = '\0';
			SET _p1_data_crt_date = '\0';

			SET _p1_data_crt_cnt = 0;
			SET _p1_data_crt_uri_cnt = 0;
			SET _p1_data_crt_uri1_king = 0;
			SET _p1_data_crt_uri2_king = 0;
			SET _p1_data_crt_koj_cnt = 0;
			SET _p1_data_crt_koj1_king = 0;
			SET _p1_data_crt_koj2_king = 0;
			SET _p1_data_crt_etc_cnt = 0;
			SET _p1_data_crt_etc1_king = 0;
			SET _p1_data_crt_etc2_king = 0;

			SET _p1_data_crt_err_flag = '\0';
			SET _p1_data_crt_err_num = 0;
			SET _p1_auto_hiki_flag = '\0';
			SET _p1_auto_hiki_date = '\0';

			SET _p1_auto_hiki_cnt = 0;
			SET _p1_auto_hiki_uri_cnt = 0;
			SET _p1_auto_hiki_uri1_king = 0;
			SET _p1_auto_hiki_uri2_king = 0;
			SET _p1_auto_hiki_koj_cnt = 0;
			SET _p1_auto_hiki_koj1_king = 0;
			SET _p1_auto_hiki_koj2_king = 0;
			SET _p1_auto_jgai_cnt = 0;
			SET _p1_auto_jgai_uri_cnt = 0;
			SET _p1_auto_jgai_uri1_king = 0;
			SET _p1_auto_jgai_uri2_king = 0;
			SET _p1_auto_jgai_koj_cnt = 0;
			SET _p1_auto_jgai_koj1_king = 0;
			SET _p1_auto_jgai_koj2_king = 0;

			SET _p1_auto_kskm_yoyk_flag = '\0';
			SET _p1_auto_kskm_yoyk_date = '\0';
			SET _p1_auto_kskm_flag = '\0';
			SET _p1_auto_kskm_date = '\0';

			SET _p1_auto_kskm_cnt = 0;
			SET _p1_auto_kskm_uri_cnt = 0;
			SET _p1_auto_kskm_uri1_king = 0;
			SET _p1_auto_kskm_uri2_king = 0;
			SET _p1_auto_kskm_koj_cnt = 0;
			SET _p1_auto_kskm_koj1_king = 0;
			SET _p1_auto_kskm_koj2_king = 0;

			SET _p1_data_ruisk_flag = '\0';
			SET _p1_data_ruisk_date = '\0';
			SET _p1_yobi01_flag = '\0';
			SET _p1_yobi02_flag = '\0';
			SET _p1_yobi03_flag = '\0';
			SET _p1_yobi01_kubn = '\0';
			SET _p1_yobi02_kubn = '\0';
			SET _p1_yobi03_kubn = '\0';

			SET _p1_yobi01_king = 0;
			SET _p1_yobi02_king = 0;
			SET _p1_yobi03_king = 0;

			SET _p1_del_flag = '\0';
			SET _p1_crt_pgm_id = '\0';
			SET _p1_crt_user_id = '\0';
			SET _p1_crt_ts = '\0';
			SET _p1_upd_pgm_id = '\0';
			SET _p1_upd_user_id = '\0';
			SET _p1_upd_ts = '\0';
			SET _p1_quoq_nyk_date = '\0';

			SET _in_p1_01 = 0;
			SET _in_p1_02 = 0;
			SET _in_p1_03 = 0;
			SET _in_p1_04 = 0;
			SET _in_p1_05 = 0;
			SET _in_p1_06 = 0;
			SET _in_p1_07 = 0;
			SET _in_p1_08 = 0;
			SET _in_p1_09 = 0;
			SET _in_p1_10 = 0;
			SET _in_p1_11 = 0;
			SET _in_p1_12 = 0;
			SET _in_p1_13 = 0;
			
			-- CUSTOM: SET VARIABLES
			-- ?
			-- END CUSTOM: SET VARIABLES
			
			FETCH FROM CR020_01
				INTO
					_p1_dm_key,
					_p1_db_inpt_flag,
					_p1_db_inpt_date_01,
					_p1_db_inpt_cnt,
					_p1_db_head_flag,
					_p1_db_head_itak_cd,
					_p1_db_head_crt_date,
					_p1_db_head_sime_date,
					_p1_db_head_siha_date,
					_p1_db_head_from_date,
					_p1_db_head_to_date,
					_p1_db_head_crt_date_2,
					_p1_db_head_sime_date_2,
					_p1_db_head_siha_date_2,
					_p1_db_head_from_date_2,
					_p1_db_head_to_date_2,
					_p1_db_head_itak_name,
					_p1_db_head_sinp_cd,
					_p1_db_head_sinp_name,
					_p1_db_tral_flag,
					_p1_db_tralitak_cd,
					_p1_db_traluri_cnt,
					_p1_db_tral_uri_king,
					_p1_db_tral_koj_cnt,
					_p1_db_tral_koj_king,
					_p1_db_tral_sas_king,
					_p1_db_end_flag,
					_p1_db_end_itak_cd,
					_p1_data_crt_flag,
					_p1_data_crt_date_02,
					_p1_data_crt_cnt,
					_p1_data_crt_uri_cnt,
					_p1_data_crt_uri1_king,
					_p1_data_crt_uri2_king,
					_p1_data_crt_koj_cnt,
					_p1_data_crt_koj1_king,
					_p1_data_crt_koj2_king,
					_p1_data_crt_etc_cnt,
					_p1_data_crt_etc1_king,
					_p1_data_crt_etc2_king,
					_p1_data_crt_err_flag,
					_p1_data_crt_err_num,
					_p1_auto_hiki_flag,
					_p1_auto_hiki_date_03,
					_p1_auto_hiki_cnt,
					_p1_auto_hiki_uri_cnt,
					_p1_auto_hiki_uri1_king,
					_p1_auto_hiki_uri2_king,
					_p1_auto_hiki_koj_cnt,
					_p1_auto_hiki_koj1_king,
					_p1_auto_hiki_koj2_king,
					_p1_auto_jgai_cnt,
					_p1_auto_jgai_uri_cnt,
					_p1_auto_jgai_uri1_king,
					_p1_auto_jgai_uri2_king,
					_p1_auto_jgai_koj_cnt,
					_p1_auto_jgai_koj1_king,
					_p1_auto_jgai_koj2_king,
					_p1_auto_kskm_yoyk_flag,
					_p1_auto_kskm_yoyk_date_04,
					_p1_auto_kskm_flag,
					_p1_auto_kskm_date_05,
					_p1_auto_kskm_cnt,
					_p1_auto_kskm_uri_cnt,
					_p1_auto_kskm_uri1_king,
					_p1_auto_kskm_uri2_king,
					_p1_auto_kskm_koj_cnt,
					_p1_auto_kskm_koj1_king,
					_p1_auto_kskm_koj2_king,
					_p1_data_ruisk_flag,
					_p1_data_ruisk_date_06,
					_p1_yobi01_flag,
					_p1_yobi02_flag,
					_p1_yobi03_flag,
					_p1_yobi01_kubn,
					_p1_yobi02_kubn,
					_p1_yobi03_kubn,
					_p1_yobi01_king,
					_p1_yobi02_king,
					_p1_yobi03_king,
					_p1_del_flag,
					_p1_crt_pgm_id_07,
					_p1_crt_user_id_08,
					_p1_crt_ts_09,
					_p1_upd_pgm_id_10,
					_p1_upd_user_id_11,
					_p1_upd_ts_12,
					_p1_quoq_nyk_date_13
				;
				
				IF _p1_dm_key = 0 THEN
					SET SQLCODE = 100;
				END IF;
				
				IF SQLCODE = 100 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = '1. クレジット自動入金ＣＴＬＦ（クオーク）読込み IsNotFound';
					LEAVE dept_loop;
				END IF;
				
			/* 補助№加算 */
      SET _wk_hojo_num = _wk_hojo_num + 1;
			INS_TRN_TATE_RUI_QUOQ_CTLS:BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				 SET SQLCODE = 102;
			END;
			INSERT INTO TRN_TATE_RUI_QUOQ_CTLS (
					 TATE_CRT_DATE
				 , HOJO_NUM
				 , DB_INPT_FLAG
				 , DB_INPT_DATE
				 , DB_INPT_CNT
				 , DB_HEAD_FLAG
				 , DB_HEAD_ITAK_CD
				 , DB_HEAD_CRT_DATE
				 , DB_HEAD_SIME_DATE
				 , DB_HEAD_SIHA_DATE
				 , DB_HEAD_FROM_DATE
				 , DB_HEAD_TO_DATE
				 , DB_HEAD_CRT_DATE_2
				 , DB_HEAD_SIME_DATE_2
				 , DB_HEAD_SIHA_DATE_2
				 , DB_HEAD_FROM_DATE_2
				 , DB_HEAD_TO_DATE_2
				 , DB_HEAD_ITAK_NAME
				 , DB_HEAD_SINP_CD
				 , DB_HEAD_SINP_NAME
				 , DB_TRAL_FLAG
				 , DB_TRAL_ITAK_CD
				 , DB_TRALURI_CNT
				 , DB_TRAL_URI_KING
				 , DB_TRAL_KOJ_CNT
				 , DB_TRAL_KOJ_KING
				 , DB_TRAL_SAS_KING
				 , DB_END_FLAG
				 , DB_END_ITAK_CD
				 , DATA_CRT_FLAG
				 , DATA_CRT_DATE
				 , DATA_CRT_CNT
				 , DATA_CRT_URI_CNT
				 , DATA_CRT_URI1_KING
				 , DATA_CRT_URI2_KING
				 , DATA_CRT_KOJ_CNT
				 , DATA_CRT_KOJ1_KING
				 , DATA_CRT_KOJ2_KING
				 , DATA_CRT_ETC_CNT
				 , DATA_CRT_ETC1_KING
				 , DATA_CRT_ETC2_KING
				 , DATA_CRT_ERR_FLAG
				 , DATA_CRT_ERR_NUM
				 , AUTO_HIKI_FLAG
				 , AUTO_HIKI_DATE
				 , AUTO_HIKI_CNT
				 , AUTO_HIKI_URI_CNT
				 , AUTO_HIKI_URI1_KING
				 , AUTO_HIKI_URI2_KING
				 , AUTO_HIKI_KOJ_CNT
				 , AUTO_HIKI_KOJ1_KING
				 , AUTO_HIKI_KOJ2_KING
				 , AUTO_JGAI_CNT
				 , AUTO_JGAI_URI_CNT
				 , AUTO_JGAI_URI1_KING
				 , AUTO_JGAI_URI2_KING
				 , AUTO_JGAI_KOJ_CNT
				 , AUTO_JGAI_KOJ1_KING
				 , AUTO_JGAI_KOJ2_KING
				 , AUTO_KSKM_YOYK_FLAG
				 , AUTO_KSKM_YOYK_DATE
				 , AUTO_KSKM_FLAG
				 , AUTO_KSKM_DATE
				 , AUTO_KSKM_CNT
				 , AUTO_KSKM_URI_CNT
				 , AUTO_KSKM_URI1_KING
				 , AUTO_KSKM_URI2_KING
				 , AUTO_KSKM_KOJ_CNT
				 , AUTO_KSKM_KOJ1_KING
				 , AUTO_KSKM_KOJ2_KING
				 , DATA_RUISK_FLAG
				 , DATA_RUISK_DATE
				 , YOBI01_FLAG
				 , YOBI02_FLAG
				 , YOBI03_FLAG
				 , YOBI01_KUBN
				 , YOBI02_KUBN
				 , YOBI03_KUBN
				 , YOBI01_KING
				 , YOBI02_KING
				 , YOBI03_KING
				 , DEL_FLAG
				 , CRT_PGM_ID
				 , CRT_USER_ID
				 , CRT_TS
				 , UPD_PGM_ID
				 , UPD_USER_ID
				 , UPD_TS
				 , QUOQ_NYK_DATE
				 )
			VALUES ( 
					_kei_date,
					_wk_hojo_num,
					_p1_db_inpt_flag,
					_p1_db_inpt_date_01,
					_p1_db_inpt_cnt,
					_p1_db_head_flag,
					_p1_db_head_itak_cd,
					_p1_db_head_crt_date,
					_p1_db_head_sime_date,
					_p1_db_head_siha_date,
					_p1_db_head_from_date,
					_p1_db_head_to_date,
					_p1_db_head_crt_date_2,
					_p1_db_head_sime_date_2,
					_p1_db_head_siha_date_2,
					_p1_db_head_from_date_2,
					_p1_db_head_to_date_2,
					_p1_db_head_itak_name,
					_p1_db_head_sinp_cd,
					_p1_db_head_sinp_name,
					_p1_db_tral_flag,
					_p1_db_tralitak_cd,
					_p1_db_traluri_cnt,
					_p1_db_tral_uri_king,
					_p1_db_tral_koj_cnt,
					_p1_db_tral_koj_king,
					_p1_db_tral_sas_king,
					_p1_db_end_flag,
					_p1_db_end_itak_cd,
					_p1_data_crt_flag,
					_p1_data_crt_date_02,
					_p1_data_crt_cnt,
					_p1_data_crt_uri_cnt,
					_p1_data_crt_uri1_king,
					_p1_data_crt_uri2_king,
					_p1_data_crt_koj_cnt,
					_p1_data_crt_koj1_king,
					_p1_data_crt_koj2_king,
					_p1_data_crt_etc_cnt,
					_p1_data_crt_etc1_king,
					_p1_data_crt_etc2_king,
					_p1_data_crt_err_flag,
					_p1_data_crt_err_num,
					_p1_auto_hiki_flag,
					_p1_auto_hiki_date_03,
					_p1_auto_hiki_cnt,
					_p1_auto_hiki_uri_cnt,
					_p1_auto_hiki_uri1_king,
					_p1_auto_hiki_uri2_king,
					_p1_auto_hiki_koj_cnt,
					_p1_auto_hiki_koj1_king,
					_p1_auto_hiki_koj2_king,
					_p1_auto_jgai_cnt,
					_p1_auto_jgai_uri_cnt,
					_p1_auto_jgai_uri1_king,
					_p1_auto_jgai_uri2_king,
					_p1_auto_jgai_koj_cnt,
					_p1_auto_jgai_koj1_king,
					_p1_auto_jgai_koj2_king,
					_p1_auto_kskm_yoyk_flag,
					_p1_auto_kskm_yoyk_date_04,
					_p1_auto_kskm_flag,
					_p1_auto_kskm_date_05,
					_p1_auto_kskm_cnt,
					_p1_auto_kskm_uri_cnt,
					_p1_auto_kskm_uri1_king,
					_p1_auto_kskm_uri2_king,
					_p1_auto_kskm_koj_cnt,
					_p1_auto_kskm_koj1_king,
					_p1_auto_kskm_koj2_king,
					_p1_data_ruisk_flag,
					_p1_data_ruisk_date_06,
					_p1_yobi01_flag,
					_p1_yobi02_flag,
					_p1_yobi03_flag,
					_p1_yobi01_kubn,
					_p1_yobi02_kubn,
					_p1_yobi03_kubn,
					_p1_yobi01_king,
					_p1_yobi02_king,
					_p1_yobi03_king,
					_p1_del_flag,
					_p1_crt_pgm_id_07,
					_p1_crt_user_id_08,
					_p1_crt_ts_09,
					_p1_upd_pgm_id_10,
					_p1_upd_user_id_11,
					_p1_upd_ts_12,
					_p1_quoq_nyk_date_13
					)
			;
			IF SQLCODE = 102 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = '[102] INS_TRN_TATE_RUI_QUOQ_CTLS - FAILED';
				LEAVE dept_loop;
			END IF;
			END INS_TRN_TATE_RUI_QUOQ_CTLS;
			
		END WHILE dept_loop;
		CLOSE CR020_01;		
	END SEL_MAIN;
	
	
	SEL_SECOND: BEGIN
		DECLARE CR020_02 CURSOR FOR
		SELECT DNPYNUM
				 , GYNUM
				 , DATA_KUBN
				 , DATA_SYBT
				 , KAME_NUM_1
				 , KAME_NUM_2
				 , KAME_NUM_3
				 , MOUS_NUM
				 , MOUS_NAME_KANA
				 , MOUS_UKE_DATE
				 , TRKM_DATE
				 , SYOY_KING
				 , SHIHA_KAIS
				 , SYO_SIHA_DATE
				 , KOKY_TESUR_KING
				 , KAME_TESUR_KING
				 , SKIP_TESUR_KING
				 , YOBI_01
				 , YOBI_02
				 , SASI_SIHA_KING
				 , TEL
				 , DNP_NUM
				 , DAIR_TEN_CODE
				 , KAME_TANT_CODE
				 , KAME_SERI_NUM
				 , TESUR_KAME2_KING
				 , TESUR_SIHA2_KING
				 , TRKM_HIYO_KING
				 , CARD_NUM
				 , YOBI_03
				 , TATE_SIME_DATE
				 , TATE_SIHA_DATE
				 , CNV_FLAG
				 , CNV_DATE
				 , CNV_TS
				 , CNV_ERR01_FLAG
				 , CNV_ERR02_FLAG
				 , CNV_ERR03_FLAG
				 , CNV_ERR04_FLAG
				 , CNV_ERR05_FLAG
				 , CNV_ERR06_FLAG
				 , CNV_ERR07_FLAG
				 , CNV_ERR08_FLAG
				 , CNV_ERR09_FLAG
				 , CNV_ERR10_FLAG
				 , CRJT_KAME_NUM
				 , CHK_SINP_CODE
				 , CHK_SIHA_HOUH_KUBN
				 , CHK_SINP_EGTN_CODE
				 , CHK_SYNIN_NUM
				 , CHK_NAME_KANA
				 , CHK_SYOY_KING
				 , CHK_SHIHA_KAIS
				 , CHK_KOKY_TESUR_KING
				 , CHK_KAME_TESUR_KING
				 , CHK_SKIP_TESUR_KING
				 , CHK_SASI_SIHA_KING
				 , CHK_TESUR_KAME2_KING
				 , CHK_TESUR_SIHA2_KING
				 , CHK_TRKM_HIYO_KING
				 , CHK_YOBI01_KING
				 , CHK_YOBI02_KING
				 , CHK_YOBI03_KING
				 , MATCH_ERR_FLAG
				 , MATCH_DATE
				 , MATCH_TS
				 , MATCH_ERR01_FLAG
				 , MATCH_ERR02_FLAG
				 , MATCH_ERR03_FLAG
				 , MATCH_ERR04_FLAG
				 , MATCH_ERR05_FLAG
				 , MATCH_ERR06_FLAG
				 , MATCH_ERR07_FLAG
				 , MATCH_ERR08_FLAG
				 , MATCH_ERR09_FLAG
				 , MATCH_ERR10_FLAG
				 , HIKI_FLAG
				 , KONY_DNPYNUM
				 , KONY_SRNUM
				 , RIRK_NUM
				 , HEAD_VERNUM
				 , LOGNUM
				 , KKYK_CODE
				 , KKYK_KANA
				 , KKYK_KANA_MYOJI
				 , KKYK_KANA_NAMAE
				 , KSKM_FLAG
				 , KSKM_DATE
				 , KSKM_TS
				 , CRNY_DNPYNUM
				 , CRNY_SRNUM
				 , CRNY_GYNUM
				 , YOBI01_FLAG
				 , YOBI02_FLAG
				 , YOBI03_FLAG
				 , YOBI01_KUBN
				 , YOBI02_KUBN
				 , YOBI03_KUBN
				 , YOBI01_KING
				 , YOBI02_KING
				 , YOBI03_KING
				 , DEL_FLAG
				 , CRT_DATE
				 , CRT_PGM_ID
				 , CRT_USER_ID
				 , CRT_TS
				 , UPD_PGM_ID
				 , UPD_USER_ID
				 , UPD_TS
		FROM TRN_TATE_QUOQ_01S;
		
		DECLARE EXIT HANDLER FOR NOT FOUND
		BEGIN
			SET SQLCODE=100;
		END;
	
		OPEN CR020_02;
			dept_loop:WHILE(SQLCODE=0) DO
			
				SET _p2_dnpynum = 0;
				SET _p2_gynum = 0;
				
				SET _p2_data_kubn = '\0';
				SET _p2_data_sybt = '\0';
				SET _p2_kame_num_1 = '\0';
				SET _p2_kame_num_2 = '\0';
				SET _p2_kame_num_3 = '\0';
				SET _p2_mous_num = '\0';
				SET _p2_mous_name_kana = '\0';
				SET _p2_mous_uke_date = '\0';
				SET _p2_trkm_date = '\0';
				SET _p2_syoy_king = '\0';
				SET _p2_shiha_kais = '\0';
				SET _p2_syo_siha_date = '\0';
				SET _p2_koky_tesur_king = '\0';
				SET _p2_kame_tesur_king = '\0';
				SET _p2_skip_tesur_king = '\0';
				SET _p2_yobi_01 = '\0';
				SET _p2_yobi_02 = '\0';
				SET _p2_sasi_siha_king = '\0';
				SET _p2_tel = '\0';
				SET _p2_dnp_num = '\0';
				SET _p2_dair_ten_code = '\0';
				SET _p2_kame_tant_code = '\0';
				SET _p2_kame_seri_num = '\0';
				SET _p2_tesur_kame2_king = '\0';
				SET _p2_tesur_siha2_king = '\0';
				SET _p2_trkm_hiyo_king = '\0';
				SET _p2_card_num = '\0';
				SET _p2_yobi_03 = '\0';
				SET _p2_tate_sime_date = '\0';
				SET _p2_tate_siha_date = '\0';
				SET _p2_cnv_flag = '\0';
				SET _p2_cnv_date = '\0';
				SET _p2_cnv_ts = '\0';
				SET _p2_cnv_err01_flag = '\0';
				SET _p2_cnv_err02_flag = '\0';
				SET _p2_cnv_err03_flag = '\0';
				SET _p2_cnv_err05_flag = '\0';
				SET _p2_cnv_err06_flag = '\0';
				SET _p2_cnv_err07_flag = '\0';
				SET _p2_cnv_err08_flag = '\0';
				SET _p2_cnv_err09_flag = '\0';
				SET _p2_cnv_err10_flag = '\0';
				SET _p2_crjt_kame_num = '\0';
				
				SET _p2_chk_sinp_code = 0;
				
				SET _p2_chk_siha_houh_kubn = '\0';
				SET _p2_chk_sinp_egtn_code = '\0';
				SET _p2_chk_synin_num = '\0';
				SET _p2_chk_name_kana = '\0';
				
				SET _p2_chk_syoy_king = 0;
				SET _p2_chk_shiha_kais = 0;
				SET _p2_chk_koky_tesur_king = 0;
				SET _p2_chk_kame_tesur_king = 0;
				SET _p2_chk_skip_tesur_king = 0;
				SET _p2_chk_sasi_siha_king = 0;
				SET _p2_chk_tesur_kame2_king = 0;
				SET _p2_chk_tesur_siha2_king = 0;
				SET _p2_chk_trkm_hiyo_king = 0;
				SET _p2_chk_yobi01_king = 0;
				SET _p2_chk_yobi02_king = 0;
				SET _p2_chk_yobi03_king = 0;
				
				SET _p2_match_err_flag = '\0';
				SET _p2_match_date = '\0';
				SET _p2_match_ts = '\0';
				SET _p2_match_err01_flag = '\0';
				SET _p2_match_err02_flag = '\0';
				SET _p2_match_err03_flag = '\0';
				SET _p2_match_err04_flag = '\0';
				SET _p2_match_err05_flag = '\0';
				SET _p2_match_err06_flag = '\0';
				SET _p2_match_err07_flag = '\0';
				SET _p2_match_err08_flag = '\0';
				SET _p2_match_err09_flag = '\0';
				SET _p2_match_err10_flag = '\0';
				SET _p2_hiki_flag = '\0';
				
				SET _p2_kony_dnpynum = 0;
				SET _p2_kony_srnum = 0;
				SET _p2_rirk_num = 0;
				SET _p2_head_vernum = 0;
				SET _p2_lognum = 0;
				
				SET _p2_kkyk_code = '\0';
				SET _p2_kkyk_kana = '\0';
				SET _p2_kkyk_kana_myoji = '\0';
				SET _p2_kkyk_kana_namae = '\0';
				SET _p2_kskm_flag = '\0';
				SET _p2_kskm_date = '\0';
				SET _p2_kskm_ts = '\0';
				
				SET _p2_crny_dnpynum = 0;
				SET _p2_crny_srnum = 0;
				SET _p2_crny_gynum = 0;
				
				SET _p2_yobi01_flag = '\0';
				SET _p2_yobi02_flag = '\0';
				SET _p2_yobi03_flag = '\0';
				SET _p2_yobi01_kubn = '\0';
				SET _p2_yobi02_kubn = '\0';
				SET _p2_yobi03_kubn = '\0';
				
				SET _p2_yobi01_king = 0;
				SET _p2_yobi02_king = 0;
				SET _p2_yobi03_king = 0;
				
				SET _p2_del_flag = '\0';
				SET _p2_crt_date = '\0';
				SET _p2_crt_pgm_id = '\0';
				SET _p2_crt_user_id = '\0';
				SET _p2_crt_ts = '\0';
				SET _p2_upd_pgm_id = '\0';
				SET _p2_upd_user_id = '\0';
				SET _p2_upd_ts = '\0';
				
				SET _in_p2_01 = 0;
				SET _in_p2_02 = 0;
				SET _in_p2_03 = 0;
				SET _in_p2_04 = 0;
				SET _in_p2_05 = 0;
				SET _in_p2_06 = 0;
				SET _in_p2_07 = 0;
				SET _in_p2_08 = 0;
				SET _in_p2_09 = 0;
				SET _in_p2_10 = 0;
				SET _in_p2_11 = 0;
				SET _in_p2_12 = 0;
				
				FETCH FROM CR020_02
				INTO
					_p2_dnpynum,
					_p2_gynum,
					_p2_data_kubn,
					_p2_data_sybt,
					_p2_kame_num_1,
					_p2_kame_num_2,
					_p2_kame_num_3,
					_p2_mous_num,
					_p2_mous_name_kana,
					_p2_mous_uke_date,
					_p2_trkm_date,
					_p2_syoy_king,
					_p2_shiha_kais,
					_p2_syo_siha_date,
					_p2_koky_tesur_king,
					_p2_kame_tesur_king,
					_p2_skip_tesur_king,
					_p2_yobi_01,
					_p2_yobi_02,
					_p2_sasi_siha_king,
					_p2_tel,
					_p2_dnp_num,
					_p2_dair_ten_code,
					_p2_kame_tant_code,
					_p2_kame_seri_num,
					_p2_tesur_kame2_king,
					_p2_tesur_siha2_king,
					_p2_trkm_hiyo_king,
					_p2_card_num,
					_p2_yobi_03,
					_p2_tate_sime_date,
					_p2_tate_siha_date,
					_p2_cnv_flag,
					_p2_cnv_date,
					_p2_cnv_ts_01,
					_p2_cnv_err01_flag,
					_p2_cnv_err02_flag,
					_p2_cnv_err03_flag,
					_p2_cnv_err04_flag,
					_p2_cnv_err05_flag,
					_p2_cnv_err06_flag,
					_p2_cnv_err07_flag,
					_p2_cnv_err08_flag,
					_p2_cnv_err09_flag,
					_p2_cnv_err10_flag,
					_p2_crjt_kame_num,
					_p2_chk_sinp_code,
					_p2_chk_siha_houh_kubn,
					_p2_chk_sinp_egtn_code,
					_p2_chk_synin_num,
					_p2_chk_name_kana,
					_p2_chk_syoy_king,
					_p2_chk_shiha_kais,
					_p2_chk_koky_tesur_king,
					_p2_chk_kame_tesur_king,
					_p2_chk_skip_tesur_king,
					_p2_chk_sasi_siha_king,
					_p2_chk_tesur_kame2_king,
					_p2_chk_tesur_siha2_king,
					_p2_chk_trkm_hiyo_king,
					_p2_chk_yobi01_king,
					_p2_chk_yobi02_king,
					_p2_chk_yobi03_king,
					_p2_match_err_flag,
					_p2_match_date_02,
					_p2_match_ts_03,
					_p2_match_err01_flag,
					_p2_match_err02_flag,
					_p2_match_err03_flag,
					_p2_match_err04_flag,
					_p2_match_err05_flag,
					_p2_match_err06_flag,
					_p2_match_err07_flag,
					_p2_match_err08_flag,
					_p2_match_err09_flag,
					_p2_match_err10_flag,
					_p2_hiki_flag,
					_p2_kony_dnpynum,
					_p2_kony_srnum,
					_p2_rirk_num,
					_p2_head_vernum,
					_p2_lognum,
					_p2_kkyk_code,
					_p2_kkyk_kana,
					_p2_kkyk_kana_myoji,
					_p2_kkyk_kana_namae,
					_p2_kskm_flag,
					_p2_kskm_date_04,
					_p2_kskm_ts_05,
					_p2_crny_dnpynum,
					_p2_crny_srnum,
					_p2_crny_gynum,
					_p2_yobi01_flag,
					_p2_yobi02_flag,
					_p2_yobi03_flag,
					_p2_yobi01_kubn,
					_p2_yobi02_kubn,
					_p2_yobi03_kubn,
					_p2_yobi01_king,
					_p2_yobi02_king,
					_p2_yobi03_king,
					_p2_del_flag,
					_p2_crt_date_06,
					_p2_crt_pgm_id_07,
					_p2_crt_user_id_08,
					_p2_crt_ts_09,
					_p2_upd_pgm_id_10,
					_p2_upd_user_id_11,
					_p2_upd_ts_12;
				
				IF _p2_dnpynum = 0 THEN
					SET SQLCODE = 100;
				END IF;
				
				IF SQLCODE = 100 THEN
					SET fnReturn = KFAIL;
					SET fnMsg = '2. 立替金ＴＲ：明細データ（クオーク）読込み IsNotFound';
					LEAVE dept_loop;
				END IF;
				
				/* 累積：立替金ＴＲ：明細データ（クオーク） */
				INS_TRN_TATE_RUI_QUOQ_01S:BEGIN
				DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
				BEGIN
					 SET SQLCODE = 102;
				END;
				INSERT INTO TRN_TATE_RUI_QUOQ_01S (
						 DNPYNUM
					 , GYNUM
					 , DATA_KUBN
					 , DATA_SYBT
					 , KAME_NUM_1
					 , KAME_NUM_2
					 , KAME_NUM_3
					 , MOUS_NUM
					 , MOUS_NAME_KANA
					 , MOUS_UKE_DATE
					 , TRKM_DATE
					 , SYOY_KING
					 , SHIHA_KAIS
					 , SYO_SIHA_DATE
					 , KOKY_TESUR_KING
					 , KAME_TESUR_KING
					 , SKIP_TESUR_KING
					 , YOBI_01
					 , YOBI_02
					 , SASI_SIHA_KING
					 , TEL
					 , DNP_NUM
					 , DAIR_TEN_CODE
					 , KAME_TANT_CODE
					 , KAME_SERI_NUM
					 , TESUR_KAME2_KING
					 , TESUR_SIHA2_KING
					 , TRKM_HIYO_KING
					 , CARD_NUM
					 , YOBI_03
					 , TATE_SIME_DATE
					 , TATE_SIHA_DATE
					 , CNV_FLAG
					 , CNV_DATE
					 , CNV_TS
					 , CNV_ERR01_FLAG
					 , CNV_ERR02_FLAG
					 , CNV_ERR03_FLAG
					 , CNV_ERR04_FLAG
					 , CNV_ERR05_FLAG
					 , CNV_ERR06_FLAG
					 , CNV_ERR07_FLAG
					 , CNV_ERR08_FLAG
					 , CNV_ERR09_FLAG
					 , CNV_ERR10_FLAG
					 , CRJT_KAME_NUM
					 , CHK_SINP_CODE
					 , CHK_SIHA_HOUH_KUBN
					 , CHK_SINP_EGTN_CODE
					 , CHK_SYNIN_NUM
					 , CHK_NAME_KANA
					 , CHK_SYOY_KING
					 , CHK_SHIHA_KAIS
					 , CHK_KOKY_TESUR_KING
					 , CHK_KAME_TESUR_KING
					 , CHK_SKIP_TESUR_KING
					 , CHK_SASI_SIHA_KING
					 , CHK_TESUR_KAME2_KING
					 , CHK_TESUR_SIHA2_KING
					 , CHK_TRKM_HIYO_KING
					 , CHK_YOBI01_KING
					 , CHK_YOBI02_KING
					 , CHK_YOBI03_KING
					 , MATCH_ERR_FLAG
					 , MATCH_DATE
					 , MATCH_TS
					 , MATCH_ERR01_FLAG
					 , MATCH_ERR02_FLAG
					 , MATCH_ERR03_FLAG
					 , MATCH_ERR04_FLAG
					 , MATCH_ERR05_FLAG
					 , MATCH_ERR06_FLAG
					 , MATCH_ERR07_FLAG
					 , MATCH_ERR08_FLAG
					 , MATCH_ERR09_FLAG
					 , MATCH_ERR10_FLAG
					 , HIKI_FLAG
					 , KONY_DNPYNUM
					 , KONY_SRNUM
					 , RIRK_NUM
					 , HEAD_VERNUM
					 , LOGNUM
					 , KKYK_CODE
					 , KKYK_KANA
					 , KKYK_KANA_MYOJI
					 , KKYK_KANA_NAMAE
					 , KSKM_FLAG
					 , KSKM_DATE
					 , KSKM_TS
					 , CRNY_DNPYNUM
					 , CRNY_SRNUM
					 , CRNY_GYNUM
					 , YOBI01_FLAG
					 , YOBI02_FLAG
					 , YOBI03_FLAG
					 , YOBI01_KUBN
					 , YOBI02_KUBN
					 , YOBI03_KUBN
					 , YOBI01_KING
					 , YOBI02_KING
					 , YOBI03_KING
					 , DEL_FLAG
					 , CRT_DATE
					 , CRT_PGM_ID
					 , CRT_USER_ID
					 , CRT_TS
					 , UPD_PGM_ID
					 , UPD_USER_ID
					 , UPD_TS)
				VALUES(
						_p2_dnpynum,
						_p2_gynum,
						_p2_data_kubn,
						_p2_data_sybt,
						_p2_kame_num_1,
						_p2_kame_num_2,
						_p2_kame_num_3,
						_p2_mous_num,
						_p2_mous_name_kana,
						_p2_mous_uke_date,
						_p2_trkm_date,
						_p2_syoy_king,
						_p2_shiha_kais,
						_p2_syo_siha_date,
						_p2_koky_tesur_king,
						_p2_kame_tesur_king,
						_p2_skip_tesur_king,
						_p2_yobi_01,
						_p2_yobi_02,
						_p2_sasi_siha_king,
						_p2_tel,
						_p2_dnp_num,
						_p2_dair_ten_code,
						_p2_kame_tant_code,
						_p2_kame_seri_num,
						_p2_tesur_kame2_king,
						_p2_tesur_siha2_king,
						_p2_trkm_hiyo_king,
						_p2_card_num,
						_p2_yobi_03,
						_p2_tate_sime_date,
						_p2_tate_siha_date,
						_p2_cnv_flag,
						_p2_cnv_date,
						_p2_cnv_ts_01,
						_p2_cnv_err01_flag,
						_p2_cnv_err02_flag,
						_p2_cnv_err03_flag,
						_p2_cnv_err04_flag,
						_p2_cnv_err05_flag,
						_p2_cnv_err06_flag,
						_p2_cnv_err07_flag,
						_p2_cnv_err08_flag,
						_p2_cnv_err09_flag,
						_p2_cnv_err10_flag,
						_p2_crjt_kame_num,
						_p2_chk_sinp_code,
						_p2_chk_siha_houh_kubn,
						_p2_chk_sinp_egtn_code,
						_p2_chk_synin_num,
						_p2_chk_name_kana,
						_p2_chk_syoy_king,
						_p2_chk_shiha_kais,
						_p2_chk_koky_tesur_king,
						_p2_chk_kame_tesur_king,
						_p2_chk_skip_tesur_king,
						_p2_chk_sasi_siha_king,
						_p2_chk_tesur_kame2_king,
						_p2_chk_tesur_siha2_king,
						_p2_chk_trkm_hiyo_king,
						_p2_chk_yobi01_king,
						_p2_chk_yobi02_king,
						_p2_chk_yobi03_king,
						_p2_match_err_flag,
						_p2_match_date_02,
						_p2_match_ts_03,
						_p2_match_err01_flag,
						_p2_match_err02_flag,
						_p2_match_err03_flag,
						_p2_match_err04_flag,
						_p2_match_err05_flag,
						_p2_match_err06_flag,
						_p2_match_err07_flag,
						_p2_match_err08_flag,
						_p2_match_err09_flag,
						_p2_match_err10_flag,
						_p2_hiki_flag,
						_p2_kony_dnpynum,
						_p2_kony_srnum,
						_p2_rirk_num,
						_p2_head_vernum,
						_p2_lognum,
						_p2_kkyk_code,
						_p2_kkyk_kana,
						_p2_kkyk_kana_myoji,
						_p2_kkyk_kana_namae,
						_p2_kskm_flag,
						_p2_kskm_date_04,
						_p2_kskm_ts_05,
						_p2_crny_dnpynum,
						_p2_crny_srnum,
						_p2_crny_gynum,
						_p2_yobi01_flag,
						_p2_yobi02_flag,
						_p2_yobi03_flag,
						_p2_yobi01_kubn,
						_p2_yobi02_kubn,
						_p2_yobi03_kubn,
						_p2_yobi01_king,
						_p2_yobi02_king,
						_p2_yobi03_king,
						_p2_del_flag,
						_p2_crt_date_06,
						_p2_crt_pgm_id_07,
						_p2_crt_user_id_08,
						_p2_crt_ts_09,
						_p2_upd_pgm_id_10,
						_p2_upd_user_id_11,
						_p2_upd_ts_12
						)
					;
				IF SQLCODE = 101 THEN 
					SET fnReturn = KFAIL;
					SET fnMsg = 'INS_TRN_TATE_RUI_QUOQ_01S FAILED';
					LEAVE dept_loop;
				END IF;
				END INS_TRN_TATE_RUI_QUOQ_01S;
			END WHILE dept_loop;
		CLOSE CR020_02;
	END SEL_SECOND;
END MAIN_PROC
CREATE DEFINER=`root`@`localhost` PROCEDURE `MG00106`(
	IN IN_SKMB_KUBN CHAR(2),
	IN IN_KYU CHAR(1),
	OUT fnReturn INT,
	OUT fnMsg CHAR(100))
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMG00106
	-- 機能		: 	製品の原価計算処理２－１
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-01-28
	-- =============================================

	DECLARE _p0106_1_syhn_code CHAR(9) DEFAULT NULL;
	DECLARE _p0106_1_kyu CHAR(2) DEFAULT NULL;
	DECLARE _p0106_1_pmn_genk_tank DECIMAL(11,2) DEFAULT 0;
	DECLARE _p0106_1_genk_tank DECIMAL(11, 2) DEFAULT 0;
	DECLARE _p0106_1_genk_syuk_sury DECIMAL(11,2) DEFAULT 0;
	DECLARE _p0106_1_genk_syuk_king DECIMAL(11,2) DEFAULT 0;
	DECLARE _p0106_1_genk_kesn_flag CHAR(2) DEFAULT NULL;
	
	DECLARE _p0106_wk_genk_tank DECIMAL(11, 2) DEFAULT 0;
	DECLARE _p0106_wk_genk_kesn_flag CHAR(2) DEFAULT NULL;
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;

	DECLARE IsNotFound INT;
	
	/***************************************************************/
  /* メイン処理 */
  /***************************************************************/
	DECLARE C0106_01 CURSOR FOR 
    SELECT 
				SYHN_CODE, 
				KYU,
				PMN_GENK_TANK, 
				GENK_TANK,
				GENK_SYUK_SURY, 
				GENK_SYUK_KING,
				GENK_KESN_FLAG
      FROM MST_SYHN_GENK_RDYS
     WHERE MST_SYHN_GENK_RDYS.SKMB_KUBN = IN_SKMB_KUBN AND MST_SYHN_GENK_RDYS.KYU = IN_KYU
  ;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND
	BEGIN
		SET IsNotFound=1;
	END;
	
	SET IsNotFound=0;
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	OPEN C0106_01;
	
	dept_loop:WHILE(IsNotFound=0) DO
	
		SET _p0106_1_syhn_code = '\0';
		SET _p0106_1_kyu = '\0';
		SET _p0106_1_pmn_genk_tank = 0;
		SET _p0106_1_genk_tank = 0;
		SET _p0106_1_genk_syuk_sury = 0;
		SET _p0106_1_genk_syuk_king = 0;
		SET _p0106_1_genk_kesn_flag = '\0';
		
		FETCH FROM C0106_01
			INTO
			_p0106_1_syhn_code,
			_p0106_1_kyu,
			_p0106_1_pmn_genk_tank,
			_p0106_1_genk_tank,
			_p0106_1_genk_syuk_sury,
			_p0106_1_genk_syuk_king,
			_p0106_1_genk_kesn_flag
		;
	
		IF IsNotFound=1 THEN
			SET fnReturn = KFAIL;
		  SET fnMsg = 'メイン処理 IsNotFound';
			LEAVE dept_loop;
		END IF;
		
		SET _p0106_wk_genk_tank = 0;
		IF (_p0106_1_genk_syuk_king > 0 AND _p0106_1_genk_syuk_sury > 0) THEN
			SET _p0106_wk_genk_tank = CAST(CAST(_p0106_1_genk_syuk_king AS DECIMAL(11,0)) / CAST(_p0106_1_genk_syuk_sury AS DECIMAL(11,2)) AS DECIMAL(11,2));
		ELSE
			IF (_p0106_1_genk_syuk_king = 0 AND _p0106_1_genk_syuk_sury > 0) THEN
				SET _p0106_wk_genk_tank = 0;
			ELSE
				SET _p0106_wk_genk_tank = CAST(_p0106_1_pmn_genk_tank AS DECIMAL(11,2));
				IF (_p0106_1_genk_syuk_king = 0 AND _p0106_1_genk_syuk_sury = 0) THEN
					SET _p0106_wk_genk_kesn_flag = '0';
				ELSE
					SET _p0106_wk_genk_kesn_flag = '1';
				END IF;
			END IF;
		END IF;
		
		/* 級商品別原価マスタ（レディ）更新 */
		UPT: BEGIN
			DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
			BEGIN
				SET SQLCODE = 100;
			END;
			UPDATE MST_SYHN_GENK_RDYS
			SET GENK_TANK    		= CAST(_p0106_wk_genk_tank AS DECIMAL(11,2)),
					GENK_KESN_FLAG 	= _p0106_wk_genk_kesn_flag
			WHERE 
					SYHN_CODE	   		= _p0106_1_syhn_code
					AND KYU        	= _p0106_1_kyu
			;
			IF SQLCODE = 100 THEN 
				SET fnReturn = KFAIL;
				SET fnMsg = '級商品別原価マスタ（レディ）更新 - UPDATE FAILED';
				LEAVE dept_loop;
			END IF;
		END UPT;
		
	END WHILE dept_loop;
	CLOSE C0106_01;
	
END MAIN_PROC
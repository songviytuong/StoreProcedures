CREATE DEFINER=`root`@`localhost` PROCEDURE `MMNH010`(
	INOUT fnReturn INT,
	OUT fnMsg CHAR(100)
)
MAIN_PROC: BEGIN
	-- =============================================
	-- ファイル名	: 	bMMNH010
	-- 機能		: 	納品基準  月次更新  店舗在庫マスタ更新
	-- 著者		: 	TBinh-PrimeLabo
	-- 作成日	: 	2019-02-12
	-- =============================================
	
	/* 作業項目 */
	DECLARE _pgmid CHAR(64);
	
	DECLARE SQLCODE     INT     DEFAULT 0;
	DECLARE KSUCCEED    INT     DEFAULT 1;
	DECLARE KFAIL     	INT     DEFAULT 0;
	
	/*INIT SETUP*/
	SET fnReturn = KSUCCEED;
	SET fnMsg = '';
	
	/***************************************************************/
	/* データ更新処理 */
	/***************************************************************/
	
	/* 店舗在庫マスタの月次更新 */
	UPT: BEGIN
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
		BEGIN
			SET SQLCODE = 100;
		END;
		UPDATE MST_ZAI_SHOPS
		SET		PMN_SEK_ZAISU     = TMN_SEK_ZAISU
				, TMN_SEK_IN_01SU   = NMN_SEK_IN_01SU
				, TMN_SEK_IN_02SU   = NMN_SEK_IN_02SU
				, TMN_SEK_OUT_01SU  = NMN_SEK_OUT_01SU
				, TMN_SEK_OUT_02SU  = NMN_SEK_OUT_02SU
				, TMN_SEK_ZAISU     = GEN_SEK_ZAISU
				, NMN_SEK_IN_01SU   = 0
				, NMN_SEK_IN_02SU   = 0
				, NMN_SEK_OUT_01SU  = 0
				, NMN_SEK_OUT_02SU  = 0
				, PMN_SHOP_ZAISU    = TMN_SHOP_ZAISU
				, TMN_SHOP_IN_01SU  = NMN_SHOP_IN_01SU
				, TMN_SHOP_IN_02SU  = NMN_SHOP_IN_02SU
				, TMN_SHOP_OUT_01SU = NMN_SHOP_OUT_01SU
				, TMN_SHOP_OUT_02SU = NMN_SHOP_OUT_02SU
				, TMN_SHOP_ZAISU    = GEN_SHOP_ZAISU
				, NMN_SHOP_IN_01SU  = 0
				, NMN_SHOP_IN_02SU  = 0
				, NMN_SHOP_OUT_01SU = 0
				, NMN_SHOP_OUT_02SU = 0
				, PMN_SEK_ZAIKIN    = TMN_SEK_ZAIKIN
				, TMN_SEK_ZAIKIN    = GEN_SEK_ZAIKIN
				, PMN_SHOP_ZAIKIN   = TMN_SHOP_ZAIKIN
				, TMN_SHOP_ZAIKIN   = GEN_SHOP_ZAIKIN
				, MONTHLY_TS        = CURRENT_TIMESTAMP
				, PGM_ID            = _pgmid
				, USER_ID           = NULL
				, MODIFIED          = CURRENT_TIMESTAMP -- TS
				, TMN_SHOP_OUT_01KS = NMN_SHOP_OUT_01KS
				, TMN_SHOP_IN_01KS  = NMN_SHOP_IN_01KS
				, TMN_SHOP_OUT_02KS = NMN_SHOP_OUT_02KS
				, TMN_SHOP_IN_02KS  = NMN_SHOP_IN_02KS
				, NMN_SHOP_OUT_01KS = 0
				, NMN_SHOP_IN_01KS  = 0
				, NMN_SHOP_OUT_02KS = 0
				, NMN_SHOP_IN_02KS  = 0
		;
		IF SQLCODE = 100 THEN 
			SET fnReturn = KFAIL;
			SET fnMsg = '店舗在庫マスタの月次更新 - UPDATE FAILED';
			LEAVE MAIN_PROC;
		END IF;
	END UPT;

END MAIN_PROC